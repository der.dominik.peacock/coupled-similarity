﻿
using System;
using UnityEngine;
using UnityEditor;

using CoupledSimilarity.Game.Graph;

namespace CoupledSimilarity.Editor.Graph
{
    [CustomEditor(typeof(MonoNode))]
    [ExecuteInEditMode]
    public class GameNodeEditor : UnityEditor.Editor
    {
        private SerializedProperty GameEdges;
        private GraphFactory EdgeFactory;
        private Vector3 PreviousPosition;

        private void OnEnable()
        {
            GameEdges = serializedObject.FindProperty("OutgoingEdges");
            EdgeFactory = Resources.Load<GraphFactory>("Config/EditorEdgeFactory");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            base.OnInspectorGUI();

            if (GUILayout.Button("Add edge"))
            {
                CreateEdge();
            }
            if(GUILayout.Button("Add edge with node"))
            {
                CreateEdgeWithNode();
            }

            CheckWhetherNodePositionHasChanged();
        }
        private void CheckWhetherNodePositionHasChanged()
        {
            MonoNode observedObject = serializedObject.targetObject as MonoNode;
            Vector3 currentPosition = observedObject.transform.position;

            bool bShouldRedrawIncomingAndOutgoingEdges = PreviousPosition != currentPosition;
            if (bShouldRedrawIncomingAndOutgoingEdges)
            {
                observedObject.RedrawEdges();
            }

            PreviousPosition = currentPosition;
        }


        private void CreateEdge()
        {
            MonoEdge newEdge = EdgeFactory.CreateEdgeWithParent(
                serializedObject.targetObject as MonoNode
                );
            AttackEdgeToObservedNode(newEdge);
        }
        
        private void CreateEdgeWithNode()
        {
            Tuple<MonoNode, MonoEdge> result = EdgeFactory.CreateEdgeWithSuccessor(serializedObject.targetObject as MonoNode);
            MonoNode successorNode = result.Item1;
            MonoEdge newEdge = result.Item2;
            AttackEdgeToObservedNode(newEdge);
        }

        private void AttackEdgeToObservedNode(MonoEdge edge)
        {
            MonoNode observedNode = serializedObject.targetObject as MonoNode;
            Undo.RecordObject(observedNode, "Add edge");
            EditorUtility.SetDirty(observedNode);
            observedNode.AddGameEdge(edge);
        }
       

    }
}