﻿
using System;
using UniRx;
using UnityEngine;
using UnityEditor;

using CoupledSimilarity.Editor.Utility;
using CoupledSimilarity.Game.Graph;

namespace CoupledSimilarity.Editor.Graph
{
    [CustomEditor(typeof(MonoEdge))]
    public class GameEdgeEditor : UnityEditor.Editor
    {
        private GraphFactory EdgeVisualChanger;
        private SerializedProperty TargetNode;
        private SerializedProperty EdgeWeight;

        private PropertyMonitor PropertyChangeMonitor = new PropertyMonitor();
        private CompositeDisposable Disposer = new CompositeDisposable();

        private void OnDisable()
        {
            Disposer.Dispose(); 
        }

        private void OnEnable()
        {
            EdgeVisualChanger = Resources.Load<GraphFactory>("Config/EditorEdgeFactory");

            TargetNode = serializedObject.FindProperty("_TargetNode");
            PropertyChangeMonitor.SubscribeForObject<MonoNode>(
                TargetNode,
                OnChangeTarget
                ).AddTo(Disposer);

            EdgeWeight = serializedObject.FindProperty("_EdgeWeight");
            PropertyChangeMonitor.SubscribeForEnum<EGameEdgeWeight>(
                 EdgeWeight,
                 OnChangeWeight
                 ).AddTo(Disposer);
        }
        private void OnChangeTarget(MonoNode oldTarget)
        {
            MonoEdge gameEdge = serializedObject.targetObject as MonoEdge;
            if(gameEdge.ParentNodeAsGameNode == oldTarget)
            {
                Debug.LogWarning("You cannot create cycles in the graph.");
                return;
            }

            if(oldTarget != null)
            {
                Undo.RecordObject(oldTarget, "Remove ingoing edge");
                EditorUtility.SetDirty(oldTarget);
                oldTarget.RemoveIngoingEdge(gameEdge);
            }
            if(gameEdge.TargetNodeAsGameNode != null)
            {
                Undo.RecordObject(gameEdge.TargetNodeAsGameNode, "Add ingoing edge");
                EditorUtility.SetDirty(gameEdge.TargetNodeAsGameNode);
                gameEdge.TargetNodeAsGameNode.AddIngoingEdge(gameEdge);
            }

            RedrawEdge();
        }
        private void RedrawEdge()
        {
            MonoEdge gameEdge = serializedObject.targetObject as MonoEdge;
            MonoNode newTarget = TargetNode.objectReferenceValue as MonoNode;
            gameEdge.UpdateDirectionAndRedraw(newTarget);
        }

        private void OnChangeWeight(EGameEdgeWeight oldWeight)
        {
            MonoEdge modifiedEdge = serializedObject.targetObject as MonoEdge;
            EGameEdgeWeight newWeight = modifiedEdge.EdgeWeight;
            EdgeVisualChanger.ChangeWeight(modifiedEdge, newWeight);
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            PropertyChangeMonitor.PollForModifications(serializedObject);

            EditorGUILayout.PropertyField(
                serializedObject.FindProperty("DistanceFromNodes")
                );
        }
    }
}