﻿
using UnityEngine;

using CoupledSimilarity.Game.Graph;

namespace CoupledSimilarity.Editor.Graph
{
    [System.Serializable]
    public class EdgeVisualConfig
    {
        public EGameEdgeWeight ConfigForWeight;
        public Sprite TextureToUse;
        public bool bIsTiled = true;
        public Color Tint = Color.white;

        public void ApplyTo(MonoEdge edge)
        {
            SpriteRenderer spriteRenderer = edge.GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = TextureToUse;
            spriteRenderer.drawMode = bIsTiled ? SpriteDrawMode.Tiled : SpriteDrawMode.Sliced;
            spriteRenderer.tileMode = SpriteTileMode.Adaptive;
            spriteRenderer.color = Tint;
        }
    }
}