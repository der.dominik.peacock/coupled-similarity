﻿
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Utility;

namespace CoupledSimilarity.Editor.Graph
{
    [CreateAssetMenu(fileName = "EditorEdgeFactory", menuName = "Editor/EdgeFactory", order = 1)]
    public class GraphFactory : ScriptableObject
    {
        public Prefab NodePrefab;
        public Vector2 InitialNodeOffset = new Vector2(1.5f, 0f);

        public Prefab EdgePrefab;
        public Vector2 InitialEdgeOffset = new Vector2(0.5f, 0f);

        public List<EdgeVisualConfig> VisualEdgeConfig = new List<EdgeVisualConfig>();

        public Tuple<MonoNode, MonoEdge> CreateEdgeWithSuccessor(MonoNode startNode)
        {
            MonoEdge edge = CreateEdgeWithParent(startNode);

            GameObject nodeObject = NodePrefab.InstantiateInEditor();
            nodeObject.transform.parent = startNode.transform.parent;
            nodeObject.transform.localPosition = InitialNodeOffset;

            MonoNode successorNode = nodeObject.GetComponent<MonoNode>();
            successorNode.AddIngoingEdge(edge);
            edge.UpdateDirectionAndRedraw(successorNode);

            return Tuple.Create(successorNode, edge);
        }

        public MonoEdge CreateEdgeWithParent(MonoNode startNode)
        {
            if(startNode == null)
            {
                throw new ArgumentException("startNode cannot be null");
            }

            GameObject edgeObject = EdgePrefab.InstantiateInEditor();
            edgeObject.transform.parent = startNode.transform;
            edgeObject.transform.localPosition = InitialEdgeOffset;

            MonoEdge gameEdge = edgeObject.GetComponent<MonoEdge>();
            gameEdge.ParentNodeAsGameNode = startNode;
            return gameEdge;
        }

        public void ChangeWeight(MonoEdge edge, EGameEdgeWeight newWeight)
        {
            VisualEdgeConfig
                .Where(e => e.ConfigForWeight == newWeight)
                .First()
                .ApplyTo(edge);
        }

    }
}