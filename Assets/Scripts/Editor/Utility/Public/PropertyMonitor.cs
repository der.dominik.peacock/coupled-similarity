﻿
using System;
using System.Collections.Generic;
using UniRx;
using UnityEditor;

namespace CoupledSimilarity.Editor.Utility
{
    public class PropertyMonitor
    {
        private List<PropertyChangeAction> ChangeActions = new List<PropertyChangeAction>();

        public IDisposable SubscribeForObject<T>(SerializedProperty propertyToListenFor, Action<T> onChangeCallback)
        {
            return Subscribe<T>(
                propertyToListenFor, 
                onChangeCallback, 
                e => e.objectReferenceValue
                );
        }
        public IDisposable SubscribeForEnum<T>(SerializedProperty propertyToListenFor, Action<T> onChangeCallback)
        {
            return Subscribe<T>(
                propertyToListenFor,
                onChangeCallback,
                e => e.enumValueIndex
                );
        }

        public IDisposable Subscribe<T>(
            SerializedProperty propertyToListenFor, 
            Action<T> onChangeCallback, 
            Func<SerializedProperty, object> valueSelectorCallback)
        {
            PropertyChangeAction changeAction = new PropertyChangeAction(
                    propertyToListenFor,
                    e => onChangeCallback((T)e),
                    valueSelectorCallback
                    );
            ChangeActions.Add(
                changeAction
                );
            return Disposable.Create(
                () => ChangeActions.Remove(changeAction)
                );
        }

        public void PollForModifications(SerializedObject serializedObject)
        {
            ChangeActions.ForEach(
                e => e.PollForModification(serializedObject)
                );
        }
    }
}
