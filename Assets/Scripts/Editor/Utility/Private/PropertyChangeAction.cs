﻿
using System;
using UnityEngine;
using UnityEditor;

namespace CoupledSimilarity.Editor.Utility
{
    internal class PropertyChangeAction
    {
        private SerializedProperty Property;
        private Action<object> OnChangeCallback;
        private Func<SerializedProperty, object> ValueSelecterCallback;

        internal PropertyChangeAction(SerializedProperty property, Action<object> onChangeCallback, Func<SerializedProperty, object> valueSelecterCallback)
        {
            Property = property;
            OnChangeCallback = onChangeCallback;
            ValueSelecterCallback = valueSelecterCallback;
        }

        internal void PollForModification(SerializedObject serializedObject)
        {
            // important: do this first otherwise oldValue receives the updated value
            object oldValue = ValueSelecterCallback(Property);

            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(Property);
            serializedObject.ApplyModifiedProperties();

            if (EditorGUI.EndChangeCheck())
            {
                OnChangeCallback(oldValue);
            }
        }

    }
}
