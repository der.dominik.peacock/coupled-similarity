﻿
using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.UI;
using CoupledSimilarity.Game.Utility;

using UnityEngine;

namespace CoupledSimilarity.Game.Cheats
{
    public class FinishInternalMovesAsDefenderPlayerInitialisation : ButtonPlayerInitialisation<FinishInternalMovesAsDefenderModel>
    {
        public FinishInternalMovesAsDefenderPlayerInitialisation(Prefab viewPrefab, UIRootReferencer rootReferencer, ConjunctiveSpecialisedGameInitialiser conjunctiveInitialiser)
            :
            base(
                () => CreateButtonView(viewPrefab, rootReferencer), 
                (_, __, defenderMoveRules) => CreateModel(defenderMoveRules)
                )
        {
            conjunctiveInitialiser.Add(this);
        }
        private static ButtonView CreateButtonView(Prefab viewPrefab, UIRootReferencer rootReferencer)
        {
            ButtonView result = viewPrefab.Instantiate().GetComponent<ButtonView>();
            result.transform.SetParent(rootReferencer.GameModeSpecificActionsRoot.transform);
            return result;
        }
        private static FinishInternalMovesAsDefenderModel CreateModel(IRules defenderMoveRules)
        {
            return new FinishInternalMovesAsDefenderModel(defenderMoveRules);
        }
    }
}

