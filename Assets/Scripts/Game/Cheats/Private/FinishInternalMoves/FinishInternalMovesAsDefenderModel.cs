﻿
using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.UI;

namespace CoupledSimilarity.Game.Cheats
{
    public class FinishInternalMovesAsDefenderModel : IButtonModel
    {
        private readonly IRules DefenderGameRules;

        public FinishInternalMovesAsDefenderModel(IRules defenderGameRules)
        {
            DefenderGameRules = defenderGameRules;
        }

        public void PerformAction()
        {
            DefenderGameRules.FinishInternalMovesAsDefender();
        }
    }
}


