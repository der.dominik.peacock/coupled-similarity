﻿
using CoupledSimilarity.Game.Rules;

namespace CoupledSimilarity.Game.Player
{
    public interface IEntityFactory
    {
        IPlayerEntity CreateAttacker();
        IPlayerEntity CreateDefender();
    }
}