﻿
using CoupledSimilarity.Game.Utility;

namespace CoupledSimilarity.Game.Player
{
    public interface IEntitySpawnConfig
    {
        Prefab AttackerPrefab { get; }
        Prefab DefenderPrefab { get; }
        float MovementSpeed { get; }
    }
}
