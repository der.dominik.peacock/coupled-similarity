﻿
using CoupledSimilarity.Game.Rules;

namespace CoupledSimilarity.Game.Player
{
    public class FPlayerEntity : IPlayerEntity
    {
        public IPlayerLogic PlayerLogic { get; }
        public IMovementLogic MovementLogic { get; }

        public FPlayerEntity(IPlayerLogic playerLogic, IMovementLogic movementLogic)
        {
            PlayerLogic = playerLogic;
            MovementLogic = movementLogic;
        }
    }
}

