﻿
using CoupledSimilarity.Game.Utility;
using UnityEngine;

namespace CoupledSimilarity.Game.Player
{
    [CreateAssetMenu(fileName = "EntitySpawnConfig", menuName = "Config/EntitySpawnConfig", order = 1)]
    public class FEntitySpawnConfig : ScriptableObject, IEntitySpawnConfig
    {
        public Prefab _AttackerPrefab;
        public Prefab AttackerPrefab => _AttackerPrefab;

        public Prefab _DefenderPrefab;
        public Prefab DefenderPrefab => _DefenderPrefab;

        public float _MovementSpeed = 0.1f;
        public float MovementSpeed => _MovementSpeed;
    }
}
