﻿
using UnityEngine;
using UniRx;

using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Player.AI;
using CoupledSimilarity.Game.Utility;

namespace CoupledSimilarity.Game.Player
{
    public class EntityFactory : IEntityFactory
    {
        private readonly IEntitySpawnConfig SpawnConfig;
        private readonly IEventDispenser EventDispenser;
        private readonly CompositeDisposable Disposer;
        private readonly IRaycaster2D Raycaster;

        public EntityFactory(IEntitySpawnConfig spawnConfig, IEventDispenser eventDispenser, CompositeDisposable disposer, IRaycaster2D raycaster)
        {
            SpawnConfig = spawnConfig;
            EventDispenser = eventDispenser;
            Disposer = disposer;
            Raycaster = raycaster;
        }

        public IPlayerEntity CreateAttacker()
        {
            GameObject physicalObject = SpawnConfig.AttackerPrefab.Instantiate();

            TweenMovementLogic movementLogic = new TweenMovementLogic(
                physicalObject.transform,
                SpawnConfig.MovementSpeed,
                EventDispenser,
                Disposer
                );

            HumanPlayerLogic humanLogic = new HumanPlayerLogic(
                    () => movementLogic.CurrentNode,
                    EventDispenser,
                    Raycaster
                    );
            return new FPlayerEntity(
                humanLogic,
                movementLogic
                );
        }

        public IPlayerEntity CreateDefender()
        {
            GameObject physicalObject = SpawnConfig.DefenderPrefab.Instantiate();

            TweenMovementLogic movementLogic = new TweenMovementLogic(
                physicalObject.transform,
                SpawnConfig.MovementSpeed,
                EventDispenser,
                Disposer
                );

            AiPlayerLogic aiPlayerLogic = new AiPlayerLogic(movementLogic);
            return new FPlayerEntity(
                aiPlayerLogic,
                movementLogic
                );
        }
    }
}
