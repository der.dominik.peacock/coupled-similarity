﻿
using UnityEngine;

using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;

namespace CoupledSimilarity.Game.Player.AI
{
    public static class ArenaEdgeExtensions
    {
        public static bool LeadsToGamePosition(this IGraphEdge<FArenaNodeValue, FArenaEdgeValue> edge, IGameState gameState)
        {
            FArenaNodeValue nodeValue = edge.TargetNode.Value;
            IGraphNode<Transform, EGameEdgeWeight> attackerPositionAfterMove = nodeValue.AttackerPosition;
            IGraphNode<Transform, EGameEdgeWeight> defenderPositionAfterMove = nodeValue.DefenderPosition;

            IGraphNode<Transform, EGameEdgeWeight> actualAttackerPosition = gameState.Attacker.MovementLogic.CurrentNode;
            IGraphNode<Transform, EGameEdgeWeight> actualDefenderPosition = gameState.Defender.MovementLogic.CurrentNode;

            return attackerPositionAfterMove == actualAttackerPosition
                && defenderPositionAfterMove == actualDefenderPosition;
        }
    }
}
