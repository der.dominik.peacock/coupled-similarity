﻿
using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Utility;

namespace CoupledSimilarity.Game.Player.AI
{
    public class FArenaEdgeValue
    {
        public readonly IOptional<IAiArenaMove> PossibleMoveForAI;
        public readonly EMoveType MoveType;

        public FArenaEdgeValue(IOptional<IAiArenaMove> possibleMoveForAI, EMoveType moveType)
        {
            PossibleMoveForAI = possibleMoveForAI;
            MoveType = moveType;
        }

        public override string ToString()
        {
            return "FArenaEdgeValue[" + MoveType + "]";
        }
    }
}
