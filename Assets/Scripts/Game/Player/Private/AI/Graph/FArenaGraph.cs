﻿
using System.Linq;
using UniRx;

using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;

namespace CoupledSimilarity.Game.Player.AI
{
    public class FArenaGraph
    {
        public IGraphNode<FArenaNodeValue, FArenaEdgeValue> CurrentGamePositionInArena { get; set; }

        public FArenaGraph(IGraphNode<FArenaNodeValue, FArenaEdgeValue> root, IGameState gameState)
        {
            CurrentGamePositionInArena = root;
            gameState.OnGameMove
                .Subscribe(
                    move => UpdateGamePositionOnPlayerMove(gameState)
                );
        }
        private void UpdateGamePositionOnPlayerMove(IGameState gameState)
        {
            CurrentGamePositionInArena = CurrentGamePositionInArena.Neighbours
                .Where(possibleMove => possibleMove.LeadsToGamePosition(gameState))
                .Select(possibleMove => possibleMove.TargetNode)
                .First();
        }
    }
}
