﻿
using UnityEngine;

using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;

namespace CoupledSimilarity.Game.Player.AI
{
    public class FArenaNodeValue
    {
        public EWinningRegion WinningRegion { get; set; } = EWinningRegion.Undetermined;

        public readonly EPlayerType PlayerWhoseTurnItIs;
        public readonly IGraphNode<Transform, EGameEdgeWeight> AttackerPosition;
        public readonly IGraphNode<Transform, EGameEdgeWeight> DefenderPosition;

        public FArenaNodeValue(
            EPlayerType playerWhoseTurnItIs,
            IGraphNode<Transform, EGameEdgeWeight> attackerPosition,
            IGraphNode<Transform, EGameEdgeWeight> defenderPosition
            )
        {
            PlayerWhoseTurnItIs= playerWhoseTurnItIs;
            AttackerPosition = attackerPosition;
            DefenderPosition = defenderPosition;
        }

        public override string ToString()
        {
            return $"FArenaNodeValue[PlayerWhoseTurnItIs:{PlayerWhoseTurnItIs}, AttackerPosition:{AttackerPosition}, DefenderPosition:{DefenderPosition}, WinningRegion:{WinningRegion}]";
        }
        public override bool Equals(object obj)
        {
            FArenaNodeValue other = obj as FArenaNodeValue;
            return other != null
                && PlayerWhoseTurnItIs == other.PlayerWhoseTurnItIs
                && AttackerPosition == other.AttackerPosition
                && DefenderPosition == other.DefenderPosition;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
