﻿
namespace CoupledSimilarity.Game.Player.AI
{
    public enum EWinningRegion
    {
        Undetermined,
        Attacker,
        Defender
    }
}
