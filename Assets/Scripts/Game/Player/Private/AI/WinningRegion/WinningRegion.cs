﻿
using System.Collections.Generic;
using System.Linq;

using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;
using UnityEngine;

namespace CoupledSimilarity.Game.Player.AI
{
    using ArenaNode = IGraphNode<FArenaNodeValue, FArenaEdgeValue>;
    public static class WinningRegion
    {
        public static void ComputeWinningRegions(ArenaNode rootNode)
        {
            // Based on compute_winning_region at https://coupledsim.bbisping.de/bisping_computingCoupledSimilarity_thesis.pdf
            HashSet<ArenaNode> allNodes = GraphSearch.FilterConnectedNodes(
                rootNode, 
                node => node.Value.WinningRegion = EWinningRegion.Defender,
                IsDefenderTurn
                );
            HashSet<ArenaNode> propagatedNodes = new HashSet<ArenaNode>();
            foreach (ArenaNode graphNode in allNodes)
            {
                IGraphNode<Transform, EGameEdgeWeight> attackerPos = graphNode.Value.AttackerPosition;
                IGraphNode<Transform, EGameEdgeWeight> defenderPos = graphNode.Value.DefenderPosition;
                if (graphNode.Neighbours.Count == 0)
                {
                    PropagateAttackerWin(graphNode, propagatedNodes);
                }
            }
        }
        private static bool IsDefenderTurn(ArenaNode node)
        {
            return node.Value.PlayerWhoseTurnItIs == EPlayerType.Defender;
        }

        private static void PropagateAttackerWin(ArenaNode node, HashSet<ArenaNode> propagatedNodes)
        {
            bool bWasPropagatedBefore = propagatedNodes.Contains(node);
            if (!bWasPropagatedBefore)
            {
                propagatedNodes.Add(node);
                node.Value.WinningRegion = EWinningRegion.Attacker;
                foreach (IGraphNode<FArenaNodeValue, FArenaEdgeValue> parentNode in node.ParentNodesOfIncomingEdges)
                {
                    bool bIsAttackerTurn = parentNode.Value.PlayerWhoseTurnItIs == EPlayerType.Attacker;
                    if (bIsAttackerTurn
                        || AreAllSuccessorsInAttackerWinningRegion(parentNode))
                    {
                        PropagateAttackerWin(parentNode, propagatedNodes);
                    }
                }
            }
        }
        private static bool AreAllSuccessorsInAttackerWinningRegion(ArenaNode node)
        {
            return node.Neighbours
                       .Select(edge => edge.TargetNode.Value.WinningRegion)
                       .All(region => region == EWinningRegion.Attacker);
        }
        
    }
}
