﻿
using System;
using System.Collections.Generic;
using System.Linq;

using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Utility;
using UniRx;
using UnityEngine;
using Random = System.Random;

namespace CoupledSimilarity.Game.Player.AI
{
    public class AiPlayerLogic : IPlayerLogic
    {
        private static IOptional<IAiArenaMove> nullMove = Optional.FromNull<IAiArenaMove>();

        private readonly IMovementLogic MovementLogic;
        private IDisposable MovementSubscription = null;
        public FArenaGraph Arena { get; private set; }

        public AiPlayerLogic(IMovementLogic movementLogic)
        {
            MovementLogic = movementLogic;
        }

        public void OnStartNewGame(IGameState gameState, IRules playerMoveRules)
        {
            Arena = new FArenaGraph(
                ArenaGraph.GenerateArenaFrom(
                    gameState.AttackerStartNode,
                    gameState.DefenderStartNode,
                    gameState.GameTypeBeingPlayed,
                    playerMoveRules
                    ),
                gameState
                );
            WinningRegion.ComputeWinningRegions(Arena.CurrentGamePositionInArena);
        }

        public void StartTurn()
        {
            PerformNextMove();
        }
        private void PerformNextMove()
        {
            IOptional<IAiArenaMove> aiMove = nullMove;
            aiMove = FindDirectMoveToDefenderWinningRegion(Arena.CurrentGamePositionInArena.Neighbours, aiMove);
            aiMove = FindPathToDefenderWinningRegionWhenNoDirectMove(Arena.CurrentGamePositionInArena, aiMove);
            aiMove = TakeRandomMoveWhenNoPathToDefenderWinningRegion(Arena.CurrentGamePositionInArena, aiMove);

            aiMove.Handle(
                move => move.PerformMove(),
                () => throw new InvalidOperationException(
                    $"The defender arena node '{Arena.CurrentGamePositionInArena}' contained an edge with a non-existent ai move. All outgoing defender edge must always have a non-null ai move. Was the arena graph incorrectly generated?")
            );

            PerformAnotherMoveIfIsDefenderTurnAgain();
        }

        private static IOptional<IAiArenaMove> FindDirectMoveToDefenderWinningRegion(
            IReadOnlyList<IGraphEdge<FArenaNodeValue, FArenaEdgeValue>> arenaNodeNeighbours,
            IOptional<IAiArenaMove> aiMove)
        {
            // ReSharper disable once ReplaceWithSingleCallToFirstOrDefault
            IGraphEdge<FArenaNodeValue, FArenaEdgeValue> edgeToDefenderWinningRegion = arenaNodeNeighbours
                .Where(e => e.TargetNode.Value.WinningRegion == EWinningRegion.Defender)
                // TODO: Select a neighbour in a clever way
                .FirstOrDefault();

            if (edgeToDefenderWinningRegion != null)
            {
                Debug.Log("AI chose direct move to winning region.");
                aiMove = edgeToDefenderWinningRegion.EdgeWeight.PossibleMoveForAI;
            }

            return aiMove;
        }
        private static IOptional<IAiArenaMove> FindPathToDefenderWinningRegionWhenNoDirectMove(
            IGraphNode<FArenaNodeValue, FArenaEdgeValue> currentNode,
            IOptional<IAiArenaMove> aiMove)
        {
            if (!aiMove.IsPresent())
            {
                List<FGraphPath<FArenaNodeValue, FArenaEdgeValue>> pathsToDefenderWinningRegion 
                    = currentNode.FindShortestPathsToArea(
                        AiEdgeExtensions.ComputeWeight,
                        node => node.Value.WinningRegion == EWinningRegion.Defender
                    );
                if (pathsToDefenderWinningRegion.Count > 0)
                {
                    Debug.Log($"AI chose shortest path to winning region. Total number of paths: {pathsToDefenderWinningRegion.Count}");
                    return SelectShortestPath(currentNode, pathsToDefenderWinningRegion)
                        .StartEdge
                        .EdgeWeight
                        .PossibleMoveForAI;
                }
            }

            return aiMove;
        }

        private static FGraphPath<FArenaNodeValue, FArenaEdgeValue> SelectShortestPath(
            IGraphNode<FArenaNodeValue, FArenaEdgeValue> currentNode,
            List<FGraphPath<FArenaNodeValue, FArenaEdgeValue>> pathList)
        {
            int indexWithMostBranches = 0;
            int hightestNumberOfBranches = 0;
            for (int listIndex = 0; listIndex < pathList.Count; ++listIndex)
            {
                Debug.Log($"{listIndex+1} {convertToString(pathList[listIndex])}");
                int currentNumberOfBranches = 0;

                for (int pathIndex = 0; pathIndex < pathList[listIndex].NodeOrder.Count; ++pathIndex)
                {
                    IGraphNode<FArenaNodeValue, FArenaEdgeValue> node = pathList[listIndex].NodeOrder[pathIndex];
                    if (node.Value.PlayerWhoseTurnItIs == EPlayerType.Defender)
                    {
                        currentNumberOfBranches += node.Neighbours.Count;
                    }
                }

                if (currentNumberOfBranches > hightestNumberOfBranches)
                {
                    hightestNumberOfBranches = currentNumberOfBranches;
                    indexWithMostBranches = listIndex;
                }
            }

            return pathList[indexWithMostBranches];
        }

        private static string convertToString(FGraphPath<FArenaNodeValue, FArenaEdgeValue> path)
        {
            string edgeSequenceString = "";
            for (int i = 0; i < path.NodeOrder.Count; ++i)
            {
                var firstNode = path.NodeOrder[i];
                
                edgeSequenceString +=
                    $"({firstNode.Value.AttackerPosition},{firstNode.Value.DefenderPosition})";
                /*if (i != path.NodeOrder.Count - 1)
                {
                    var secondNode = path.NodeOrder[i + 1];
                    var edge = firstNode.GetEdgeTo(secondNode);
                    edgeSequenceString += $" ={edge.EdgeWeight.MoveType}=> ";
                }*/
            }

            return edgeSequenceString;
        }
        private static IOptional<IAiArenaMove> TakeRandomMoveWhenNoPathToDefenderWinningRegion(
            IGraphNode<FArenaNodeValue, FArenaEdgeValue> currentNode,
            IOptional<IAiArenaMove> aiMove)
        {
            if (!aiMove.IsPresent())
            {
                int maxIndex = currentNode.Neighbours.Count - 1;
                if (maxIndex < 0)
                {
                    return aiMove;
                }

                Debug.Log("AI chose random move.");
                return currentNode.Neighbours[UnityEngine.Random.Range(0, maxIndex)]
                    .EdgeWeight
                    .PossibleMoveForAI;
            }
            return aiMove;
        }
        private void PerformAnotherMoveIfIsDefenderTurnAgain()
        {
            bool bIsStillDefenderTurn =
                Arena.CurrentGamePositionInArena.Value.PlayerWhoseTurnItIs == EPlayerType.Defender;
            bool bHasNotSubscribedForMovement = MovementSubscription == null;
            if (bIsStillDefenderTurn
                && bHasNotSubscribedForMovement)
            {
                MovementSubscription = MovementLogic.OnReachTargetNode.Subscribe(
                    node => PerformNextMove()
                );
            }
        }
        public void EndTurn()
        {
            if (MovementSubscription != null)
            {
                MovementSubscription.Dispose();
                MovementSubscription = null;
            }
        }
    }
}
