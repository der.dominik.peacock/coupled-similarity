﻿
using UnityEngine;

using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;

namespace CoupledSimilarity.Game.Player.AI
{
    public class MoveToNeighbourArenaMove : AbstractArenaMove
    {
        private readonly IGraphEdge<Transform, EGameEdgeWeight> EdgeToMoveOver;

        public MoveToNeighbourArenaMove(IRules gameRules, IGraphEdge<Transform, EGameEdgeWeight> edgeToMoveOver) 
            : base(gameRules)
        {
            EdgeToMoveOver = edgeToMoveOver;
        }

        public override void PerformMove()
        {
            GameRules.MoveOverEdge(EdgeToMoveOver);
        }
    }
}
