﻿
using CoupledSimilarity.Game.Rules;

namespace CoupledSimilarity.Game.Player.AI
{
    public class FinishInternalMovesArenaMove : AbstractArenaMove
    {
        public FinishInternalMovesArenaMove(IRules gameRules) 
            : base(gameRules)
        {}

        public override void PerformMove()
        {
            GameRules.FinishInternalMovesAsDefender();
        }
    }
}
