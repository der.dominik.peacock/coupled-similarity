﻿
namespace CoupledSimilarity.Game.Player.AI
{
    public interface IAiArenaMove
    {
        void PerformMove();
    }
}
