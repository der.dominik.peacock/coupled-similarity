﻿
using CoupledSimilarity.Game.Rules;

namespace CoupledSimilarity.Game.Player.AI
{
    public abstract class AbstractArenaMove : IAiArenaMove
    {
        protected readonly IRules GameRules;

        public AbstractArenaMove(IRules gameRules)
        {
            GameRules = gameRules;
        }

        public abstract void PerformMove();
    }
}
