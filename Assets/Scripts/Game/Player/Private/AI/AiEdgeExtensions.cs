﻿
using UnityEngine;

using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Utility;

namespace CoupledSimilarity.Game.Player.AI
{
    public static class AiEdgeExtensions
    {
        public static EBreadthFirstSearchEdgeWeight ComputeWeight(
            IGraphNode<FArenaNodeValue, FArenaEdgeValue> fromNode,
            IGraphEdge<FArenaNodeValue, FArenaEdgeValue> overEdge)
        {
            bool bDoesMoveEndTurnForCurrentPlayer =
                fromNode.Value.PlayerWhoseTurnItIs != overEdge.TargetNode.Value.PlayerWhoseTurnItIs;
            return bDoesMoveEndTurnForCurrentPlayer 
                ? EBreadthFirstSearchEdgeWeight.UnitWeight : EBreadthFirstSearchEdgeWeight.ZeroWeight;
        }

        public static IOptional<IGraphEdge<Transform, EGameEdgeWeight>> GetPhysicalEdgeMovedOver(
            this IGraphNode<FArenaNodeValue, FArenaEdgeValue> fromNode,
            IGraphEdge<FArenaNodeValue, FArenaEdgeValue> overEdge)
        {
            if (overEdge.EdgeWeight.MoveType != EMoveType.MoveOverEdge)
            {
                return Optional.FromNull<IGraphEdge<Transform, EGameEdgeWeight>>();
            }

            IGraphNode<Transform, EGameEdgeWeight> startNode = fromNode.Value.AttackerPosition;
            IGraphNode<Transform, EGameEdgeWeight> endNode = overEdge.TargetNode.Value.AttackerPosition;
            if (fromNode.Value.PlayerWhoseTurnItIs == EPlayerType.Defender)
            {
                startNode = fromNode.Value.DefenderPosition;
                endNode = overEdge.TargetNode.Value.DefenderPosition;
            }

            return Optional.FromValue(
                startNode.GetEdgeTo(endNode)
                );
        }
    }
}
