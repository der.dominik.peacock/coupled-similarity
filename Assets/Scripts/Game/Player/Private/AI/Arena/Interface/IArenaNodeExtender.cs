﻿
using System;

namespace CoupledSimilarity.Game.Player.AI
{
    public interface IArenaNodeExtender
    {
        void ExtendArenaNode(FEnqueuedNodeExtension enqueuedNodeExtension, Action<FArenaNodeValue, FArenaEdgeValue> subsequentGameState);
    }
}
