﻿
using UnityEngine;

using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Utility;

namespace CoupledSimilarity.Game.Player.AI
{
    public class FEnqueuedNodeExtension
    {
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> NodeToExtend;
        public readonly IOptional<FEnqueuedNodeExtension> ParentExtension;

        public EPlayerType PlayerWhoseTurnItIs => NodeToExtend.Value.PlayerWhoseTurnItIs;
        public IOptional<FNativeGraphEdge<FArenaNodeValue, FArenaEdgeValue>> EdgeFromPreviousGamePositionToNodeToExtend
        {
            get
            {
                return ParentExtension.Map(
                    previousGamePosition => (FNativeGraphEdge<FArenaNodeValue, FArenaEdgeValue>)previousGamePosition.NodeToExtend.GetEdgeTo(NodeToExtend)
                    );
            }
        }

        public EArenaMoveStage CurrentArenaMoveStage { get; }
        private IOptional<IGraphEdge<Transform, EGameEdgeWeight>> LastEdgeAttackerMovedOver = null;

        public FEnqueuedNodeExtension(
            FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> nodeToExtend,
            IOptional<FEnqueuedNodeExtension> parentExtension)
        {
            Debug.Assert(nodeToExtend != null);

            NodeToExtend = nodeToExtend;
            ParentExtension = parentExtension;
            CurrentArenaMoveStage = EArenaMoveStage.None;
        }
        public FEnqueuedNodeExtension(
            FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> nodeToExtend,
            FNativeGraphEdge<FArenaNodeValue, FArenaEdgeValue> edgeLeadingToThisState,
            FEnqueuedNodeExtension parentExtension,
            EGameType gameTypeBeingPlayed
            )
        {
            Debug.Assert(nodeToExtend != null);

            NodeToExtend = nodeToExtend;
            ParentExtension = Optional.FromValue(parentExtension);
            CurrentArenaMoveStage = ComputeMoveStageForTransitioningBetweenSubsequentStates(
                parentExtension.NodeToExtend.Value,
                nodeToExtend.Value,
                edgeLeadingToThisState.EdgeWeight,
                gameTypeBeingPlayed,
                parentExtension.CurrentArenaMoveStage
                );
        }

        private enum EMoveStageComputationMode
        {
            ForWeakBisimulation,
            ForCoupledSimulation
        }
        public EArenaMoveStage ComputeMoveStageForMovingToNextState(
            FArenaNodeValue nodeChildValue,
            FArenaEdgeValue edgeValueBetweenStates,
            EGameType gameTypeBeingPlayed)
        {
            return ComputeMoveStageForTransitioningBetweenSubsequentStates(
                pastState: NodeToExtend.Value,
                currentState: nodeChildValue,
                edgeValueBetweenStates: edgeValueBetweenStates, 
                gameTypeBeingPlayed, 
                CurrentArenaMoveStage
                );
        }
        private EArenaMoveStage ComputeMoveStageForTransitioningBetweenSubsequentStates(
            FArenaNodeValue pastState,
            FArenaNodeValue currentState,
            FArenaEdgeValue edgeValueBetweenStates,
            EGameType gameTypeBeingPlayed,
            EArenaMoveStage pastArenaMoveStage
            )
        {
            EMoveStageComputationMode computationMode = gameTypeBeingPlayed == EGameType.CoupledSimulation
                ? EMoveStageComputationMode.ForCoupledSimulation : EMoveStageComputationMode.ForWeakBisimulation;
            if (currentState.PlayerWhoseTurnItIs == EPlayerType.Attacker)
            {
                return EArenaMoveStage.None;
            }

            switch (pastArenaMoveStage)
            {
                case EArenaMoveStage.None:
                    if(edgeValueBetweenStates.MoveType == EMoveType.StartSymmetrySwapAsAttacker
                        && computationMode == EMoveStageComputationMode.ForCoupledSimulation)
                    {
                        return EArenaMoveStage.MakeSecondInternaMoves;
                    }
                    Debug.Assert(edgeValueBetweenStates.MoveType == EMoveType.MoveOverEdge 
                        || (edgeValueBetweenStates.MoveType == EMoveType.StartSymmetrySwapAsAttacker && computationMode == EMoveStageComputationMode.ForWeakBisimulation)
                        );
                    return EArenaMoveStage.MakeFirstInternalMoves;

                case EArenaMoveStage.MakeFirstInternalMoves:
                    IGraphEdge<Transform, EGameEdgeWeight> edgeToNewState = pastState.DefenderPosition.GetEdgeTo(currentState.DefenderPosition);
                    Debug.Assert(edgeToNewState != null);
                    bool bDidDefenderJustSimulateAttacker = edgeToNewState.EdgeWeight == GetLastEdgeAttackerMovedOver().Value.EdgeWeight;
                    if (bDidDefenderJustSimulateAttacker)
                    {
                        return EArenaMoveStage.MakeSecondInternaMoves;
                    }
                    return EArenaMoveStage.MakeFirstInternalMoves;

                case EArenaMoveStage.MakeSecondInternaMoves:
                    return EArenaMoveStage.MakeSecondInternaMoves;

                default:
                    throw new System.InvalidOperationException();
            }
        }

        public IOptional<FNativeGraphEdge<FArenaNodeValue, FArenaEdgeValue>> GetLastAttackerMove()
        {
            return ParentExtension.Map(
                parentExtension =>
                {
                    if(parentExtension.NodeToExtend.Value.PlayerWhoseTurnItIs == EPlayerType.Attacker)
                    {
                        return EdgeFromPreviousGamePositionToNodeToExtend;
                    }
                    return parentExtension.GetLastAttackerMove();
                }
                );
        }
        public IOptional<IGraphEdge<Transform, EGameEdgeWeight>> GetLastEdgeAttackerMovedOver()
        {
            if (LastEdgeAttackerMovedOver == null)
            {
                LastEdgeAttackerMovedOver = ParentExtension.Map(
                    parentExtension =>
                    {
                        if (DidAttackerMoveOverEdge(parentExtension))
                        {
                            return Optional.FromValue(
                                GetAttackerEdgeFromParentToCurrent(parentExtension)
                                );
                        }
                        return parentExtension.GetLastEdgeAttackerMovedOver();
                    }
                    );
            }
            return LastEdgeAttackerMovedOver;
        }
        private IGraphEdge<Transform, EGameEdgeWeight> GetAttackerEdgeFromParentToCurrent(FEnqueuedNodeExtension parentExtension)
        {
            IGraphNode<Transform, EGameEdgeWeight> parentAttackerPosition = parentExtension.NodeToExtend.Value.AttackerPosition;
            IGraphNode<Transform, EGameEdgeWeight> currentAttackerPosition = NodeToExtend.Value.AttackerPosition;

            IGraphEdge<Transform, EGameEdgeWeight> edgeAttackerMovedOver = parentAttackerPosition.GetEdgeTo(currentAttackerPosition);
            Debug.Assert(edgeAttackerMovedOver != null, "The attacker did not move over any edge last turn.");
            return edgeAttackerMovedOver;
        }
        private bool DidAttackerMoveOverEdge(FEnqueuedNodeExtension parentExtension)
        {
            FArenaNodeValue parentNodeValue = parentExtension.NodeToExtend.Value;
            EMoveType previousMoveType = EdgeFromPreviousGamePositionToNodeToExtend.Value.EdgeWeight.MoveType;
            return parentNodeValue.PlayerWhoseTurnItIs == EPlayerType.Attacker
                            && previousMoveType == EMoveType.MoveOverEdge;
        }
    }
}
