﻿
namespace CoupledSimilarity.Game.Player.AI
{
    public enum  EArenaMoveStage
    {
        None                = 0,

        // Defender
        MakeFirstInternalMoves    = 100,
        MakeSecondInternaMoves    = 101
    }
}
