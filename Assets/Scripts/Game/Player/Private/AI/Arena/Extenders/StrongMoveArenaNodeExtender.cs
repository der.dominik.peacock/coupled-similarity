﻿
using System;
using System.Linq;
using UnityEngine;

using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Utility;

namespace CoupledSimilarity.Game.Player.AI
{
    public class StrongMoveArenaNodeExtender : IArenaNodeExtender
    {
        private delegate FArenaNodeValue NodeFactoryMethod(FEnqueuedNodeExtension currentNodeExtension, IGraphEdge<Transform, EGameEdgeWeight> childOfExtendedNode, IGraphNode<Transform, EGameEdgeWeight> notExtendedNode);
        private delegate FArenaEdgeValue EdgeFactoryMethod(FEnqueuedNodeExtension currentNodeExtension, IGraphEdge<Transform, EGameEdgeWeight> childOfExtendedNode, IGraphNode<Transform, EGameEdgeWeight> notExtendedNode);

        protected readonly IRules DefenderGameRules;

        public StrongMoveArenaNodeExtender(IRules defenderGameRules)
        {
            DefenderGameRules = defenderGameRules;
        }

        public virtual void ExtendArenaNode(FEnqueuedNodeExtension queuedNodeExtension, Action<FArenaNodeValue, FArenaEdgeValue> subsequentGameState)
        {
            FArenaNodeValue extendedNodeValue                       = queuedNodeExtension.NodeToExtend.Value;
            IGraphNode<Transform, EGameEdgeWeight> nodeToExtend     = extendedNodeValue.AttackerPosition;
            IGraphNode<Transform, EGameEdgeWeight> nodeNotToExtend  = extendedNodeValue.DefenderPosition;
            NodeFactoryMethod createNextArenaNode                   = CreateNode_WhenAttackerMoves; 
            EdgeFactoryMethod createCorrespondingArenaEdge          = CreateEdge_WhenAttackerMoves;

            if (extendedNodeValue.PlayerWhoseTurnItIs == EPlayerType.Defender)
            {
                nodeToExtend                    = extendedNodeValue.DefenderPosition;
                nodeNotToExtend                 = extendedNodeValue.AttackerPosition;
                createNextArenaNode             = CreateNode_WhenDefenderMoves;
                createCorrespondingArenaEdge    = CreateEdge_WhenDefenderMoves;
            }
            
            nodeToExtend.Neighbours
                .Where(edgeToCheck => extendedNodeValue.PlayerWhoseTurnItIs == EPlayerType.Attacker 
                    || CanDefenderMoveOverEdge(queuedNodeExtension, edgeToCheck))
                .ForEach(
                    childEdge => subsequentGameState(
                        createNextArenaNode(queuedNodeExtension, childEdge, nodeNotToExtend),
                        createCorrespondingArenaEdge(queuedNodeExtension, childEdge, nodeNotToExtend)
                        )
                );
        }

        protected virtual bool CanDefenderMoveOverEdge(FEnqueuedNodeExtension queuedNodeExtension, IGraphEdge<Transform, EGameEdgeWeight> edgeToCheck)
        {
            IOptional<IGraphEdge<Transform, EGameEdgeWeight>> lastEdgeWeightAttackerMovedOver = queuedNodeExtension.GetLastEdgeAttackerMovedOver();
            if(!lastEdgeWeightAttackerMovedOver.IsPresent())
            {
                throw new InvalidOperationException("Tried to extend defender node although attacker has not moved over any edge. This happens only in coupled simulation when the attacker performs symmetry swap before any non-internal move. Handle this case in the coupled simulation node extender class.");
            }
            return lastEdgeWeightAttackerMovedOver.Value.EdgeWeight == edgeToCheck.EdgeWeight;
        }

        protected virtual FArenaNodeValue CreateNode_WhenAttackerMoves(FEnqueuedNodeExtension currentNodeExtension, IGraphEdge<Transform, EGameEdgeWeight> childOfExtendedNode, IGraphNode<Transform, EGameEdgeWeight> notExtendedNode)
        {
            return new FArenaNodeValue(EPlayerType.Defender, attackerPosition: childOfExtendedNode.TargetNode, defenderPosition: notExtendedNode);
        }
        protected virtual FArenaNodeValue CreateNode_WhenDefenderMoves(FEnqueuedNodeExtension currentNodeExtension, IGraphEdge<Transform, EGameEdgeWeight> childOfExtendedNode, IGraphNode<Transform, EGameEdgeWeight> notExtendedNode)
        {
            return new FArenaNodeValue(EPlayerType.Attacker, attackerPosition: notExtendedNode, defenderPosition: childOfExtendedNode.TargetNode);
        }

        private static FArenaEdgeValue EmptyMoveValue = new FArenaEdgeValue(Optional.FromNull<IAiArenaMove>(), EMoveType.MoveOverEdge);
        private FArenaEdgeValue CreateEdge_WhenAttackerMoves(FEnqueuedNodeExtension currentNodeExtension, IGraphEdge<Transform, EGameEdgeWeight> edgeAttackerMovedOver, IGraphNode<Transform, EGameEdgeWeight> defenderPosition)
        {
            return EmptyMoveValue;
        }
        private FArenaEdgeValue CreateEdge_WhenDefenderMoves(FEnqueuedNodeExtension currentNodeExtension, IGraphEdge<Transform, EGameEdgeWeight> edgeDefenderMovedOver, IGraphNode<Transform, EGameEdgeWeight> attackerPosition)
        {
            return new FArenaEdgeValue(
                Optional.FromValue<IAiArenaMove>(
                    new MoveToNeighbourArenaMove(
                        DefenderGameRules, 
                        edgeDefenderMovedOver
                        )
                    ),
                EMoveType.MoveOverEdge
                );
        }
    }
}