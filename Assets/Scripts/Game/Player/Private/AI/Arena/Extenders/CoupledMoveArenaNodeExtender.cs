﻿
using System;

using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Utility;

namespace CoupledSimilarity.Game.Player.AI
{
    public class CoupledMoveArenaNodeExtender : WeakMoveArenaNodeExtender
    {
        public CoupledMoveArenaNodeExtender(IRules defenderGameRules)
            : base(defenderGameRules)
        {}

        protected override void CreateFinishInternalMovesAsDefenderNode(FEnqueuedNodeExtension queuedNodeExtension, Action<FArenaNodeValue, FArenaEdgeValue> subsequentGameState)
        {
            EMoveType lastAttackerMoveType = queuedNodeExtension.GetLastAttackerMove().Value.EdgeWeight.MoveType;
            if (lastAttackerMoveType == EMoveType.StartSymmetrySwapAsAttacker)
            {
                subsequentGameState(
                    new FArenaNodeValue(EPlayerType.Attacker, queuedNodeExtension.NodeToExtend.Value.DefenderPosition, queuedNodeExtension.NodeToExtend.Value.AttackerPosition),
                    new FArenaEdgeValue(Optional.FromValue<IAiArenaMove>(new FinishInternalMovesArenaMove(DefenderGameRules)), EMoveType.FinishInternalMovesAsDefender)
                    );
            }
            else
            {
                base.CreateFinishInternalMovesAsDefenderNode(queuedNodeExtension, subsequentGameState);
            }
        }
    }
}