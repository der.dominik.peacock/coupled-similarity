﻿
using System;

using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Utility;

namespace CoupledSimilarity.Game.Player.AI
{
    public class CoupledSymmetrySwapNodeExtender : StrongSymmetrySwapNodeExtender, IArenaNodeExtender
    {
        protected override bool CanReverseSymmetrySwaps => false;
        protected override void CreateSymmetrySwapNode(FEnqueuedNodeExtension enqueuedNodeExtension, Action<FArenaNodeValue, FArenaEdgeValue> subsequentGameState)
        {
            FArenaNodeValue extendedNodeValue = enqueuedNodeExtension.NodeToExtend.Value;
            subsequentGameState(
                new FArenaNodeValue(
                        EPlayerType.Defender,
                        extendedNodeValue.AttackerPosition,
                        extendedNodeValue.DefenderPosition
                        ),
                new FArenaEdgeValue(
                    Optional.FromNull<IAiArenaMove>(),
                    EMoveType.StartSymmetrySwapAsAttacker
                    )
                );
        }
    }
}
