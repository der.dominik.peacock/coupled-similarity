﻿
using System;
using UnityEngine;

using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Utility;

namespace CoupledSimilarity.Game.Player.AI
{
    public class WeakMoveArenaNodeExtender : StrongMoveArenaNodeExtender, IArenaNodeExtender
    {
        public WeakMoveArenaNodeExtender(IRules defenderGameRules)
            : base(defenderGameRules)
        {}

        public override void ExtendArenaNode(FEnqueuedNodeExtension queuedNodeExtension, Action<FArenaNodeValue, FArenaEdgeValue> subsequentGameState)
        {
            EArenaMoveStage currentMoveStage = queuedNodeExtension.CurrentArenaMoveStage;
            if (currentMoveStage == EArenaMoveStage.MakeSecondInternaMoves)
            {
                CreateFinishInternalMovesAsDefenderNode(queuedNodeExtension, subsequentGameState);
            }
            base.ExtendArenaNode(queuedNodeExtension, subsequentGameState);
        }
        protected virtual void CreateFinishInternalMovesAsDefenderNode(FEnqueuedNodeExtension queuedNodeExtension, Action<FArenaNodeValue, FArenaEdgeValue> subsequentGameState)
        {
            subsequentGameState(
                new FArenaNodeValue(EPlayerType.Attacker, queuedNodeExtension.NodeToExtend.Value.AttackerPosition, queuedNodeExtension.NodeToExtend.Value.DefenderPosition),
                new FArenaEdgeValue(Optional.FromValue<IAiArenaMove>(new FinishInternalMovesArenaMove(DefenderGameRules)), EMoveType.FinishInternalMovesAsDefender)
                );
        }

        protected override bool CanDefenderMoveOverEdge(FEnqueuedNodeExtension queuedNodeExtension, IGraphEdge<Transform, EGameEdgeWeight> edgeToCheck)
        {
            EArenaMoveStage currentMoveStage = queuedNodeExtension.CurrentArenaMoveStage;
            return edgeToCheck.EdgeWeight == EGameEdgeWeight.InternalMove
                || (currentMoveStage == EArenaMoveStage.MakeFirstInternalMoves 
                    && base.CanDefenderMoveOverEdge(queuedNodeExtension, edgeToCheck));
        }
        protected override FArenaNodeValue CreateNode_WhenDefenderMoves(FEnqueuedNodeExtension currentNodeExtension, IGraphEdge<Transform, EGameEdgeWeight> edgeDefenderMovedOver, IGraphNode<Transform, EGameEdgeWeight> attackerPosition)
        {
            return new FArenaNodeValue(EPlayerType.Defender, attackerPosition: attackerPosition, defenderPosition: edgeDefenderMovedOver.TargetNode);
        }
    }
}