﻿
using System;

using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Utility;

namespace CoupledSimilarity.Game.Player.AI
{
    public class StrongSymmetrySwapNodeExtender : IArenaNodeExtender
    {
        protected virtual bool CanReverseSymmetrySwaps => true;

        public void ExtendArenaNode(FEnqueuedNodeExtension enqueuedNodeExtension, Action<FArenaNodeValue, FArenaEdgeValue> subsequentGameState)
        {
            if (!IsAttackerTurn(enqueuedNodeExtension))
            {
                return;
            }

            if (CanReverseSymmetrySwaps
                && DidAttackerJustSymmetrySwap(enqueuedNodeExtension))
            {
                CreateEdgeForReversingSymmetrySwap(enqueuedNodeExtension);
            }
            else
            {
                CreateSymmetrySwapNode(enqueuedNodeExtension, subsequentGameState);
            }
        }
        private bool IsAttackerTurn(FEnqueuedNodeExtension nodeToExtend)
        {
            return nodeToExtend.NodeToExtend.Value.PlayerWhoseTurnItIs == EPlayerType.Attacker;
        }
        private bool DidAttackerJustSymmetrySwap(FEnqueuedNodeExtension enqueuedNodeExtension)
        {
            IOptional<FNativeGraphEdge<FArenaNodeValue, FArenaEdgeValue>> lastEdgeUsed = enqueuedNodeExtension.EdgeFromPreviousGamePositionToNodeToExtend;
            return lastEdgeUsed.IsPresent()
                && lastEdgeUsed.Value.EdgeWeight.MoveType == EMoveType.StartSymmetrySwapAsAttacker;
        }
        private void CreateEdgeForReversingSymmetrySwap(FEnqueuedNodeExtension enqueuedNodeExtension)
        {
            enqueuedNodeExtension.NodeToExtend.AddNeighbour(
                enqueuedNodeExtension.ParentExtension.Value.NodeToExtend,
                new FArenaEdgeValue(
                    Optional.FromNull<IAiArenaMove>(),
                    EMoveType.StartSymmetrySwapAsAttacker
                    )
                );
        }
        protected virtual void CreateSymmetrySwapNode(FEnqueuedNodeExtension enqueuedNodeExtension, Action<FArenaNodeValue, FArenaEdgeValue> subsequentGameState)
        {
            FArenaNodeValue extendedNodeValue = enqueuedNodeExtension.NodeToExtend.Value;
            subsequentGameState(
                new FArenaNodeValue(
                        EPlayerType.Attacker,
                        extendedNodeValue.DefenderPosition,
                        extendedNodeValue.AttackerPosition
                        ),
                new FArenaEdgeValue(
                    Optional.FromNull<IAiArenaMove>(),
                    EMoveType.StartSymmetrySwapAsAttacker
                    )
                );
        }
    }
}
