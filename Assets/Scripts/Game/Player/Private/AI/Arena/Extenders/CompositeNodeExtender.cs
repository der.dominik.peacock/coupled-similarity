﻿
using System;
using System.Collections.Generic;

namespace CoupledSimilarity.Game.Player.AI
{
    public class CompositeNodeExtender : IArenaNodeExtender
    {
        private List<IArenaNodeExtender> Children = new List<IArenaNodeExtender>();

        public void AddNodeExtender(IArenaNodeExtender extenderToAdd)
        {
            Children.Add(extenderToAdd);
        }

        public void ExtendArenaNode(FEnqueuedNodeExtension enqueuedNodeExtension, Action<FArenaNodeValue, FArenaEdgeValue> subsequentGameState)
        {
            Children.ForEach(
                e => e.ExtendArenaNode(enqueuedNodeExtension, subsequentGameState)
                );
        }
    }
}
