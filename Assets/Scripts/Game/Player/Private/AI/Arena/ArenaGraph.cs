﻿
using System.Collections.Generic;
using UnityEngine;

using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Utility;

namespace CoupledSimilarity.Game.Player.AI
{
    public static class ArenaGraph
    {
        private class FNodeCombinationMetaData
        {
            public readonly EPlayerType PlayerWhoseTurnItIs;
            public readonly EArenaMoveStage MoveStage;

            public FNodeCombinationMetaData(EPlayerType playerWhoseTurnItIs, EArenaMoveStage moveStage)
            {
                PlayerWhoseTurnItIs = playerWhoseTurnItIs;
                MoveStage = moveStage;
            }

            public override bool Equals(object obj)
            {
                return obj is FNodeCombinationMetaData data 
                    && PlayerWhoseTurnItIs == data.PlayerWhoseTurnItIs 
                    && MoveStage == data.MoveStage;
            }

            public override int GetHashCode()
            {
                var hashCode = 1587203780;
                hashCode = hashCode * -1521134295 + PlayerWhoseTurnItIs.GetHashCode();
                hashCode = hashCode * -1521134295 + MoveStage.GetHashCode();
                return hashCode;
            }
        }

        public static IGraphNode<FArenaNodeValue, FArenaEdgeValue> GenerateArenaFrom(
            IGraphNode<Transform, EGameEdgeWeight> initialAttackerNode, 
            IGraphNode<Transform, EGameEdgeWeight> initialDefenderNode,
            EGameType gameType,
            IRules defenderGameRules
            )
        {
            IArenaNodeExtender rulesToGenerateFor = ArenaNodeExtenderSelector.GetArenaNodeExtenderFor(gameType, defenderGameRules);
            ExtensionMap<IGraphNode<Transform, EGameEdgeWeight>, FNodeCombinationMetaData, FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>> extensionMap = new ExtensionMap<IGraphNode<Transform, EGameEdgeWeight>, FNodeCombinationMetaData, FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>>();

            Queue<FEnqueuedNodeExtension> nodesToInspect = new Queue<FEnqueuedNodeExtension>();
            FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> rootNode = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                new FArenaNodeValue(EPlayerType.Attacker, initialAttackerNode, initialDefenderNode)
                );
            FEnqueuedNodeExtension rootNodeExtension = new FEnqueuedNodeExtension(rootNode, Optional.FromNull<FEnqueuedNodeExtension>());
            extensionMap.MarkCombination(
                initialAttackerNode, 
                initialDefenderNode,
                // EArenaMoveStage.None might need to change if the way it's computed is changed.
                new FNodeCombinationMetaData(EPlayerType.Attacker, EArenaMoveStage.None),  
                rootNode
                );
            nodesToInspect.Enqueue(
                rootNodeExtension
                );

            // Do depth first search to find all possible state permutations
            while (nodesToInspect.Count > 0)
            {
                FEnqueuedNodeExtension queuedNodeExtension = nodesToInspect.Dequeue();
                rulesToGenerateFor.ExtendArenaNode(
                    queuedNodeExtension,
                    (nodeChildValue, edgeValue) =>
                    {
                        FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> childNode = null;
                        FNodeCombinationMetaData nodeComboData = new FNodeCombinationMetaData(
                            nodeChildValue.PlayerWhoseTurnItIs,
                            queuedNodeExtension.ComputeMoveStageForMovingToNextState(nodeChildValue, edgeValue, gameType)
                            );

                        if (!extensionMap.IsCombinationMarked(nodeChildValue.AttackerPosition, nodeChildValue.DefenderPosition, nodeComboData, out childNode))
                        {
                            childNode = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                                nodeChildValue
                                );
                            FNativeGraphEdge<FArenaNodeValue, FArenaEdgeValue> edgeLeadingToNewExtension 
                                = queuedNodeExtension.NodeToExtend.AddNeighbour(childNode, edgeValue);

                            extensionMap.MarkCombination(nodeChildValue.AttackerPosition, nodeChildValue.DefenderPosition, nodeComboData, childNode);
                            nodesToInspect.Enqueue(
                                new FEnqueuedNodeExtension(childNode, edgeLeadingToNewExtension, queuedNodeExtension, gameType)
                                );
                        }
                        else
                        {
                            queuedNodeExtension.NodeToExtend.AddNeighbour(childNode, edgeValue);
                        }
                    }
                    );
            }

            return rootNode;
        }
    }
}