﻿
using System;
using CoupledSimilarity.Game.Rules;

namespace CoupledSimilarity.Game.Player.AI
{
    public class CoupledSimulationArenaNodeExtender : IArenaNodeExtender
    {
        private CompositeNodeExtender SubActions = new CompositeNodeExtender();

        public CoupledSimulationArenaNodeExtender(IRules defenderGameRules)
        {
            SubActions.AddNodeExtender(
                new CoupledMoveArenaNodeExtender(defenderGameRules)
                );
            SubActions.AddNodeExtender(
                new CoupledSymmetrySwapNodeExtender()
                );
        }

        public void ExtendArenaNode(FEnqueuedNodeExtension enqueuedNodeExtension, Action<FArenaNodeValue, FArenaEdgeValue> subsequentGameState)
        {
            SubActions.ExtendArenaNode(enqueuedNodeExtension, subsequentGameState);
        }
    }
}
