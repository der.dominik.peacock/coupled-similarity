﻿
using System;
using CoupledSimilarity.Game.Rules;

namespace CoupledSimilarity.Game.Player.AI
{
    public class BisimulationArenaNodeExtender : IArenaNodeExtender
    {
        private CompositeNodeExtender SubActions = new CompositeNodeExtender();

        public BisimulationArenaNodeExtender(IRules defenderGameRules)
        {
            SubActions.AddNodeExtender(
                new StrongMoveArenaNodeExtender(defenderGameRules)
                );
            SubActions.AddNodeExtender(
                new StrongSymmetrySwapNodeExtender()
                );
        }

        public void ExtendArenaNode(FEnqueuedNodeExtension enqueuedNodeExtension, Action<FArenaNodeValue, FArenaEdgeValue> subsequentGameState)
        {
            SubActions.ExtendArenaNode(enqueuedNodeExtension, subsequentGameState);
        }
    }
}
