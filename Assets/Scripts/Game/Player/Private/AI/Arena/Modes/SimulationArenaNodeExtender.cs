﻿
using System;
using CoupledSimilarity.Game.Rules;

namespace CoupledSimilarity.Game.Player.AI
{
    public class SimulationArenaNodeExtender : IArenaNodeExtender
    {
        private CompositeNodeExtender SubActions = new CompositeNodeExtender();

        public SimulationArenaNodeExtender(IRules defenderGameRules)
        {
            SubActions.AddNodeExtender(
                new StrongMoveArenaNodeExtender(defenderGameRules)
                );
        }

        public void ExtendArenaNode(FEnqueuedNodeExtension enqueuedNodeExtension, Action<FArenaNodeValue, FArenaEdgeValue> subsequentGameState)
        {
            SubActions.ExtendArenaNode(enqueuedNodeExtension, subsequentGameState);
        }
    }
}
