﻿
using System;
using CoupledSimilarity.Game.Rules;

namespace CoupledSimilarity.Game.Player.AI
{
    public class WeakBisimulationArenaNodeExtender : IArenaNodeExtender
    {
        private CompositeNodeExtender SubActions = new CompositeNodeExtender();

        public WeakBisimulationArenaNodeExtender(IRules defenderGameRules)
        {
            SubActions.AddNodeExtender(
                new WeakMoveArenaNodeExtender(defenderGameRules)
                );
            SubActions.AddNodeExtender(
                new StrongSymmetrySwapNodeExtender()
                );
        }

        public void ExtendArenaNode(FEnqueuedNodeExtension enqueuedNodeExtension, Action<FArenaNodeValue, FArenaEdgeValue> subsequentGameState)
        {
            SubActions.ExtendArenaNode(enqueuedNodeExtension, subsequentGameState);
        }
    }
}
