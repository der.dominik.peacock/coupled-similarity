﻿
using CoupledSimilarity.Game.Rules;

namespace CoupledSimilarity.Game.Player.AI
{
    public static class ArenaNodeExtenderSelector
    {
        public static IArenaNodeExtender GetArenaNodeExtenderFor(EGameType gameType, IRules defenderGameRules)
        {
            switch (gameType)
            {
                case EGameType.Simulation:
                    return new SimulationArenaNodeExtender(defenderGameRules);
                case EGameType.Bisimulation:
                    return new BisimulationArenaNodeExtender(defenderGameRules);
                case EGameType.WeakBisimulation:
                    return new WeakBisimulationArenaNodeExtender(defenderGameRules);
                case EGameType.CoupledSimulation:
                    return new CoupledSimulationArenaNodeExtender(defenderGameRules);
                default:
                    throw new System.ArgumentException(string.Format("gameType '{0}' is out of bounds", gameType));
            }
        }
    }
}