﻿
using System;
using UnityEngine;
using UniRx;

using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Utility;

namespace CoupledSimilarity.Game.Player
{
    using Node = IGraphNode<Transform, EGameEdgeWeight>;
    using Edge = IGraphEdge<Transform, EGameEdgeWeight>;
    public class HumanPlayerLogic : IPlayerLogic
    {
        private readonly Func<Node> GetNodePosition;
        private readonly IEventDispenser EventDispenser;
        private readonly IRaycaster2D Raycaster;

        private IRules RulesToUse;
        private IDisposable OnPressScreenSubscription;

        public HumanPlayerLogic(Func<Node> getNodePosition, IEventDispenser eventDispenser, IRaycaster2D raycaster)
        {
            GetNodePosition = getNodePosition;
            EventDispenser = eventDispenser;
            Raycaster = raycaster;
        }
        public void OnStartNewGame(IGameState gameState, IRules gameRules)
        {
            RulesToUse = gameRules;
        }
        public void StartTurn()
        {
            // This might need to be added to the Level's CompositeDisposable
            OnPressScreenSubscription = EventDispenser.OnPressScreen
                .Subscribe(OnPressScreen);
        }
        public void EndTurn()
        {
            OnPressScreenSubscription.Dispose();
            OnPressScreenSubscription = null;
        }

        private void OnPressScreen(Vector3 pressPosition)
        {
            FHitResult2D hitResult = Raycaster.RaycastAtScreenPosition(pressPosition);
            Node clickedNode = GetNodeOrNull(hitResult);
            MoveToNodeIfNotNull(clickedNode);
        }
        private Node GetNodeOrNull(FHitResult2D hitResult)
        {
            return hitResult.HitCollider != null ? 
                hitResult.HitCollider.gameObject.FindNode<Transform, EGameEdgeWeight>() 
                :
                null;
        }
        private void MoveToNodeIfNotNull(Node node)
        {
            if(node != null)
            {
                Edge overEdge = GetNodePosition().GetEdgeTo(node);
                if(overEdge != null)
                {
                    RulesToUse.MoveOverEdge(overEdge);
                }
            }
        }
    }
}
