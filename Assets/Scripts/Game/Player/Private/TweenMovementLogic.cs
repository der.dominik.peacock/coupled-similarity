﻿
using System;
using UniRx;
using UnityEngine;

using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Utility;

namespace CoupledSimilarity.Game.Rules
{
    using Node = IGraphNode<Transform, EGameEdgeWeight>;
    public class TweenMovementLogic : IMovementLogic
    {
        private readonly float MovementPerSecond;

        private bool bIsPositionDirty = false;

        private event Action<Node> OnReachTargetNodeEvent;
        public IObservable<Node> OnReachTargetNode { get; }
        public Node CurrentNode { get; set; }
        public Transform EntityTransform { get; }

        public TweenMovementLogic(Transform entityTransform, float movementPerSecond, IEventDispenser eventDispenser, CompositeDisposable lifeCycle)
        {
            OnReachTargetNode = Observable.FromEvent<Node>(
                sub => OnReachTargetNodeEvent += sub,
                sub => OnReachTargetNodeEvent -= sub
            );
            EntityTransform = entityTransform;
            MovementPerSecond = movementPerSecond;
            eventDispenser.OnUpdate
                .Where(_ => bIsPositionDirty)
                .Subscribe(_ => UpdatePosition())
                .AddTo(lifeCycle);
        }

        public void UpdatePosition()
        {
            Vector3 currentLocation = EntityTransform.position;
            Vector3 targetLocation = CurrentNode.Value.position;
            float deltaMovement = MovementPerSecond * Time.deltaTime;

            EntityTransform.position = Vector3.Lerp(currentLocation, targetLocation, deltaMovement);
            bIsPositionDirty = !currentLocation.IsNearlyEqualTo(targetLocation);

            if (!bIsPositionDirty)
            {
                OnReachTargetNodeEvent?.Invoke(CurrentNode);
            }
        }

        public void MoveTo(Node nodeToMoveTo)
        {
            CurrentNode = nodeToMoveTo;
            bIsPositionDirty = true;
        }
        public void TeleportTo(Node nodeToTeleportTo)
        {
            CurrentNode = nodeToTeleportTo;
            bIsPositionDirty = false;
            EntityTransform.position = CurrentNode.Value.position;
        }
    }
}

