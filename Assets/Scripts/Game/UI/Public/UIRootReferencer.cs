﻿
using UnityEngine;

namespace CoupledSimilarity.Game.UI
{
    /**
     * References all important UI roots.
     */
    public class UIRootReferencer : MonoBehaviour
    {
        /**
         * All buttons for game mode specific actions, such as symmetry swaps, are attached to this.
         */
        public GameObject GameModeSpecificActionsRoot;
    }
}

