﻿
using System;
using UniRx;

using CoupledSimilarity.Game.Rules;

namespace CoupledSimilarity.Game.UI
{
    public interface IGameTypeSelectorView : IShowableUI
    {
        IObservable<EGameType> OnSelectGameType { get; }
    }
}