﻿
using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

using CoupledSimilarity.Game.Rules;

namespace CoupledSimilarity.Game.UI
{
    public class GameTypeSelectorView : MonoBehaviour, IGameTypeSelectorView
    {
        public GameObject ParentPanel;
        public Button BisimulationButton;
        public Button WeakBisimulationButton;
        public Button CoupledSimulationButton;

        private event Action<EGameType> OnSelectGameTypeEvent;
        public IObservable<EGameType> OnSelectGameType { get; }

        public GameTypeSelectorView()
        {
            OnSelectGameType = Observable.FromEvent<EGameType>(
                e => OnSelectGameTypeEvent += e,
                e => OnSelectGameTypeEvent -= e
                );
        }

        public void Show()
        {
            ParentPanel.SetActive(true);
        }

        public void Hide()
        {
            ParentPanel.SetActive(false);
        }

        private void Awake()
        {
            BisimulationButton.OnClickAsObservable()
                      .Subscribe(_ => OnSelectGameTypeEvent?.Invoke(EGameType.Bisimulation));
            WeakBisimulationButton.OnClickAsObservable()
                   .Subscribe(_ => OnSelectGameTypeEvent?.Invoke(EGameType.WeakBisimulation));
            CoupledSimulationButton.OnClickAsObservable()
                .Subscribe(_ => OnSelectGameTypeEvent?.Invoke(EGameType.CoupledSimulation));
        }
    }
}