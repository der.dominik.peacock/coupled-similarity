﻿
using UniRx;

namespace CoupledSimilarity.Game.UI
{
    public class GameTypeSelectorController
    {
        public GameTypeSelectorController(IGameTypeSelectorView view, GameTypeSelectorModel model)
        {
            view.OnSelectGameType.Subscribe(
                e =>
                {
                    model.SelectGameType(e);
                    view.Hide();
                }
                );
        }
    }
}