﻿
using CoupledSimilarity.Game.Rules;

namespace CoupledSimilarity.Game.UI
{
    public class GameTypeSelectorModel 
    {
        private readonly GameMode GameModeToStart;

        public GameTypeSelectorModel(GameMode gameModeToStart)
        {
            GameModeToStart = gameModeToStart;
        }

        public void SelectGameType(EGameType gameType)
        {
            GameModeToStart.SelectSimulationGameType(gameType);
        }
    }
}