﻿
using System;
using System.Linq;
using UnityEngine;

using CoupledSimilarity.Game.Graph;

namespace CoupledSimilarity.Game.UI
{
    public class ScreenResizer : MonoBehaviour
    {
        private MonoNode[] AllNodes;
        private int PreviousScreenHeight;
        private int PreviousScreenWidth;

        public float HeightPadding = 1.0f;
        public float WidthPadding = 2.0f;

        private void Awake()
        {
            AllNodes = FindObjectsOfType<MonoNode>();
            ResizeScreen();
        }
        private void Update()
        {
            if(Screen.height != PreviousScreenHeight
                || Screen.width != PreviousScreenWidth)
            {
                ResizeScreen();
            }

            PreviousScreenHeight = Screen.height;
            PreviousScreenWidth = Screen.width;
        }

        private void ResizeScreen()
        {
            if (AllNodes.Length <= 1)
            {
                throw new InvalidOperationException("This level must contains at least two nodes in order to recompute screen size.");
            }

            float minX = float.MaxValue;
            float maxX = float.MinValue;
            float minY = float.MaxValue;
            float maxY = float.MinValue;
            foreach (MonoNode e in AllNodes)
            {
                minX = Mathf.Min(minX, e.transform.position.x);
                maxX = Mathf.Max(maxX, e.transform.position.x);
                minY = Mathf.Min(minY, e.transform.position.y);
                maxY = Mathf.Max(maxY, e.transform.position.y);
            }

            float widthRequired = maxX - minX;
            float totalWidthRequired = widthRequired + 2.0f * WidthPadding;
            // Don't let the IDE fool you: these casts are not redudant
            float aspectRatio = (float)Screen.height / (float)Screen.width;
            float minHeightRequired = maxY - minY + HeightPadding;
            Camera.main.orthographicSize = Math.Max(
                minHeightRequired * 0.5f,
                // https://answers.unity.com/questions/760671/resizing-orthographic-camera-to-fit-2d-sprite-on-s.html
                totalWidthRequired * 0.5f * aspectRatio
                );
        }
    }
}

