﻿
using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Utility;

namespace CoupledSimilarity.Game.UI
{
    public class SymmetrySwapPlayerInitialisation : ButtonPlayerInitialisation<SymmetrySwapModel>
    {
        public SymmetrySwapPlayerInitialisation(Prefab viewPrefab, UIRootReferencer rootReferencer)
            :
            base(
                () => CreateButtonView(viewPrefab, rootReferencer),
                (_, attackerMoveRules, __) => CreateModel(attackerMoveRules)
                )
        {}
        private static ButtonView CreateButtonView(Prefab viewPrefab, UIRootReferencer rootReferencer)
        {
            ButtonView result = viewPrefab.Instantiate().GetComponent<ButtonView>();
            result.transform.SetParent(rootReferencer.GameModeSpecificActionsRoot.transform);
            return result;
        }
        private static SymmetrySwapModel CreateModel(IRules attackerMoveRules)
        {
            return new SymmetrySwapModel(attackerMoveRules);
        }
    }
}

