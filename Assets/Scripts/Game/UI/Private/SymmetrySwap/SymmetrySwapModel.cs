﻿
using CoupledSimilarity.Game.Rules;

namespace CoupledSimilarity.Game.UI
{
    public class SymmetrySwapModel : IButtonModel
    {
        private readonly IRules AttackerGameRules;

        public SymmetrySwapModel(IRules attackerGameRules)
        {
            AttackerGameRules = attackerGameRules;
        }

        public void PerformAction()
        {
            AttackerGameRules.StartSymmetrySwapAsAttacker();
        }
    }
}


