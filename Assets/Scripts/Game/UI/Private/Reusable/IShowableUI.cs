﻿
using System;
using UniRx;

using CoupledSimilarity.Game.Rules;

namespace CoupledSimilarity.Game.UI
{
    public interface IShowableUI 
    {
        void Show();
        void Hide();
    }
}