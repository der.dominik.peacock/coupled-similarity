﻿
using System;

using CoupledSimilarity.Game.Rules;

namespace CoupledSimilarity.Game.UI
{
    public class ButtonPlayerInitialisation<Model> : ISpecialisedGameInitialiser where Model : IButtonModel
    {
        public delegate Model CreateModelFunctionType(IGameState gameState, IRules attackerMoveRules, IRules defenderMoveRules);

        private readonly Func<ButtonView> CreateButtonViewCallback;
        private readonly CreateModelFunctionType CreateModelCallback;

        private ButtonView Instance;
        private ButtonController ButtonController;

        public ButtonPlayerInitialisation(Func<ButtonView> createButtonViewCallback, CreateModelFunctionType createModelCallback)
        {
            CreateButtonViewCallback = createButtonViewCallback;
            CreateModelCallback = createModelCallback;
        }

        public void SetupNewGame(IGameState gameState, IRules attackerMoveRules, IRules defenderMoveRules)
        {
            ButtonView buttonView = GetOrCreateButtonView();
            ButtonController = ButtonController ?? new ButtonController(
                buttonView,
                CreateModelCallback(gameState, attackerMoveRules, defenderMoveRules)
                );
            buttonView.Show();
        }
        public void DisableSetupSystems(IGameState gameState)
        {
            GetOrCreateButtonView().Hide();
        }

        private ButtonView GetOrCreateButtonView()
        {
            if (Instance == null)
            {
                Instance = CreateButtonViewCallback();
            }
            return Instance;
        }
    }
}

