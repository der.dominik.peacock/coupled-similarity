﻿

using System;
using UniRx;

namespace CoupledSimilarity.Game.UI
{
    public interface IButtonView : IShowableUI
    {
        IObservable<Unit> OnPressButton { get; }
    }
}


