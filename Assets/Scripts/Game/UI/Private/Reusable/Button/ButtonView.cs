﻿

using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace CoupledSimilarity.Game.UI
{
    public class ButtonView : MonoBehaviour, IButtonView
    {
        public Button ActionButton;

        public IObservable<Unit> OnPressButton => ActionButton.OnClickAsObservable();

        public void Show()
        {
            gameObject.SetActive(true);
        }
        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}


