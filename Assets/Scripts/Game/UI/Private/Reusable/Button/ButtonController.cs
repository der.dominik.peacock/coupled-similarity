﻿
using System;
using UniRx;

namespace CoupledSimilarity.Game.UI
{
    public class ButtonController
    {
        public ButtonController(IButtonView view, IButtonModel model)
        {
            view.OnPressButton.Subscribe(
                _ => model.PerformAction()
                );
        }
    }
}


