﻿

using System;
using UniRx;

namespace CoupledSimilarity.Game.UI
{
    public interface IButtonModel
    {
        void PerformAction();
    }
}


