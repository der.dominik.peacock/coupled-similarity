
using Zenject;

using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.UI;

namespace CoupledSimilarity.Game.Launch
{
    public class NormalGameStartScript : MonoInstaller
    {
        private class StartGameScript : IInitializable
        {
            private readonly GameMode GameMode;
            private readonly IGameTypeSelectorView GameTypeSelectorView;

            public StartGameScript(GameMode gameMode, IGameTypeSelectorView gameTypeSelectorView)
            {
                GameMode = gameMode;
                GameTypeSelectorView = gameTypeSelectorView;
            }

            public void Initialize()
            {
                GameMode.PrepareNewMatch();
                GameTypeSelectorView.Show();
            }
        }

        public override void InstallBindings()
        {
            Container.BindInterfacesTo<StartGameScript>().AsSingle().NonLazy();
        }
    }
}
