
using Zenject;

using CoupledSimilarity.Game.Rules;

namespace CoupledSimilarity.Game.Launch
{
    public class SpecifiedGameStartScript : MonoInstaller
    {
        private class StartGameScript : IInitializable
        {
            private readonly GameMode GameMode;
            private readonly EGameType GameTypeToStart;

            public StartGameScript(GameMode gameMode, EGameType gameTypeToStart)
            {
                GameMode = gameMode;
                GameTypeToStart = gameTypeToStart;
            }

            public void Initialize()
            {
                GameMode.PrepareNewMatch();
                // Just for testing purpose
                GameMode.SelectSimulationGameType(GameTypeToStart);
            }
        }

        public EGameType GameTypeToStart;

        public override void InstallBindings()
        {
            Container.Bind<EGameType>().FromInstance(GameTypeToStart).WhenInjectedInto<StartGameScript>();
            Container.BindInterfacesTo<StartGameScript>().AsSingle().NonLazy();
        }
    }
}
