
using UniRx;
using Zenject;

namespace CoupledSimilarity.Game.Launch
{
    public class LevelInstaller : MonoInstaller
    {
        private CompositeDisposable LevelLifecycle = new CompositeDisposable();

        public override void InstallBindings()
        {
            Container.Bind<CompositeDisposable>().FromInstance(LevelLifecycle);

            UtilityInstaller.Install(Container);
        }
    }
}
