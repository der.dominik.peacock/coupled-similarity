
using Zenject;

using CoupledSimilarity.Game.UI;

namespace CoupledSimilarity.Game.Launch
{
    public class LevelUIMonoInstaller : MonoInstaller<LevelUIMonoInstaller>
    {
        public UIRootReferencer RootReferencer;

        public GameTypeSelectorView GameTypeSelctorView;

        public override void InstallBindings()
        {
            Container.Bind<UIRootReferencer>().FromInstance(RootReferencer);

            Container.Bind<IGameTypeSelectorView>().FromInstance(GameTypeSelctorView);
            Container.Bind<GameTypeSelectorModel>().ToSelf().AsSingle();
            Container.Bind<GameTypeSelectorController>().ToSelf().AsSingle().NonLazy();
        }
    }
}