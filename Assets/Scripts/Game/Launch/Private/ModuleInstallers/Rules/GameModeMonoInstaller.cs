using UnityEngine;
using Zenject;

using CoupledSimilarity.Game.Cheats;
using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Player;
using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.UI;
using CoupledSimilarity.Game.Utility;

namespace CoupledSimilarity.Game.Launch
{
    using Node = IGraphNode<Transform, EGameEdgeWeight>;
    public class GameModeMonoInstaller : MonoInstaller
    {
        private class FInjectifiedGameState : FGameState
        {
            public FInjectifiedGameState(
                IEntityFactory entityFactory,
                [Inject(Id = "attackerStart")]
                Node attackerStart,
                [Inject(Id = "defenderStart")]
                Node defenderStart) 
                    : base(entityFactory.CreateAttacker(), entityFactory.CreateDefender(), attackerStart, defenderStart)
            {}
        }
        private class ZenjectifiedMappedGameInitialisation : MappedGameInitialisation
        {
            public ZenjectifiedMappedGameInitialisation(
                [Inject(Id = EGameType.Simulation)]
                ISpecialisedGameInitialiser simulationInitialiser,
                [Inject(Id = EGameType.Bisimulation)]
                ISpecialisedGameInitialiser bisimulationInitialiser,
                [Inject(Id = EGameType.WeakBisimulation)]
                ISpecialisedGameInitialiser weakBisimulationInitialiser,
                [Inject(Id = EGameType.CoupledSimulation)]
                ISpecialisedGameInitialiser coupledSimilarityInitialiser)
                    : base(simulationInitialiser, bisimulationInitialiser, weakBisimulationInitialiser, coupledSimilarityInitialiser)
            { }
        }

        public MonoNode AttackerStartNode;
        public MonoNode DefenderStartNode;

        public Prefab SymmetrySwapViewPrefab;

        public override void InstallBindings()
        {
            // Binding ISpecialisedPlayerInitialisation
            ConjunctiveSpecialisedGameInitialiser sharedConjunctiveInitialiser = new ConjunctiveSpecialisedGameInitialiser();
            // SymmetrySwapPlayerInitialisation
            Container.Bind<Prefab>()
                .FromInstance(SymmetrySwapViewPrefab)
                .WhenInjectedInto<SymmetrySwapPlayerInitialisation>();
            Container.Bind<ConjunctiveSpecialisedGameInitialiser>()
                .FromInstance(sharedConjunctiveInitialiser)
                .WhenInjectedInto<SymmetrySwapPlayerInitialisation>();
            Container.Bind<SymmetrySwapPlayerInitialisation>()
                .ToSelf()
                .AsTransient()
                .OnInstantiated<SymmetrySwapPlayerInitialisation>((_, e) => sharedConjunctiveInitialiser.Add(e))
                .NonLazy();
            // FinishInternalMovesAsDefenderPlayerInitialisation
            if (CheatStatics.ShouldInitializeCheats())
            {
                Container.Bind<Prefab>()
                    .FromInstance(
                        Prefab.FromGameObjectInstance(
                            Resources.Load<GameObject>("Cheats/FinishInternalMovesAsDefender"))
                        )
                    .WhenInjectedInto<FinishInternalMovesAsDefenderPlayerInitialisation>();
                Container.Bind<ConjunctiveSpecialisedGameInitialiser>()
                    .FromInstance(sharedConjunctiveInitialiser)
                    .WhenInjectedInto<FinishInternalMovesAsDefenderPlayerInitialisation>();
                Container
                    .Bind<FinishInternalMovesAsDefenderPlayerInitialisation>()
                    .ToSelf()
                    .AsSingle()
                    .NonLazy();
            }

            // Binding IGameInitialisation
            Container.Bind<ISpecialisedGameInitialiser>()
                .WithId(EGameType.Simulation)
                .To<EmptySpecialisedGameInitialiser>()
                .AsTransient();
            Container.Bind<ISpecialisedGameInitialiser>()
                .WithId(EGameType.Bisimulation)
                .To<SymmetrySwapPlayerInitialisation>()
                .AsTransient();
            Container.Bind<ISpecialisedGameInitialiser>()
                .WithId(EGameType.WeakBisimulation)
                .FromMethod(
                    () => ConjunctiveSpecialisedGameInitialiser.CreateWithChild(sharedConjunctiveInitialiser)
                    )
                .AsTransient();
            Container.Bind<ISpecialisedGameInitialiser>()
                .WithId(EGameType.CoupledSimulation)
                .FromMethod(
                    () => ConjunctiveSpecialisedGameInitialiser.CreateWithChild(sharedConjunctiveInitialiser)
                    )
                .AsTransient();
            Container.Bind<IGameInitialisation>().To<ZenjectifiedMappedGameInitialisation>().AsSingle();

            // Binding GameMode
            Container.Bind<IInvalidMoveHandler>().To<DebugInvalidMoveHandler>().AsSingle();
            Container.Bind<IRuleFactory>().To<RuleFactory>().AsSingle();
            Container.Bind<Node>().WithId("attackerStart").FromInstance(AttackerStartNode);
            Container.Bind<Node>().WithId("defenderStart").FromInstance(DefenderStartNode);
            Container.Bind<FGameState>().To<FInjectifiedGameState>().AsTransient();
            Container.Bind<GameMode>().ToSelf().AsSingle().NonLazy();
        }
    }
}