
using Zenject;

using CoupledSimilarity.Game.Utility;

public class UtilityInstaller : Installer<UtilityInstaller>
{
    public override void InstallBindings()
    {
        Container.Bind<IRaycaster2D>().To<UnityRaycaster2D>().AsSingle();
        Container.Bind<IEventDispenser>().To<UniRxEventDispenser>().AsSingle();
    }
}