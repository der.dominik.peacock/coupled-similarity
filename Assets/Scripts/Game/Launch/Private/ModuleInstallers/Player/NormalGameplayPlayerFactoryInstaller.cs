using UnityEngine;
using Zenject;

using CoupledSimilarity.Game.Player;

namespace CoupledSimilarity.Game.Launch
{
    [CreateAssetMenu(fileName = "PlayerFactoryInstaller", menuName = "Installers/PlayerFactoryInstaller", order = 1)]
    public class NormalGameplayPlayerFactoryInstaller : ScriptableObjectInstaller<NormalGameplayPlayerFactoryInstaller>
    {
        public FEntitySpawnConfig SpawnConfig;

        public override void InstallBindings()
        {
            Container.Bind<IEntitySpawnConfig>().FromInstance(SpawnConfig);
            Container.Bind<IEntityFactory>().To<EntityFactory>().AsSingle();
        }
    }
}