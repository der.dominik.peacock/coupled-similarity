﻿
using System.Collections.Generic;

namespace CoupledSimilarity.Game.Utility
{
    public static class StringExtensions
    {
        public static string toCompactString<T>(this List<T> list)
        {
            string contents = "";
            bool bEmitComma = true;
            for(int i = 0; i < list.Count; ++i)
            {
                if (!bEmitComma)
                {
                    contents += ", ";
                }
                bEmitComma = false;
                contents += string.Format("{0} ", i);
                contents += list[i];
            }

            return string.Format("list[count:{0}, contents: {1}]", list.Count, contents);
        }
    }
}
