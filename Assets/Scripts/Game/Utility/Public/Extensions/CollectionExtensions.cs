﻿
using System;
using System.Collections.Generic;

namespace CoupledSimilarity.Game.Utility
{
    public static class CollectionExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> func)
        {
            foreach(T e in enumerable)
            {
                func(e);
            }
        }
    }
}
