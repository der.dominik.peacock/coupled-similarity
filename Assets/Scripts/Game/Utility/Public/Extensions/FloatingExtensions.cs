﻿
using System;

namespace CoupledSimilarity.Game.Utility
{
    public static class FloatingExtensions
    {
        public static bool IsNearlyZero(this double x)
        {
            return IsNearlyZero<double>(x, 0.001);
        }

        public static bool IsNearlyZero(this float x)
        {
            return IsNearlyZero<float>(x, 0.001f);
        }

        private static bool IsNearlyZero<T>(this T x, T precision) where T : IComparable
        {
            return x.CompareTo(precision) < 0;
        }
    }
}
