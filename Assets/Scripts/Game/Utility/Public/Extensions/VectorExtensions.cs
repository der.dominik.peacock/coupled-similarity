﻿
using UnityEngine;

namespace CoupledSimilarity.Game.Utility
{
    public static class VectorExtensions
    {
        public static bool IsNearlyEqualTo(this Vector3 first, Vector3 second)
        {
            return (first - second).sqrMagnitude < 1e-6;
        }
    }
}
