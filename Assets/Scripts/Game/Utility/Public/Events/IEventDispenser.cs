﻿
using System;
using UniRx;
using UnityEngine;

namespace CoupledSimilarity.Game.Utility
{
    public interface IEventDispenser
    {
        IObservable<bool> OnApplicationPause { get; }

        IObservable<Unit> OnUpdate { get; }
        IObservable<Unit> OnLateUpdate { get; }

        IObservable<Vector3> OnPressScreen { get; }
        IObservable<Vector3> OnReleaseScreen { get; }
    }
}
