﻿
using System;
using UniRx;
using UnityEngine;

namespace CoupledSimilarity.Game.Utility
{
    public class InputListener
    {
        private enum EMouseState
        {
            None    = 0,
            Down    = 1
        }
        private EMouseState PreviousMouseState = EMouseState.None;

        public readonly IObservable<Vector3> OnPressScreen;
        public readonly IObservable<Vector3> OnReleaseScreen;

        private event Action<Vector3> OnMouseDown;
        private event Action<Vector3> OnMouseUp;

        public InputListener(IObservable<Unit> OnUpdate)
        {
            OnPressScreen = Observable.FromEvent<Vector3>(
                subscriber => OnMouseDown += subscriber,
                subscriber => OnMouseDown -= subscriber
                );
            OnReleaseScreen = Observable.FromEvent<Vector3>(
                subscriber => OnMouseUp += subscriber,
                subscriber => OnMouseUp -= subscriber
                );

            OnUpdate.Subscribe(_ => Update());
        }

        private void Update()
        {
            switch(PreviousMouseState)
            {
                case EMouseState.None:
                    if(Input.GetMouseButtonDown(0))
                    {
                        OnMouseDown?.Invoke(Input.mousePosition);
                        PreviousMouseState = EMouseState.Down;
                    }
                    break;
                case EMouseState.Down:
                    if(Input.GetMouseButtonUp(0))
                    {
                        OnMouseUp?.Invoke(Input.mousePosition);
                        PreviousMouseState = EMouseState.None;
                    }
                    break;
            }
        }
    }
}
