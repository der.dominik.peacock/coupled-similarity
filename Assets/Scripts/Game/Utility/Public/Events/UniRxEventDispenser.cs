﻿
using System;
using UniRx;
using UnityEngine;

namespace CoupledSimilarity.Game.Utility
{
    public class UniRxEventDispenser : IEventDispenser
    {
        private readonly InputListener InputListener;

        public IObservable<bool> OnApplicationPause => MainThreadDispatcher.OnApplicationPauseAsObservable();

        public IObservable<Unit> OnUpdate => MainThreadDispatcher.UpdateAsObservable();
        public IObservable<Unit> OnLateUpdate => MainThreadDispatcher.LateUpdateAsObservable();

        public IObservable<Vector3> OnPressScreen => InputListener.OnPressScreen;
        public IObservable<Vector3> OnReleaseScreen => InputListener.OnReleaseScreen;

        public UniRxEventDispenser()
        {
            MainThreadDispatcher.Initialize();
            InputListener = new InputListener(OnUpdate);
        }
    }
}
