﻿
using System;

namespace CoupledSimilarity.Game.Utility
{
    public static class BooleanExtensions
    {
        public static void IfTrue(this bool condition, Action then)
        {
            if(condition)
            {
                then();
            }
        }
        public static void IfFalse(this bool condition, Action then)
        {
            if (!condition)
            {
                then();
            }
        }
    }
}
