﻿
using System;

namespace CoupledSimilarity.Game.Utility
{
    public interface IEither<A,B>
    {
        void Handle(Action<A> handleA, Action<B> handleB);
    }
}
