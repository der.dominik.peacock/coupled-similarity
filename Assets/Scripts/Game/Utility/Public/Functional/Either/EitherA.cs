﻿
using System;

namespace CoupledSimilarity.Game.Utility
{
    public class EitherA<A, B> : IEither<A, B>
    {
        private readonly A Value;

        public EitherA(A value)
        {
            Value = value;
        }

        public void Handle(Action<A> handleA, Action<B> handleB)
        {
            handleA(Value);
        }
    }
}
