﻿
using System;

namespace CoupledSimilarity.Game.Utility
{
    public class EitherB<A, B> : IEither<A, B>
    {
        private readonly B Value;

        public EitherB(B value)
        {
            Value = value;
        }

        public void Handle(Action<A> handleA, Action<B> handleB)
        {
            handleB(Value);
        }
    }
}
