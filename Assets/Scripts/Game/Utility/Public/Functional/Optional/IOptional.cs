﻿
using System;

namespace CoupledSimilarity.Game.Utility
{
    public interface IOptional<T>
    {
        T Value { get; }

        bool IsPresent();
        void IfPresent(Action<T> consumer);
        void Handle(Action<T> consumeIfPresent, Action ifNull);
    }
}
