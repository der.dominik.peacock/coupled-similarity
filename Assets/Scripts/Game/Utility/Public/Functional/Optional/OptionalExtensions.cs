﻿
using System;

namespace CoupledSimilarity.Game.Utility
{
    public static class Optional
    {
        public static IOptional<T> FromValue<T>(T value)
        {
            if(value == null)
            {
                throw new ArgumentException("Use FromNull instead for creating null values.");
            }
            return new FilledOptional<T>(value);
        }

        public static IOptional<T> FromNull<T>()
        {
            return new EmptyOptional<T>();
        }

        public static R Process<T, R>(this IOptional<T> optional, Func<T, R> consumerIfNotNull, Func<R> ifNull)
        {
            R result = default(R);
            optional.Handle(
                value => result = consumerIfNotNull(value),
                () => result = ifNull()
                );
            return result;
        }

        public static IOptional<R> Map<T, R>(this IOptional<T> mapFrom, Func<T, IOptional<R>> mappingFunction)
        {
            IOptional<R> result = null;
            mapFrom.Handle(
                tvalue => mappingFunction(tvalue).Handle(
                    rvalue => result = new FilledOptional<R>(rvalue),
                    () => result = new EmptyOptional<R>()
                    ),
                () => result = new EmptyOptional<R>()
                );
            return result;
        }
        public static IOptional<R> Map<T, R>(this IOptional<T> mapFrom, Func<T, R> mappingFunction)
        {
            IOptional<R> result = null;
            mapFrom.Handle(
                value => result = new FilledOptional<R>(mappingFunction(value)),
                () => result = new EmptyOptional<R>()
                );
            return result;
        }
    }
}
