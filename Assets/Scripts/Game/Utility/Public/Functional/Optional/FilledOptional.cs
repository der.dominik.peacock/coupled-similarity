﻿
using System;

namespace CoupledSimilarity.Game.Utility
{
    public class FilledOptional<T> : IOptional<T>
    {
        public T Value { get; }

        public FilledOptional(T value)
        {
            Value = value;
        }

        public bool IsPresent()
        {
            return true;
        }
        public void IfPresent(Action<T> consumer)
        {
            consumer(Value);
        }
        public void Handle(Action<T> consumeIfPresent, Action ifNull)
        {
            consumeIfPresent(Value);
        }
    }
}
