﻿
using System;

namespace CoupledSimilarity.Game.Utility
{
    public class EmptyOptional<T> : IOptional<T>
    {
        public T Value => throw new InvalidOperationException("This optional is empty.");

        public bool IsPresent()
        {
            return false;
        }
        public void IfPresent(Action<T> consumer)
        { }
        public void Handle(Action<T> consumeIfPresent, Action ifNull)
        {
            ifNull();
        }
    }
}
