﻿
using UniRx;

namespace CoupledSimilarity.Game.Player.AI
{
    public class ExtensionList<K, V>
    {
        private readonly ExtensionMap<K, V, Unit> Implementation = new ExtensionMap<K, V, Unit>();

        public void MarkCombination(K firstNode, K secondNode, V comboValue)
        {
            Implementation.MarkCombination(firstNode, secondNode, comboValue, Unit.Default);
        }
        public bool IsCombinationMarked(K firstNode, K secondNode, V comboValue)
        {
            return Implementation.IsCombinationMarked(firstNode, secondNode, comboValue, out _);
        }
    }
}
