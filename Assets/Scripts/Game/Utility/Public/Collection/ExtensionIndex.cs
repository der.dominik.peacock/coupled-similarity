﻿

namespace CoupledSimilarity.Game.Player.AI
{
    class ExtensionIndex
    {
        private int FirstNodeId;
        private int SecondNodeId;
        private int CombinationValue;

        public ExtensionIndex()
        { }
        public ExtensionIndex(int firstNodeId, int secondNodeId, int CombinationValue)
        {
            SetIds(firstNodeId, secondNodeId, CombinationValue);
        }

        public void SetIds(int firstNodeId, int secondNodeId, int combinationValue)
        {
            FirstNodeId = firstNodeId;
            SecondNodeId = secondNodeId;
            CombinationValue = combinationValue;
        }

        public override bool Equals(object obj)
        {
            ExtensionIndex other = obj as ExtensionIndex;
            return other != null
                && FirstNodeId == other.FirstNodeId
                && SecondNodeId == other.SecondNodeId
                && CombinationValue == other.CombinationValue;
        }
        public override int GetHashCode()
        {
            // Linear combination of primes 
            return FirstNodeId * 5 + SecondNodeId * 13 + CombinationValue * 23;
        }
    }
}
