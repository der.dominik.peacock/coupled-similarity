﻿
using System.Collections.Generic;

namespace CoupledSimilarity.Game.Player.AI
{
    public class ExtensionMap<K, V, E>
    {
        private readonly Dictionary<K, int> NodeToIdBindings = new Dictionary<K, int>();
        private readonly Dictionary<V, int> ComboValueToIdBindings = new Dictionary<V, int>();
        private readonly Dictionary<ExtensionIndex, E> EnqueuedNodeIndices = new Dictionary<ExtensionIndex, E>();
        private int NextIndex = 0;
        ExtensionIndex ChildNodeIndex = new ExtensionIndex();

        public void MarkCombination(K firstNode, K secondNode, V combinationValue, E markObjected)
        {
            if (IsCombinationMarked(firstNode, secondNode, combinationValue, out _))
            {
                return;
            }
            EnqueuedNodeIndices.Add(
                    new ExtensionIndex(
                        GetOrIncrementNodeId(firstNode),
                        GetOrIncrementNodeId(secondNode),
                        GetOrIncrementComboValueId(combinationValue)
                        ),
                    markObjected
                    );
        }

        public bool IsCombinationMarked(K firstNode, K secondNode, V combinationValue, out E result)
        {
            int firstNodeId = GetOrIncrementNodeId(firstNode);
            int secondNodeId = GetOrIncrementNodeId(secondNode);
            int comboValueId = GetOrIncrementComboValueId(combinationValue);
            ChildNodeIndex.SetIds(firstNodeId, secondNodeId, comboValueId);

            return EnqueuedNodeIndices.TryGetValue(ChildNodeIndex, out result);
        }
        private int GetOrIncrementNodeId(K node)
        {
            int result;
            if (!NodeToIdBindings.TryGetValue(node, out result))
            {
                NodeToIdBindings[node] = NextIndex;
                return NextIndex++;
            }
            return result;
        }
        private int GetOrIncrementComboValueId(V comboValue)
        {
            int result;
            if (!ComboValueToIdBindings.TryGetValue(comboValue, out result))
            {
                ComboValueToIdBindings[comboValue] = NextIndex;
                return NextIndex++;
            }
            return result;
        }
    }
}
