﻿
using UnityEngine;

namespace CoupledSimilarity.Game.Utility
{
    public static class MagicValues
    {
        // Raycast distance for checking what is under mouse / touch 
        public const float ScreenRaycastDistance = 10.0f;
    }
}
