﻿
using UnityEngine;

namespace CoupledSimilarity.Game.Utility
{
    public struct FHitResult2D
    {
        public readonly Collider2D HitCollider;

        public FHitResult2D(Collider2D hitCollider)
        {
            HitCollider = hitCollider;
        }
    }
}
