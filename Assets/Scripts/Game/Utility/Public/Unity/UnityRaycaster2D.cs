﻿
using UnityEngine;

namespace CoupledSimilarity.Game.Utility
{
    public class UnityRaycaster2D : IRaycaster2D
    {
        public FHitResult2D RaycastAtScreenPosition(Vector3 screenPosition)
        {
            RaycastHit2D hitResult = Physics2D.GetRayIntersection(
                       Camera.main.ScreenPointToRay(screenPosition),
                       MagicValues.ScreenRaycastDistance
                       );
            return new FHitResult2D(hitResult.collider);
        }
    }
}
