﻿
using System;
using UniRx;
using UnityEngine;

namespace CoupledSimilarity.Game.Utility
{
    public interface IRaycaster2D
    {
        FHitResult2D RaycastAtScreenPosition(Vector3 screenPosition);
    }
}
