﻿
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CoupledSimilarity.Game.Utility
{
    // The purpose of this class is type safety when handling prefabs in code.
    // It is easy to pass in a GameObject when actually a Prefab is expected.
    [System.Serializable]
    public class Prefab
    {
        [SerializeField]
        private GameObject _PrefabInstance;
        public GameObject PrefabInstance { get => _PrefabInstance; }

        private Prefab(GameObject prefabInstance)
        {
            _PrefabInstance = prefabInstance;
        }

        public static Prefab FromGameObjectInstance(GameObject prefabInstance)
        {
            return new Prefab(prefabInstance);
        }

        public GameObject Instantiate()
        {
            GameObject gameObject = Object.Instantiate(PrefabInstance);
            if(gameObject == null)
            {
                Debug.LogError("Instantiation returned null. Is Prefab not set-up correctly?");
            }
            return gameObject;
        }

#if UNITY_EDITOR
        public GameObject InstantiateInEditor()
        {
            return (GameObject)PrefabUtility.InstantiatePrefab(PrefabInstance);
        }
#endif
    }
}
