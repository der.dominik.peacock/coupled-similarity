﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UniRx;
using UnityEngine;

namespace CoupledSimilarity.Game.Graph
{
    using Edge = IGraphEdge<Transform, EGameEdgeWeight>;
    public class MonoNode : MonoBehaviour, IGraphNode<Transform, EGameEdgeWeight>
    {
        [SerializeField]
        private List<MonoEdge> OutgoingEdges = new List<MonoEdge>();
        [SerializeField]
        private List<MonoEdge> IngoingEdges = new List<MonoEdge>();

        public IReadOnlyList<Edge> Neighbours => OutgoingEdges.Select(e => e as Edge).ToList();
        public IReadOnlyList<IGraphNode<Transform, EGameEdgeWeight>> ParentNodesOfIncomingEdges => throw new NotImplementedException();
        public Transform Value => transform;

#if UNITY_EDITOR
        public void AddGameEdge(MonoEdge edge)
        {
            if (OutgoingEdges.Contains(edge))
            {
                throw new InvalidOperationException(
                    string.Format("The edge {0} was already added to OutgoingEdges.", edge.ToString())
                    );
            }
            OutgoingEdges.Add(edge);
        }
        public void RemoveOutgoingEdge(MonoEdge edge)
        {
            OutgoingEdges.Remove(edge);
        }
        public void AddIngoingEdge(MonoEdge edge)
        {
            if (IngoingEdges.Contains(edge))
            {
                throw new InvalidOperationException(
                    string.Format("The edge {0} was already added to IngoingEdges.", edge.ToString())
                    );
            }
            IngoingEdges.Add(edge);
        }
        public void RemoveIngoingEdge(MonoEdge edge)
        {
            IngoingEdges.Remove(edge);
        }
        public void RedrawEdges()
        {
            IngoingEdges.Where(e => e != null).ToList().ForEach(e => e.Redraw());
            OutgoingEdges.Where(e => e != null).ToList().ForEach(e => e.Redraw());
        }
#endif

    }
}


