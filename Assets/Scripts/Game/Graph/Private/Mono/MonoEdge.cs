﻿
using System;
using UniRx;
using UnityEngine;

namespace CoupledSimilarity.Game.Graph
{
    [ExecuteInEditMode]
    public class MonoEdge : MonoBehaviour, IGraphEdge<Transform, EGameEdgeWeight>
    {
        [SerializeField]
        private MonoNode _TargetNode;
        [SerializeField]
        private EGameEdgeWeight _EdgeWeight;

        [SerializeField]
        private float DistanceFromNodes = 0.1f;
        private SpriteRenderer EdgeSprite => GetComponent<SpriteRenderer>();

        public IGraphNode<Transform, EGameEdgeWeight> TargetNode { get => _TargetNode; }
        public EGameEdgeWeight EdgeWeight { get => _EdgeWeight; set => _EdgeWeight = value; }

#if UNITY_EDITOR

        [SerializeField]
        private MonoNode _ParentNode;

        public MonoNode ParentNodeAsGameNode { get => _ParentNode; set => _ParentNode = value; }
        public MonoNode TargetNodeAsGameNode { get => _TargetNode; }

        private void OnDestroy()
        {
            if(!Application.isPlaying)
            {
                ParentNodeAsGameNode?.RemoveOutgoingEdge(this);
                TargetNodeAsGameNode?.RemoveIngoingEdge(this);
            }
        }

        public void SetUpForTest(MonoNode newParent, MonoNode newTarget)
        {
            _ParentNode = newParent;
            _TargetNode = newTarget;

            newParent.AddGameEdge(this);
            newTarget.AddIngoingEdge(this);
        }
        public void UpdateDirectionAndRedraw(MonoNode newTarget)
        {
            UpdateDirection(newTarget);
            Redraw();
        }
        public void UpdateDirection(MonoNode newTarget)
        {
            _TargetNode = newTarget;
        }
        public void Redraw()
        {
            if(_TargetNode == null) // can be legitimately null when user just created edge
            {
                return;
            }

            Vector3 toTargetNormalised = _TargetNode.transform.position - _ParentNode.transform.position;
            float nodeDistance = toTargetNormalised.magnitude;
            toTargetNormalised.Normalize();

            transform.localPosition = toTargetNormalised * DistanceFromNodes;
            EdgeSprite.size = new Vector2(
                nodeDistance - (2.0f * DistanceFromNodes), 
                EdgeSprite.size.y
                );

            transform.rotation = Quaternion.FromToRotation(Vector3.right, toTargetNormalised);
        }

#endif

    }
}


