﻿
namespace CoupledSimilarity.Game.Graph
{
    [System.Serializable]
    public class FNativeGraphEdge<NodeValue, EdgeValue> : IGraphEdge<NodeValue, EdgeValue>
    {
        public FNativeGraphNode<NodeValue, EdgeValue> NativeTargetNode { get; }
        public FNativeGraphNode<NodeValue, EdgeValue> ParentNode { get; }
        public IGraphNode<NodeValue, EdgeValue> TargetNode => NativeTargetNode;
        public EdgeValue EdgeWeight { get; set; }

        public FNativeGraphEdge(FNativeGraphNode<NodeValue, EdgeValue> targetNode, EdgeValue edgeWeight)
        {
            NativeTargetNode = targetNode;
            EdgeWeight = edgeWeight;
        }

        public override string ToString()
        {
            return string.Format("FNativeGraphEdge[weight:{0}, targetNodeValue:{1}]", EdgeWeight, TargetNode.Value);
        }
    }
}


