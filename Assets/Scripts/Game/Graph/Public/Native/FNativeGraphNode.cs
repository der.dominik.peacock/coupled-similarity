﻿
using System.Collections.Generic;
using System.Collections.ObjectModel;

using CoupledSimilarity.Game.Utility;

namespace CoupledSimilarity.Game.Graph
{
    public class FNativeGraphNode<NodeValue, EdgeValue> : IGraphNode<NodeValue, EdgeValue>
    {
        private readonly List<IGraphEdge<NodeValue, EdgeValue>> AdjacencyList = new List<IGraphEdge<NodeValue, EdgeValue>>();
        private readonly List<IGraphNode<NodeValue, EdgeValue>> IncomingEdgeList = new List<IGraphNode<NodeValue, EdgeValue>>();
        public IReadOnlyList<IGraphEdge<NodeValue, EdgeValue>> Neighbours { get; }
        public IReadOnlyList<IGraphNode<NodeValue, EdgeValue>> ParentNodesOfIncomingEdges { get; }

        public NodeValue Value { get; set; }

        public FNativeGraphNode(NodeValue value)
        {
            Value = value;
            Neighbours = new ReadOnlyCollection<IGraphEdge<NodeValue, EdgeValue>>(AdjacencyList);
            ParentNodesOfIncomingEdges = new ReadOnlyCollection<IGraphNode<NodeValue, EdgeValue>>(IncomingEdgeList);
        }

        public FNativeGraphEdge<NodeValue, EdgeValue> AddNeighbour(FNativeGraphNode<NodeValue, EdgeValue> target, EdgeValue edgeValue)
        {
            FNativeGraphEdge<NodeValue, EdgeValue> edge = new FNativeGraphEdge<NodeValue, EdgeValue>(
                target, 
                edgeValue
                );

            AdjacencyList.Add(edge);
            target.IncomingEdgeList.Add(this);

            return edge;
        }

        public override string ToString()
        {
            return $"FNativeGraphNode[value:{Value}, children: {AdjacencyList.toCompactString()}]";
        }
    }
}


