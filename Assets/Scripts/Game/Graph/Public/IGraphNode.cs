﻿

using System.Collections.Generic;

namespace CoupledSimilarity.Game.Graph
{
    public interface IGraphNode<NodeValue, EdgeValue>
    {
        IReadOnlyList<IGraphEdge<NodeValue, EdgeValue>> Neighbours { get; }
        IReadOnlyList<IGraphNode<NodeValue, EdgeValue>> ParentNodesOfIncomingEdges { get; }
        NodeValue Value { get; }
    }
}


