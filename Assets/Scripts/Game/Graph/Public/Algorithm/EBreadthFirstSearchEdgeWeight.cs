﻿
using System;

namespace CoupledSimilarity.Game.Graph
{
    public enum EBreadthFirstSearchEdgeWeight
    {
        ZeroWeight = 0,
        UnitWeight = 1
    }

    public static class BreadFirstSearchEdgeWeight
    {
        public static int AsInt(this EBreadthFirstSearchEdgeWeight weight)
        {
            switch (weight)
            {
                case EBreadthFirstSearchEdgeWeight.ZeroWeight:
                    return 0;
                case EBreadthFirstSearchEdgeWeight.UnitWeight:
                    return 1;
                default:
                    throw new ArgumentOutOfRangeException(nameof(weight), weight, null);
            }
        }
    }
}
