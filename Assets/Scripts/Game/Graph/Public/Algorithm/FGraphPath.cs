﻿
using System.Collections.Generic;
using CoupledSimilarity.Game.Utility;
using UnityEditor.Experimental.GraphView;

namespace CoupledSimilarity.Game.Graph
{
    public class FGraphPath<V, E>
    {
        public readonly List<IGraphNode<V, E>> NodeOrder = new List<IGraphNode<V, E>>();
        public IGraphEdge<V, E> StartEdge => NodeOrder[0].GetEdgeTo(NodeOrder[1]);
        public IGraphNode<V, E> EndNode => NodeOrder[NodeOrder.Count - 1];
        public int TotalPathWeight { get; }

        private FGraphPath(IGraphNode<V, E> initialNode)
        {
            NodeOrder = new List<IGraphNode<V, E>>();
            NodeOrder.Add(initialNode);
            TotalPathWeight = 0;
        }
        private FGraphPath(FGraphPath<V, E> path, IGraphNode<V, E> nextNode, int weightToAdd)
        {
            NodeOrder = new List<IGraphNode<V, E>>(path.NodeOrder);
            NodeOrder.Add(nextNode);
            TotalPathWeight = path.TotalPathWeight + weightToAdd;
        }

        public static FGraphPath<V, E> FromInitialNode(IGraphNode<V, E> initialNode)
        {
            return new FGraphPath<V, E>(initialNode);
        }
        public FGraphPath<V, E> CreatePathWithExtendedNode(IGraphNode<V, E> nodeToAppend, int weightToAdd)
        {
            return new FGraphPath<V, E>(this, nodeToAppend, weightToAdd);
        }

        public override string ToString()
        {
            string edgeSequenceString = "";
            for (int i = 0; i < NodeOrder.Count - 1; ++i)
            {
                edgeSequenceString += $"{i+1}: From {NodeOrder[i].Value} to {NodeOrder[i+1].Value} over {NodeOrder[i].GetEdgeTo(NodeOrder[i + 1]).EdgeWeight} ";
            }
            return $"[FGraphPath][TotalPathWeight:{TotalPathWeight} NodeOrder:{edgeSequenceString}]";
        }
    }
}
