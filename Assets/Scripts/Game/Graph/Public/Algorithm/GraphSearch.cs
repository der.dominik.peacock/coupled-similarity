﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoupledSimilarity.Game.Graph
{
    public static class GraphSearch
    {
        public static HashSet<IGraphNode<V, E>> FilterConnectedNodes<V, E>(
            IGraphNode<V, E> fromNode, 
            Action<IGraphNode<V, E>> onVisitNode,
            Func<IGraphNode<V, E>, bool> where) 
        {
            HashSet<IGraphNode<V, E>> result = new HashSet<IGraphNode<V, E>>();

            Queue<IGraphNode<V, E>> queuedNodes = new Queue<IGraphNode<V, E>>();
            HashSet<IGraphNode<V, E>> visitedNodes = new HashSet<IGraphNode<V, E>>();

            onVisitNode(fromNode);
            queuedNodes.Enqueue(fromNode);
            visitedNodes.Add(fromNode);

            while (queuedNodes.Count > 0)
            {
                IGraphNode<V, E> node = queuedNodes.Dequeue();
                onVisitNode(node);
                if (where(node))
                {
                    result.Add(node);
                }

                foreach (IGraphNode<V, E> nodeNeighbour in node.Neighbours.Select(e => e.TargetNode))
                {
                    if (!visitedNodes.Contains(nodeNeighbour))
                    {
                        visitedNodes.Add(nodeNeighbour);
                        queuedNodes.Enqueue(nodeNeighbour);
                    }
                }
            }

            return result;
        }


        public delegate EBreadthFirstSearchEdgeWeight GetEdgeWeightFunc<V, E>(IGraphNode<V, E> fromNode, IGraphEdge<V, E> overEdge);
        public delegate bool IsInAreaFunc<V, E>(IGraphNode<V, E> nodeToCheck);
        /**
         * Finds all shortest paths from fromNode to some other nodes that isInArea returns true for.
         * Assumes are edge weights are either 0 or 1; uses breadth first search.
         */
        public static List<FGraphPath<V, E>> FindShortestPathsToArea<V, E>(
            this IGraphNode<V, E> fromNode,
            GetEdgeWeightFunc<V, E> getEdgeWeight,
            IsInAreaFunc<V, E> isInArea)
        {
            int shortestPathWeight = Int32.MaxValue;
            List<FGraphPath<V, E>> foundPaths = new List<FGraphPath<V, E>>();

            Queue<FGraphPath<V, E>> extensionQueue = new Queue<FGraphPath<V, E>>();
            HashSet<IGraphNode<V, E>> extendedSet = new HashSet<IGraphNode<V, E>>();

            extensionQueue.Enqueue(FGraphPath<V, E>.FromInitialNode(fromNode));
            extendedSet.Add(fromNode);

            while (extensionQueue.Count > 0)
            {
                FGraphPath<V, E> pathToExtend = extensionQueue.Dequeue();
                bool bCanExtendedPathsOnlyBeLongerThanShortestPath = pathToExtend.TotalPathWeight >= shortestPathWeight;
                if (bCanExtendedPathsOnlyBeLongerThanShortestPath)
                {
                    continue;
                }

                IGraphNode<V, E> endNode = pathToExtend.EndNode;
                foreach (IGraphEdge<V, E> toNeighbourEdge in endNode.Neighbours)
                {
                    int edgeWeight = getEdgeWeight(endNode, toNeighbourEdge).AsInt();
                    int newPathTotalWeight = pathToExtend.TotalPathWeight + edgeWeight;
                    bool bIsNewPathLongerThanShortestPath = newPathTotalWeight > shortestPathWeight;
                    if (bIsNewPathLongerThanShortestPath)
                    {
                        continue;
                    }

                    IGraphNode<V, E> targetNode = toNeighbourEdge.TargetNode;
                    FGraphPath<V, E> extendedPath =
                        pathToExtend.CreatePathWithExtendedNode(targetNode, edgeWeight);
                    if (isInArea(endNode))
                    {
                        foundPaths.Add(extendedPath);
                        shortestPathWeight = Math.Min(shortestPathWeight, newPathTotalWeight);
                    }
                    else if (!extendedSet.Contains(targetNode))
                    {
                        extendedSet.Add(targetNode);
                        extensionQueue.Enqueue(extendedPath);
                    }
                }
            }

            RemoveAllPathsLongerThan(foundPaths, shortestPathWeight);
            return foundPaths;
        }

        private static void RemoveAllPathsLongerThan<V, E>(List<FGraphPath<V, E>> allPaths, int maxAllowedTotalWeight)
        {
            for (int i = 0; i < allPaths.Count; ++i)
            {
                if (allPaths[i].TotalPathWeight > maxAllowedTotalWeight)
                {
                    allPaths.RemoveAt(i);
                    --i;
                }
            }
        }
    }
}
