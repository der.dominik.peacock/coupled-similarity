﻿
using UnityEngine;

namespace CoupledSimilarity.Game.Graph
{
    public static class UnityExtensions
    {
        public static IGraphNode<N, E> FindNode<N, E>(this GameObject gameObject)
        {
            return gameObject.GetComponent<MonoNode>() as IGraphNode<N, E>;
        }
    }
}


