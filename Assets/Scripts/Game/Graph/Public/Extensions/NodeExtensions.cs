﻿
using System;
using System.Linq;
using UnityEngine;

namespace CoupledSimilarity.Game.Graph
{
    public static class NodeExtensions
    {
        public static IGraphEdge<N, E> GetEdgeTo<N, E>(this IGraphNode<N, E> fromNode, IGraphNode<N, E> toNode)
        {
            return fromNode.Neighbours
                .Where(e => e.TargetNode == toNode)
                .FirstOrDefault();
        }

        public static bool EdgeExistsTo<N, E>(this IGraphNode<N, E> fromNode, IGraphNode<N, E> toNode)
        {
            return fromNode.GetEdgeTo(toNode) != null;
        }

        public static bool HasAnyNeighbours<N, E>(this IGraphNode<N, E> fromNode)
        {
            return fromNode.HasAnyNeighboursWhere(_ => true);
        }
        public static bool HasAnyNeighboursWhere<N, E>(this IGraphNode<N, E> fromNode, Func<IGraphEdge<N, E>, bool> condition)
        {
            // Editor script has a bug that neighbour list sometimes contains null ...
            if (fromNode.Neighbours.Contains(null))
            {
                Debug.LogWarning(string.Format("Graph node {0} contains null neighbour.", fromNode));
            }
            // ... so we must manually count instead of using the list's count property
            return fromNode.Neighbours.Where(e => e != null && condition(e)).Count() > 0;
        }
    }
}


