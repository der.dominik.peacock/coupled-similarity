﻿

namespace CoupledSimilarity.Game.Graph
{
    public enum EGameEdgeWeight
    {
        StraightArrowWeight         = 0,
        InternalMove                = 1,
        EmptyArrowWeight            = 2
    }
}


