﻿

using System.Collections.Generic;

namespace CoupledSimilarity.Game.Graph
{
    public interface IGraphEdge<NodeValue, EdgeValue>
    {
        IGraphNode<NodeValue, EdgeValue> TargetNode { get; }
        EdgeValue EdgeWeight { get; set; }
    }
}


