﻿

namespace CoupledSimilarity.Game.Rules
{
    public interface IInvalidMoveHandler
    {
        void HandleInvalidMove(EMoveInvalidityReason invalidityReason, EMoveType triedMoveType);
    }
}

