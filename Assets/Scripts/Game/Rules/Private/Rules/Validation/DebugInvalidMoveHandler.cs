﻿
using UnityEngine;

namespace CoupledSimilarity.Game.Rules
{
    public class DebugInvalidMoveHandler : IInvalidMoveHandler
    {
        public void HandleInvalidMove(EMoveInvalidityReason invalidityReason, EMoveType triedMoveType)
        {
            Debug.LogError(
                string.Format(
                    "The tried move {0} is invalid because {1}",
                    triedMoveType, 
                    invalidityReason 
                    )
                );
        }
    }
}

