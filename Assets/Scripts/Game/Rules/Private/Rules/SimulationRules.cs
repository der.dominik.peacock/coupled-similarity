﻿
using System;
using UnityEngine;

using CoupledSimilarity.Game.Graph;

namespace CoupledSimilarity.Game.Rules
{
    /**
     * Rules:
     *  - Attacker may move over an edge. Ends turn.
     *  - Defender may move over an edge of same weight. Ends turn.
     */
    public class SimulationRules : AbstractRules
    {
        protected EGameEdgeWeight LastWeightAttackerMovedOver { get; private set; }

        public SimulationRules(
            IInvalidMoveHandler invalidMoveHandler,
            FGameState gameState, 
            IGameMode gameMode
            ) 
            : base(invalidMoveHandler, gameState, gameMode)
        {}

        public override void MoveOverEdge(IGraphEdge<Transform, EGameEdgeWeight> edgeToMoveOver)
        {
            switch (GetPlayerTypeOf(ActivePlayer))
            {
                case EPlayerType.Attacker:
                    if (PerformMoveOverEdge(edgeToMoveOver))
                    {
                        LastWeightAttackerMovedOver = edgeToMoveOver.EdgeWeight;
                    }
                    break;
                case EPlayerType.Defender:
                    if (edgeToMoveOver.EdgeWeight != LastWeightAttackerMovedOver)
                    {
                        HandleInvalidMove(EMoveInvalidityReason.DefenderMoveOverDifferentEdgeWeight, EMoveType.MoveOverEdge);
                        return;
                    }
                    if(!PerformMoveOverEdge(edgeToMoveOver))
                    {
                        return;
                    }
                    break;
            }

            CheckWhetherAnybodyLostAndAdvanceGame();
        }
        protected bool PerformMoveOverEdge(IGraphEdge<Transform, EGameEdgeWeight> edgeToMoveOver)
        {
            if(!ValidateMoveOverEdge(edgeToMoveOver))
            {
                return false;
            }
            ActivePlayer.MovementLogic.MoveTo(edgeToMoveOver.TargetNode);
            BroadcastOnMove(ActivePlayer, EMoveType.MoveOverEdge);
            return true;
        }
        protected bool ValidateMoveOverEdge(IGraphEdge<Transform, EGameEdgeWeight> edgeToMoveOver)
        {
            IGraphNode<Transform, EGameEdgeWeight> actualNode = ActivePlayer.MovementLogic.CurrentNode;
            if (!actualNode.EdgeExistsTo(edgeToMoveOver.TargetNode))
            {
                HandleInvalidMove(EMoveInvalidityReason.MoveToUnneighbouredNode, EMoveType.MoveOverEdge);
                return false;
            }
            return true;
        }

        protected override bool HasPlayerGotMovesLeft(IPlayerEntity entityToCheck)
        {
            switch (GetPlayerTypeOf(entityToCheck))
            {
                case EPlayerType.Attacker:
                    return entityToCheck.MovementLogic.CurrentNode.HasAnyNeighbours();
                case EPlayerType.Defender:
                    return HasDefenderGotMovesLeft(entityToCheck);
                default:
                    throw new InvalidOperationException();
            }
        }
        protected virtual bool HasDefenderGotMovesLeft(IPlayerEntity entityToCheck)
        {
            return
                (entityToCheck == ActivePlayer
                    && entityToCheck.MovementLogic.CurrentNode.HasAnyNeighbours())
                || (entityToCheck == OtherPlayer
                    && (entityToCheck.MovementLogic.CurrentNode.HasAnyNeighboursWhere(neighbourEdge => neighbourEdge.EdgeWeight == LastWeightAttackerMovedOver)));
        }
    }
}
