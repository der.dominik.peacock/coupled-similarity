﻿
using UnityEngine;

using CoupledSimilarity.Game.Graph;

namespace CoupledSimilarity.Game.Rules
{
    public class CoupledSimulationRules : WeakBisimulationRules
    {
        private enum EDefenderMovementState
        {
            CanMakeAnyMoves,
            MustRespondToSymmetrySwap
        }
        private EDefenderMovementState DefenderMovementState = EDefenderMovementState.CanMakeAnyMoves;

        public CoupledSimulationRules( 
            IInvalidMoveHandler invalidMoveHandler,
            FGameState gameState, 
            IGameMode gameMode
            ) 
            : base(invalidMoveHandler, gameState, gameMode)
        {}

        public override void MoveOverEdge(IGraphEdge<Transform, EGameEdgeWeight> edgeToMoveOver)
        {
            switch (DefenderMovementState)
            {
                case EDefenderMovementState.CanMakeAnyMoves:
                    base.MoveOverEdge(edgeToMoveOver);
                    break;
                case EDefenderMovementState.MustRespondToSymmetrySwap:
                    if(edgeToMoveOver.EdgeWeight == EGameEdgeWeight.InternalMove)
                    {
                        PerformMoveOverEdge(edgeToMoveOver);
                    }
                    else
                    {
                        HandleInvalidMove(EMoveInvalidityReason.DefenderMakesMainMoveAfterSymmetrySwap, EMoveType.MoveOverEdge);
                    }
                    break;
            }
        }

        public override void StartSymmetrySwapAsAttacker()
        {
            DefenderMovementState = EDefenderMovementState.MustRespondToSymmetrySwap;
            BroadcastOnMove(ActivePlayer, EMoveType.StartSymmetrySwapAsAttacker);
            OnPlayerFinishMove();
        }

        public override void FinishInternalMovesAsDefender()
        {
            switch (DefenderMovementState)
            {
                case EDefenderMovementState.CanMakeAnyMoves:
                    base.FinishInternalMovesAsDefender();
                    break;
                case EDefenderMovementState.MustRespondToSymmetrySwap:
                    DefenderMovementState = EDefenderMovementState.CanMakeAnyMoves;
                    SwapPlayersForSymmetrySwap();
                    BroadcastOnMove(ActivePlayer, EMoveType.FinishInternalMovesAsDefender);
                    CheckWhetherAnybodyLostAndAdvanceGame();
                    break;
            }
        }
    }
}
