﻿

namespace CoupledSimilarity.Game.Rules
{
    public interface IRuleFactory
    {
        IRules CreateGameRules(EGameType gameType, IGameMode gameMode, FGameState gameState);
        IRules DecorateHumanVerificationRules(IRules gameRules, IPlayerEntity humanPlayer, FGameState gameState);
        IRules DecorateBotVerificationRules(IRules gameRules, IPlayerEntity humanPlayer, FGameState gameState);
    }
}

