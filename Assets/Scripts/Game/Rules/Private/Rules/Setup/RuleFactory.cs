﻿
using System;
using UnityEngine;

namespace CoupledSimilarity.Game.Rules
{
    public class RuleFactory : IRuleFactory
    {
        private readonly IInvalidMoveHandler InvalidMoveHandler;

        public RuleFactory(IInvalidMoveHandler invalidMoveHandler)
        {
            InvalidMoveHandler = invalidMoveHandler;
        }

        public IRules CreateGameRules(EGameType gameType, IGameMode gameMode, FGameState gameState)
        {
            switch (gameType)
            {
                case EGameType.Simulation:
                    return new SimulationRules(
                        InvalidMoveHandler,
                        gameState,
                        gameMode
                    );
                case EGameType.Bisimulation:
                    return new BisimulationRules(
                        InvalidMoveHandler,
                        gameState,
                        gameMode
                        );
                case EGameType.WeakBisimulation:
                    return new WeakBisimulationRules(
                        InvalidMoveHandler,
                        gameState,
                        gameMode
                        );
                case EGameType.CoupledSimulation:
                    return new CoupledSimulationRules(
                        InvalidMoveHandler,
                        gameState,
                        gameMode
                        );
                default:
                    throw new InvalidOperationException(string.Format("EGameType {0} is out of range.", gameType));
            }
        }

        public IRules DecorateHumanVerificationRules(IRules gameRules, IPlayerEntity humanPlayer, FGameState gameState)
        {
            return new HumanTurnEnforcerRulesDecorator(
                gameRules,
                humanPlayer,
                () => gameState.ActivePlayer,
                new AttackerMoveTypeValidator()
                );
        }

        public IRules DecorateBotVerificationRules(IRules gameRules, IPlayerEntity humanPlayer, FGameState gameState)
        {
            return new BotTurnEnforcerRulesDecorator(
                   gameRules,
                   humanPlayer,
                   () => gameState.ActivePlayer,
                   new DefenderMoveTypeValidator()
                   );
        }
    }
}

