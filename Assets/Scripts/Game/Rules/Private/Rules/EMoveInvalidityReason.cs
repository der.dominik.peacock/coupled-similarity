﻿

namespace CoupledSimilarity.Game.Rules
{
    public enum EMoveInvalidityReason
    {
        NotSupportedByGameMode                          = 0,
        // Simulation
        MoveToUnneighbouredNode                         = 100,
        DefenderMoveOverDifferentEdgeWeight             = 101,
        // Weak bisimulation: happens during first phase of making internal moves
        DefenderFinishInternalMovesBeforeMainMove = 200,
        // Weak bisimulation: happens during second phase of making internal moves
        DefenderMakesMainMoveAlthoughMayOnlyMakeInternal = 300,
        // Coupled simulation: happens when defender is supposed to make internal moves
        DefenderMakesMainMoveAfterSymmetrySwap = 400
    }
}

