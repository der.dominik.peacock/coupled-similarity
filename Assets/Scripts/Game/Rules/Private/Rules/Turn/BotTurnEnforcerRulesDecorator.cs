﻿
using System;

namespace CoupledSimilarity.Game.Rules
{
    public class BotTurnEnforcerRulesDecorator : TurnEnforcerRulesDecorator
    {
        public BotTurnEnforcerRulesDecorator(
            IRules realImplementation,
            IPlayerEntity playerUsingThisDecorator,
            Func<IPlayerEntity> getActivePlayer,
            IMoveTypeValidator moveTypeValidator
            ) 
                : base(realImplementation, playerUsingThisDecorator, getActivePlayer, moveTypeValidator)
        {}

        protected override void HandlePlayerMoveOutOfTurn()
        {
            throw new InvalidOperationException("The bot AI tried to move out of turn!");
        }
    }
}

