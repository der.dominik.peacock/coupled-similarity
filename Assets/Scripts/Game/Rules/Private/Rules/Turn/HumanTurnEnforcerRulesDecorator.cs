﻿
using System;
using UnityEngine;

namespace CoupledSimilarity.Game.Rules
{
    public class HumanTurnEnforcerRulesDecorator : TurnEnforcerRulesDecorator
    {
        public HumanTurnEnforcerRulesDecorator(
            IRules realImplementation,
            IPlayerEntity playerUsingThisDecorator,
            Func<IPlayerEntity> getActivePlayer,
            IMoveTypeValidator moveTypeValidator
            ) 
                : base(realImplementation, playerUsingThisDecorator, getActivePlayer, moveTypeValidator)
        {}

        protected override void HandlePlayerMoveOutOfTurn()
        {
            // TODO: Replace this log with UI
            Debug.LogError("Wait for your opponent to finish!");
        }
    }
}

