﻿
namespace CoupledSimilarity.Game.Rules
{
    public class DefenderMoveTypeValidator : IMoveTypeValidator
    {
        public bool IsMoveTypeAllowedForPlayer(EMoveType moveType)
        {
            switch (moveType)
            {
                case EMoveType.MoveOverEdge:
                    return true;
                case EMoveType.StartSymmetrySwapAsAttacker:
                    return false;
                case EMoveType.FinishInternalMovesAsDefender:
                    return true;
                default:
                    return false;
            }
        }
    }
}

