﻿
namespace CoupledSimilarity.Game.Rules
{
    public class AttackerMoveTypeValidator : IMoveTypeValidator
    {
        public bool IsMoveTypeAllowedForPlayer(EMoveType moveType)
        {
            switch (moveType)
            {
                case EMoveType.MoveOverEdge:
                    return true;
                case EMoveType.StartSymmetrySwapAsAttacker:
                    return true;
                case EMoveType.FinishInternalMovesAsDefender:
                    return false;
                default:
                    return false;
            }
        }
    }
}

