﻿
using System;
using UnityEngine;

using CoupledSimilarity.Game.Graph;

namespace CoupledSimilarity.Game.Rules
{
    public interface IMoveTypeValidator
    {
        bool IsMoveTypeAllowedForPlayer(EMoveType moveType);
    }
}

