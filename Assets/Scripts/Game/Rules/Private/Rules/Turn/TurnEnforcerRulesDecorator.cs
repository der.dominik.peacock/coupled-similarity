﻿
using System;
using UnityEngine;

using CoupledSimilarity.Game.Graph;

namespace CoupledSimilarity.Game.Rules
{
    public abstract class TurnEnforcerRulesDecorator : IRules
    {
        private readonly IRules RealImplementation;
        private readonly IPlayerEntity PlayerUsingThisDecorator;
        private readonly Func<IPlayerEntity> GetActivePlayer;
        private readonly IMoveTypeValidator MoveTypeValidator;

        public TurnEnforcerRulesDecorator(
            IRules realImplementation, 
            IPlayerEntity playerUsingThisDecorator, 
            Func<IPlayerEntity> getActivePlayer,
            IMoveTypeValidator moveTypeValidator
            )
        {
            RealImplementation = realImplementation;
            PlayerUsingThisDecorator = playerUsingThisDecorator;
            GetActivePlayer = getActivePlayer;
            MoveTypeValidator = moveTypeValidator;
        }

        public void MoveOverEdge(IGraphEdge<Transform, EGameEdgeWeight> edgeToMoveOver)
        {
            if (ValidatePlayerTurn(EMoveType.MoveOverEdge))
            {
                RealImplementation.MoveOverEdge(edgeToMoveOver);
            }
        }
        public void StartSymmetrySwapAsAttacker()
        {
            if (ValidatePlayerTurn(EMoveType.StartSymmetrySwapAsAttacker))
            {
                RealImplementation.StartSymmetrySwapAsAttacker();
            }
        }
        public void FinishInternalMovesAsDefender()
        {
            if (ValidatePlayerTurn(EMoveType.FinishInternalMovesAsDefender))
            {
                RealImplementation.FinishInternalMovesAsDefender();
            }
        }

        private bool ValidatePlayerTurn(EMoveType moveType)
        {
            if(GetActivePlayer() == PlayerUsingThisDecorator)
            {
                return MoveTypeValidator.IsMoveTypeAllowedForPlayer(moveType);
            }
            else
            {
                HandlePlayerMoveOutOfTurn();
                return false;
            }
        }
        protected abstract void HandlePlayerMoveOutOfTurn();
    }
}

