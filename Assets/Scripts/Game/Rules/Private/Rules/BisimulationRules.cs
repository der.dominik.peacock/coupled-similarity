﻿
using UnityEngine;

using CoupledSimilarity.Game.Graph;

namespace CoupledSimilarity.Game.Rules
{
    /**
     * Rules:
     *  - Attacker may move over an edge. Ends turn.
     *  - Defender may move over an edge of same weight. Ends turn.
     *  - Attacker may swap positions with defender.
     */
    public class BisimulationRules : SimulationRules
    {
        public BisimulationRules(
            IInvalidMoveHandler invalidMoveHandler,
            FGameState gameState, 
            IGameMode gameMode
            ) 
            : base(invalidMoveHandler, gameState, gameMode)
        {}

        public override void StartSymmetrySwapAsAttacker()
        {
            SwapPlayersForSymmetrySwap();
            BroadcastOnMove(ActivePlayer, EMoveType.StartSymmetrySwapAsAttacker);
        }
        protected void SwapPlayersForSymmetrySwap()
        {
            IGraphNode<Transform, EGameEdgeWeight> oldAttackerNode = ActivePlayer.MovementLogic.CurrentNode;
            ActivePlayer.MovementLogic.TeleportTo(OtherPlayer.MovementLogic.CurrentNode);
            OtherPlayer.MovementLogic.TeleportTo(oldAttackerNode);
        }

        protected override bool HasPlayerGotMovesLeft(IPlayerEntity entityToCheck)
        {
            if(GetPlayerTypeOf(entityToCheck) == EPlayerType.Defender)
            {
                return HasPlayerGotAnyNeighbours(entityToCheck);
            }
            return HasPlayerGotAnyNeighbours(entityToCheck)
                   || WouldAttackerBeAbleToMoveAfterSymmetrySwap();
        }
        private bool HasPlayerGotAnyNeighbours(IPlayerEntity player)
        {
            return base.HasPlayerGotMovesLeft(player);
        }
        private bool WouldAttackerBeAbleToMoveAfterSymmetrySwap()
        {
            return Defender.MovementLogic.CurrentNode.Neighbours.Count > 0;
        }
    }
}
