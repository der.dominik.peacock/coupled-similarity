﻿
using UnityEngine;

using CoupledSimilarity.Game.Graph;

namespace CoupledSimilarity.Game.Rules
{
    public interface IRules
    {
        void MoveOverEdge(IGraphEdge<Transform, EGameEdgeWeight> edgeToMoveOver);
        void StartSymmetrySwapAsAttacker();
        void FinishInternalMovesAsDefender();
    }
}

