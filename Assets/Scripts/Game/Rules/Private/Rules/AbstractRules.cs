﻿
using System;
using UnityEngine;

using CoupledSimilarity.Game.Graph;

namespace CoupledSimilarity.Game.Rules
{
    public abstract class AbstractRules : IRules
    {
        private readonly IInvalidMoveHandler InvalidMoveHandler;
        private readonly FGameState GameState;
        private readonly IGameMode GameMode;

        protected IPlayerEntity ActivePlayer => GameState.ActivePlayer;
        protected IPlayerEntity OtherPlayer => ActivePlayer == GameState.Attacker ? GameState.Defender : GameState.Attacker;
        protected IPlayerEntity Attacker => GameState.Attacker;
        protected IPlayerEntity Defender => GameState.Defender;

        protected AbstractRules(
            IInvalidMoveHandler invalidMoveHandler,
            FGameState gameState, 
            IGameMode gameMode)
        {
            InvalidMoveHandler = invalidMoveHandler;
            GameState = gameState;
            GameMode = gameMode;
        }

        protected void BroadcastOnMove(IPlayerEntity playerThatMoved, EMoveType performedMove)
        {
            GameState.BroadcastOnGameMoveEvent(
                new FGameMoveEvent(playerThatMoved, performedMove)
                );
        }

        protected void CheckWhetherAnybodyLostAndAdvanceGame()
        {
            if (!HasPlayerGotMovesLeft(OtherPlayer))
            {
                OnPlayerWins(ActivePlayer);
            }
            else if (!HasPlayerGotMovesLeft(ActivePlayer))
            {
                OnPlayerWins(OtherPlayer);
            }
            else
            {
                OnPlayerFinishMove();
            }
        }
        protected virtual bool HasPlayerGotMovesLeft(IPlayerEntity entityToCheck)
        {
            return false;
        }
        protected void OnPlayerWins(IPlayerEntity playerThatWon)
        {
            GameMode.OnPlayerWins(
                GetPlayerTypeOf(playerThatWon)
                );
        }
        protected void OnPlayerFinishMove()
        {
            GameMode.OnPlayerFinishMove();
        }
        protected EPlayerType GetPlayerTypeOf(IPlayerEntity player)
        {
            return player == GameState.Attacker ? EPlayerType.Attacker : EPlayerType.Defender;
        }


        public virtual void MoveOverEdge(IGraphEdge<Transform, EGameEdgeWeight> edgeToMoveOver)
        {
            HandleInvalidMove(EMoveInvalidityReason.NotSupportedByGameMode, EMoveType.MoveOverEdge);
        }
        public virtual void StartSymmetrySwapAsAttacker()
        {
            HandleInvalidMove(EMoveInvalidityReason.NotSupportedByGameMode, EMoveType.StartSymmetrySwapAsAttacker);
        }
        public virtual void FinishInternalMovesAsDefender()
        {
            HandleInvalidMove(EMoveInvalidityReason.NotSupportedByGameMode, EMoveType.FinishInternalMovesAsDefender);
        }
        protected void HandleInvalidMove(EMoveInvalidityReason reason, EMoveType moveType)
        {
            InvalidMoveHandler.HandleInvalidMove(reason, moveType);
        }

    }
}

