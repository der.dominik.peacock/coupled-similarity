﻿
using UnityEngine;

using CoupledSimilarity.Game.Graph;

namespace CoupledSimilarity.Game.Rules
{
    /**
     * Rules:
     *  - Attacker may move over an edge. Ends turn.
     *  - Defender may move over an edge of same weight. Ends turn.
     *  - Attacker may swap positions with defender.
     *  - 
     */
    public class WeakBisimulationRules : BisimulationRules
    {
        private enum EMovementState
        {
            MakingFirstInternalMoves,
            MakingSecondInternalMoves
        }
        private EMovementState DefenderMovementState = EMovementState.MakingFirstInternalMoves;

        public WeakBisimulationRules( 
            IInvalidMoveHandler invalidMoveHandler,
            FGameState gameState, 
            IGameMode gameMode
            ) 
            : base(invalidMoveHandler, gameState, gameMode)
        {}

        public override void FinishInternalMovesAsDefender()
        {
            if(DefenderMovementState == EMovementState.MakingFirstInternalMoves)
            {
                HandleInvalidMove(EMoveInvalidityReason.DefenderFinishInternalMovesBeforeMainMove, EMoveType.FinishInternalMovesAsDefender);
                return;
            }

            DefenderMovementState = EMovementState.MakingFirstInternalMoves;
            BroadcastOnMove(ActivePlayer, EMoveType.FinishInternalMovesAsDefender);
            CheckWhetherAnybodyLostAndAdvanceGame();
        }

        public override void MoveOverEdge(IGraphEdge<Transform, EGameEdgeWeight> edgeToMoveOver)
        {
            if(ValidateMoveOverEdge(edgeToMoveOver))
            {
                switch (GetPlayerTypeOf(ActivePlayer))
                {
                    case EPlayerType.Attacker:
                        base.MoveOverEdge(edgeToMoveOver);
                        break;
                    case EPlayerType.Defender:
                        HandleDefenderMove(edgeToMoveOver);
                        break;
                }
            }
        }
        private void HandleDefenderMove(IGraphEdge<Transform, EGameEdgeWeight> edgeToMoveOver)
        {
            switch (DefenderMovementState)
            {
                case EMovementState.MakingFirstInternalMoves:
                    if(edgeToMoveOver.EdgeWeight == LastWeightAttackerMovedOver)
                    {
                        DefenderMovementState = EMovementState.MakingSecondInternalMoves;
                    }
                    else if (edgeToMoveOver.EdgeWeight != EGameEdgeWeight.InternalMove)
                    {
                        HandleInvalidMove(EMoveInvalidityReason.DefenderMoveOverDifferentEdgeWeight, EMoveType.MoveOverEdge);
                        return;
                    }
                    PerformMoveOverEdge(edgeToMoveOver);
                    break;
                case EMovementState.MakingSecondInternalMoves:
                    if(edgeToMoveOver.EdgeWeight == EGameEdgeWeight.InternalMove)
                    {
                        PerformMoveOverEdge(edgeToMoveOver);
                    }
                    else
                    {
                        HandleInvalidMove(EMoveInvalidityReason.DefenderMakesMainMoveAlthoughMayOnlyMakeInternal, EMoveType.MoveOverEdge);
                    }
                    break;
            }
        }

        protected override bool HasDefenderGotMovesLeft(IPlayerEntity entityToCheck)
        {
            return
                (entityToCheck == ActivePlayer
                    && entityToCheck.MovementLogic.CurrentNode.HasAnyNeighbours())
                || (entityToCheck == OtherPlayer
                    && (entityToCheck.MovementLogic.CurrentNode.HasAnyNeighboursWhere(neighbourEdge => neighbourEdge.EdgeWeight == LastWeightAttackerMovedOver)
                        || entityToCheck.MovementLogic.CurrentNode.HasAnyNeighboursWhere(neighbourEdge => neighbourEdge.EdgeWeight == EGameEdgeWeight.InternalMove)));
        }
    }
}
