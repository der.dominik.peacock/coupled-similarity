﻿
using System;
using UniRx;
using UnityEngine;

using CoupledSimilarity.Game.Graph;

namespace CoupledSimilarity.Game.Rules
{
    public class FGameState : IGameState
    {
        private event Action OnStartNewGameEvent;
        private event Action<FGameMoveEvent> OnGameMoveEvent;
        private event Action<FGameEndStatistics> OnEndGameEvent;
        public IObservable<Unit> OnPrepareNewGame { get; }
        public IObservable<FGameMoveEvent> OnGameMove { get; }
        public IObservable<FGameEndStatistics> OnEndGame { get; } 

        public EMoveType LastMoveType { get; set; }
        public EGameEdgeWeight WeightOfLastEdge { get; set; }

        public IPlayerEntity Attacker { get; }
        public IPlayerEntity Defender { get; }
        public IPlayerEntity ActivePlayer { get; set; }

        public EGameType GameTypeBeingPlayed { get; set; }
        public IGraphNode<Transform, EGameEdgeWeight> AttackerStartNode { get; }
        public IGraphNode<Transform, EGameEdgeWeight> DefenderStartNode { get; }

        public FGameState(IPlayerEntity attacker, IPlayerEntity defender, IGraphNode<Transform, EGameEdgeWeight> attackerStart, IGraphNode<Transform, EGameEdgeWeight> defenderStart)
        {
            OnPrepareNewGame = Observable.FromEvent(
                subscriber => OnStartNewGameEvent += subscriber,
                subscriber => OnStartNewGameEvent -= subscriber
                );
            OnGameMove = Observable.FromEvent<FGameMoveEvent>(
                subscriber => OnGameMoveEvent += subscriber,
                subscriber => OnGameMoveEvent -= subscriber
                );
            OnEndGame = Observable.FromEvent<FGameEndStatistics>(
                subscriber => OnEndGameEvent += subscriber,
                subscriber => OnEndGameEvent -= subscriber
                );

            Attacker = attacker;
            Defender = defender;
            AttackerStartNode = attackerStart;
            DefenderStartNode = defenderStart;
        }

        public void BroadcastOnPrepareNewGameEvent()
        {
            OnStartNewGameEvent?.Invoke();
        }
        public void BroadcastOnGameMoveEvent(FGameMoveEvent eventData)
        {
            OnGameMoveEvent?.Invoke(eventData);
        }
        public void BroadcastOnEndGameEvent(FGameEndStatistics eventData)
        {
            OnEndGameEvent?.Invoke(eventData);
        }

    }
}

