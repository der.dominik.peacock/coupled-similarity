﻿
namespace CoupledSimilarity.Game.Rules
{
    public class MappedGameInitialisation : IGameInitialisation
    {
        private readonly ISpecialisedGameInitialiser SimulationInitialiser;
        private readonly ISpecialisedGameInitialiser BisimulationInitialiser;
        private readonly ISpecialisedGameInitialiser WeakBisimulationInitialiser;
        private readonly ISpecialisedGameInitialiser CoupledSimilarityInitialiser;

        public MappedGameInitialisation(
            ISpecialisedGameInitialiser simulationInitialiser,
            ISpecialisedGameInitialiser bisimulationInitialiser,
            ISpecialisedGameInitialiser weakBisimulationInitialiser,
            ISpecialisedGameInitialiser coupledSimilarityInitialiser)
        {
            SimulationInitialiser = simulationInitialiser;
            BisimulationInitialiser = bisimulationInitialiser;
            WeakBisimulationInitialiser = weakBisimulationInitialiser;
            CoupledSimilarityInitialiser = coupledSimilarityInitialiser;
        }

        public void SetupNewGame(EGameType type, IGameState gameState, IRules attackerMoveRules, IRules defenderMoveRules)
        {
            SimulationInitialiser.DisableSetupSystems(gameState);
            BisimulationInitialiser.DisableSetupSystems(gameState);
            WeakBisimulationInitialiser.DisableSetupSystems(gameState);
            CoupledSimilarityInitialiser.DisableSetupSystems(gameState);

            switch (type)
            {
                case EGameType.Simulation:
                    SimulationInitialiser.SetupNewGame(gameState, attackerMoveRules, defenderMoveRules);
                    break;
                case EGameType.Bisimulation:
                    BisimulationInitialiser.SetupNewGame(gameState, attackerMoveRules, defenderMoveRules);
                    break;
                case EGameType.WeakBisimulation:
                    WeakBisimulationInitialiser.SetupNewGame(gameState, attackerMoveRules, defenderMoveRules);
                    break;
                case EGameType.CoupledSimulation:
                    CoupledSimilarityInitialiser.SetupNewGame(gameState, attackerMoveRules, defenderMoveRules);
                    break;
            }
        }
    }
}

