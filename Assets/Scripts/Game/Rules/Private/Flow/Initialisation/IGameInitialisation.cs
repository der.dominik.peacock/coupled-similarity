﻿
namespace CoupledSimilarity.Game.Rules
{
    public interface IGameInitialisation
    {
        void SetupNewGame(EGameType type, IGameState gameState, IRules attackerMoveRules, IRules defenderMoveRules);
    }
}

