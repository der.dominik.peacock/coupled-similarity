﻿
using System;
using System.Collections.Generic;
using UniRx;

namespace CoupledSimilarity.Game.Rules
{
    public class ConjunctiveSpecialisedGameInitialiser : ISpecialisedGameInitialiser
    {
        public List<ISpecialisedGameInitialiser> Children = new List<ISpecialisedGameInitialiser>();

        public static ConjunctiveSpecialisedGameInitialiser CreateWithChild(ISpecialisedGameInitialiser child)
        {
            ConjunctiveSpecialisedGameInitialiser result = new ConjunctiveSpecialisedGameInitialiser();
            result.Add(child);
            return result;
        }

        public IDisposable Add(ISpecialisedGameInitialiser newChild)
        {
            Children.Add(newChild);
            return Disposable.Create(
                () => Children.Remove(newChild)
                );
        }

        public void SetupNewGame(IGameState gameState, IRules attackerMoveRules, IRules defenderMoveRules)
        {
            Children.ForEach(
                e => e.SetupNewGame(gameState, attackerMoveRules, defenderMoveRules)
                );
        }

        public void DisableSetupSystems(IGameState gameState)
        {
            Children.ForEach(
                e => e.DisableSetupSystems(gameState)
                );
        }
    }
}

