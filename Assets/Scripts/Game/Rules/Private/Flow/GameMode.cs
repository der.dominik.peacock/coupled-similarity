﻿
using UnityEngine;

namespace CoupledSimilarity.Game.Rules
{
    public class GameMode : IGameMode
    {
        private readonly FGameState GameState;
        private readonly IRuleFactory RuleFactory;
        private readonly IGameInitialisation GameInitialisation;

        private IRules GameRules;

        public IPlayerEntity ActivePlayer { get => GameState.ActivePlayer; set => GameState.ActivePlayer = value; }

        public GameMode(FGameState gameState, IRuleFactory ruleFactory, IGameInitialisation gameInitialisation)
        {
            GameState = gameState;
            RuleFactory = ruleFactory;
            GameInitialisation = gameInitialisation;
        }

        public void PrepareNewMatch()
        {
            GameState.BroadcastOnPrepareNewGameEvent();

            GameState.Attacker.MovementLogic.TeleportTo(GameState.AttackerStartNode);
            GameState.Defender.MovementLogic.TeleportTo(GameState.DefenderStartNode);
        }

        public void SelectSimulationGameType(EGameType gameTypeToPlay)
        {
            GameState.GameTypeBeingPlayed = gameTypeToPlay;
            GameRules = RuleFactory.CreateGameRules(gameTypeToPlay, this, GameState);

            IRules attackerRules = RuleFactory.DecorateHumanVerificationRules(GameRules, GameState.Attacker, GameState);
            IRules defenderRules = RuleFactory.DecorateBotVerificationRules(GameRules, GameState.Defender, GameState);
            GameInitialisation.SetupNewGame(gameTypeToPlay, GameState, attackerRules, defenderRules);

            StartGame(attackerRules, defenderRules);
        }
        private void StartGame(IRules attackerRules, IRules defenderRules)
        {
            GameState.Attacker.PlayerLogic.OnStartNewGame(
                GameState,
                attackerRules
                );
            GameState.Defender.PlayerLogic.OnStartNewGame(
                GameState,
                defenderRules
                );

            ActivePlayer = GameState.Attacker;
            ActivePlayer.PlayerLogic.StartTurn(); // TODO: Investigate why player cannot move
        }

        public void OnPlayerFinishMove()
        {
            ActivePlayer.PlayerLogic.EndTurn();
            SelectNextPlayer();
            ActivePlayer.PlayerLogic.StartTurn();
        }
        private void SelectNextPlayer()
        {
            // TODO: Visualise whose turn it is
            if(GameState.ActivePlayer == GameState.Attacker)
            {
                GameState.ActivePlayer = GameState.Defender;
            }
            else
            {
                GameState.ActivePlayer = GameState.Attacker;
            }
        }

        public void OnPlayerWins(EPlayerType playerThatWon)
        {
            GameState.BroadcastOnEndGameEvent(
                new FGameEndStatistics(playerThatWon)
                );
            Debug.Log($"Player {playerThatWon} won");
        }
    }
}

