﻿
using System;
using UniRx;

using CoupledSimilarity.Game.Graph;

namespace CoupledSimilarity.Game.Rules
{
    [System.Serializable]
    public enum EGameType
    {
        Simulation          = 0,
        Bisimulation        = 1,
        WeakBisimulation    = 2,
        CoupledSimulation   = 3
    }
}

