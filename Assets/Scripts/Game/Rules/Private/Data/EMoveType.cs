﻿
using System;
using UniRx;

using CoupledSimilarity.Game.Graph;

namespace CoupledSimilarity.Game.Rules
{
    public enum EMoveType
    {
        MoveOverEdge                    = 0,
        StartSymmetrySwapAsAttacker     = 1,
        FinishInternalMovesAsDefender   = 2
    }
}

