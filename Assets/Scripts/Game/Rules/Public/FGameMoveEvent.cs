﻿

namespace CoupledSimilarity.Game.Rules
{
    public class FGameMoveEvent
    {
        public readonly IPlayerEntity PlayerThatMoved;
        public readonly EMoveType PerformedMove;

        public FGameMoveEvent(IPlayerEntity playerThatMoved, EMoveType performedMove)
        {
            PlayerThatMoved = playerThatMoved;
            PerformedMove = performedMove;
        }
    }
}
