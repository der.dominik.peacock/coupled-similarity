﻿
using System;
using UniRx;

using CoupledSimilarity.Game.Graph;

namespace CoupledSimilarity.Game.Rules
{
    public interface IGameMode
    {
        void PrepareNewMatch();
        void SelectSimulationGameType(EGameType gameTypeToPlay);
        void OnPlayerFinishMove();
        void OnPlayerWins(EPlayerType playerThatWon);
    }
}

