﻿
namespace CoupledSimilarity.Game.Rules
{
    public interface ISpecialisedGameInitialiser
    {
        void SetupNewGame(IGameState gameState, IRules attackerMoveRules, IRules defenderMoveRules);
        void DisableSetupSystems(IGameState gameState);
    }
}

