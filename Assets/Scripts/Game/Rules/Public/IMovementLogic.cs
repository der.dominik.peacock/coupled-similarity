﻿
using System;
using UnityEngine;

using CoupledSimilarity.Game.Graph;

namespace CoupledSimilarity.Game.Rules
{
    using Node = IGraphNode<Transform, EGameEdgeWeight>;
    public interface IMovementLogic
    {
        IObservable<Node> OnReachTargetNode { get; }
        Node CurrentNode { get; }
        Transform EntityTransform { get; }

        void MoveTo(Node nodeToMoveTo);
        void TeleportTo(Node nodeToTeleportTo);
    }
}

