﻿
using System;
using UniRx;
using UnityEngine;

using CoupledSimilarity.Game.Graph;

namespace CoupledSimilarity.Game.Rules
{
    public interface IGameState
    {
        IObservable<Unit> OnPrepareNewGame { get; }
        IObservable<FGameMoveEvent> OnGameMove { get; }
        IObservable<FGameEndStatistics> OnEndGame { get; }

        EMoveType LastMoveType { get; }
        EGameEdgeWeight WeightOfLastEdge { get; }

        IPlayerEntity Attacker { get; }
        IPlayerEntity Defender { get; }
        IPlayerEntity ActivePlayer { get; }

        EGameType GameTypeBeingPlayed { get; }
        IGraphNode<Transform, EGameEdgeWeight> AttackerStartNode { get; }
        IGraphNode<Transform, EGameEdgeWeight> DefenderStartNode { get; }
    }
}

