﻿
using UnityEngine;

using CoupledSimilarity.Game.Graph;

namespace CoupledSimilarity.Game.Rules
{
    public interface IPlayerLogic
    {
        void OnStartNewGame(IGameState gameState, IRules playerMoveRules);
        void StartTurn();
        void EndTurn();
    }
}

