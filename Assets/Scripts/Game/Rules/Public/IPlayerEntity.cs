﻿
namespace CoupledSimilarity.Game.Rules
{
    public interface IPlayerEntity
    {
        IPlayerLogic PlayerLogic { get; }
        IMovementLogic MovementLogic { get; }
    }
}

