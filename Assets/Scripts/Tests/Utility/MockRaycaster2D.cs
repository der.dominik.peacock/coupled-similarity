﻿
using System;
using UnityEngine;

using CoupledSimilarity.Game.Utility;

namespace Tests
{
    public class MockRaycaster2D : IRaycaster2D
    {
        Func<Vector3, Collider2D> CreateCollider;

        public MockRaycaster2D(Func<Vector3, Collider2D> createCollider)
        {
            CreateCollider = createCollider;
        }

        public FHitResult2D RaycastAtScreenPosition(Vector3 screenPosition)
        {
            return new FHitResult2D(
                CreateCollider(screenPosition)
                );
        }
    }
}
