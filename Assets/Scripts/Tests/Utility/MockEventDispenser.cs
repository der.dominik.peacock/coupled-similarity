﻿
using System;
using UniRx;
using UnityEngine;

using CoupledSimilarity.Game.Utility;

namespace Tests
{
    public class MockEventDispenser : IEventDispenser
    {
        public ISubject<bool> OnApplicationPauseSubject { get; } = new Subject<bool>();
        public IObservable<bool> OnApplicationPause => OnApplicationPauseSubject;

        public ISubject<Unit> OnUpdateSubject { get; } = new Subject<Unit>();
        public IObservable<Unit> OnUpdate => OnUpdateSubject;

        public ISubject<Unit> OnLateUpdateSubject { get; } = new Subject<Unit>();
        public IObservable<Unit> OnLateUpdate => OnLateUpdateSubject;

        public ISubject<Vector3> OnPressScreenSubject { get; } = new Subject<Vector3>();
        public IObservable<Vector3> OnPressScreen => OnPressScreenSubject;

        public ISubject<Vector3> OnReleaseScreenSubject { get; } = new Subject<Vector3>();
        public IObservable<Vector3> OnReleaseScreen => OnReleaseScreenSubject;
    }
}
