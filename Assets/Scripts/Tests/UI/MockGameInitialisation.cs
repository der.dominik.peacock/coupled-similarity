﻿
using System;
using NUnit.Framework;

using CoupledSimilarity.Game.Rules;

namespace Tests
{
    public class MockGameInitialisation : IGameInitialisation
    {
        public void SetupNewGame(EGameType type, IGameState gameState, IRules attackerMoveRules, IRules defenderMoveRules)
        {
            // Don't do anything because this method is intended for setting up UI, which tests do not have.
        }
    }
}
