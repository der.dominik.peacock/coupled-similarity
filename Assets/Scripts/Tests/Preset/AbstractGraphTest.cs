
using System;

using CoupledSimilarity.Game.Rules;

namespace Tests
{
    public abstract class AbstractGraphTest<AttackerLogic, DefenderLogic, GraphType> where GraphType : ITestGraph, new()
        where AttackerLogic : IPlayerLogic 
        where DefenderLogic : IPlayerLogic
    {
        protected GraphType Graph { get; private set; }
        protected readonly MockEventDispenser Events = new MockEventDispenser();

        protected FPlayerEntityMock<AttackerLogic, MovementLogicMock> Attacker;
        protected FPlayerEntityMock<DefenderLogic, MovementLogicMock> Defender;
        protected FGameState GameState;
        protected IGameMode GameMode;

        protected void SetUp(Func<AttackerLogic> createAttacker, Func<DefenderLogic> createDefender)
        {
            Graph = CreateGraph();

            Attacker = CreatePlayer(createAttacker);
            Defender = CreatePlayer(createDefender);

            GameState = CreateGameState(Attacker, Defender);
            GameMode = CreateGameMode();
        }
        private FPlayerEntityMock<PlayerLogic, MovementLogicMock> CreatePlayer<PlayerLogic>(Func<PlayerLogic> createPlayerLogic)
            where PlayerLogic : IPlayerLogic
        {
            MovementLogicMock movementLogic =
                new MovementLogicMock();
            return new FPlayerEntityMock<PlayerLogic, MovementLogicMock>(
                createPlayerLogic(),
                movementLogic
                );
        }
        protected FGameState CreateGameState(
            FPlayerEntityMock<AttackerLogic, MovementLogicMock> attacker,
            FPlayerEntityMock<DefenderLogic, MovementLogicMock> defender)
        {
            return new FGameState(
                attacker,
                defender,
                Graph.AttackerStartNode,
                Graph.DefenderStartNode
                );
        }

        protected IGameMode CreateGameMode()
        {
            return new GameMode(GameState, CreateRuleFactory(), CreateGameInitialisation());
        }
        protected virtual IRuleFactory CreateRuleFactory()
        {
            return new RuleFactory(
                new DebugInvalidMoveHandler()
                );
        }
        protected virtual GraphType CreateGraph()
        {
            return new GraphType();
        }
        protected IGameInitialisation CreateGameInitialisation()
        {
            return new MockGameInitialisation();
        }

    }
}

