
using System;

using CoupledSimilarity.Game.Rules;

namespace Tests
{
    public abstract class SamePlayerGraphTest<PlayerLogic, GraphType> : AbstractGraphTest<PlayerLogic, PlayerLogic, GraphType> where GraphType : ITestGraph, new()
        where PlayerLogic : IPlayerLogic
    {
        protected void SetUp(Func<PlayerLogic> createPlayer)
        {
            SetUp(createPlayer, createPlayer);
        }
    }
}

