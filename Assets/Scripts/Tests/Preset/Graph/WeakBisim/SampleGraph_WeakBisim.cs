
using UnityEngine;

using CoupledSimilarity.Game.Graph;

namespace Tests
{
    public class SampleGraph_WeakBisim : ITestGraph
    {
        public IGraphNode<Transform, EGameEdgeWeight> AttackerStartNode => L_Root.AsNode();
        public IGraphNode<Transform, EGameEdgeWeight> DefenderStartNode => R_Root.AsNode();

        // Left nodes
        public GameObject L_Root                            = new GameObject("L_Root", typeof(CircleCollider2D), typeof(MonoNode));
        public GameObject L_NodeA                           = new GameObject("L_NodeA", typeof(CircleCollider2D), typeof(MonoNode));
        public GameObject L_NodeB                           = new GameObject("L_NodeB", typeof(CircleCollider2D), typeof(MonoNode));
        public GameObject L_NodeC                           = new GameObject("L_NodeC", typeof(CircleCollider2D), typeof(MonoNode));
        public GameObject L_NodeInternalToSink              = new GameObject("L_NodeInternalToSink", typeof(CircleCollider2D), typeof(MonoNode));
        public GameObject L_NodeSink                        = new GameObject("L_NodeSink", typeof(CircleCollider2D), typeof(MonoNode));
        // Left edges
        public GameObject L_E_Root_NodeA                                = new GameObject("L_E_Root_NodeA", typeof(MonoEdge));
        public GameObject L_E_Root_NodeC_Internal                       = new GameObject("L_E_Root_NodeC_Internal", typeof(MonoEdge));
        public GameObject L_E_NodeC_NodeA                               = new GameObject("L_E_NodeC_RootA", typeof(MonoEdge));
        public GameObject L_E_NodeA_NodeB_Internal                      = new GameObject("L_E_NodeA_NodeB_Internal", typeof(MonoEdge));
        public GameObject L_E_NodeB_Root                                = new GameObject("L_E_NodeB_Root", typeof(MonoEdge));
        public GameObject L_E_NodeB_NodeInternalToSink_Internal         = new GameObject("L_E_NodeB_NodeInternalToSink_Internal", typeof(MonoEdge));
        public GameObject L_E_NodeInternalToSink_NodeSink               = new GameObject("L_E_NodeInternalToSink_NodeSink", typeof(MonoEdge));

        // Right edges
        public GameObject R_Root                            = new GameObject("R_Root", typeof(CircleCollider2D), typeof(MonoNode));
        public GameObject R_NodeA                           = new GameObject("R_NodeA", typeof(CircleCollider2D), typeof(MonoNode));
        public GameObject R_NodeB                           = new GameObject("R_NodeB", typeof(CircleCollider2D), typeof(MonoNode));
        // Right edges
        public GameObject R_E_Root_NodeA                    = new GameObject("R_E_Root_NodeA", typeof(MonoEdge));
        public GameObject R_E_Root_NodeA_Internal           = new GameObject("R_E_Root_NodeA_Internal", typeof(MonoEdge));
        public GameObject R_E_Root_NodeA_EmptyArrowWeight   = new GameObject("R_E_Root_NodeA_EmptyArrowWeight", typeof(MonoEdge));
        public GameObject R_E_NodeA_NodeB_Internal          = new GameObject("R_E_NodeA_NodeB_Internal", typeof(MonoEdge));
        public GameObject R_E_NodeB_Root_Internal           = new GameObject("R_E_NodeB_Root_Internal", typeof(MonoEdge));

        public SampleGraph_WeakBisim()
        {
            CreateGraph();
        }

        public SampleGraph_WeakBisim(bool bDefenderHasAdditionalEdges)
        {
            if(bDefenderHasAdditionalEdges)
            {
                CreateGraphWithAdditionalDefenderEdges();
            }
            else
            {
                CreateGraph();
            }
        }

        private void CreateGraph()
        {
            L_Root
                .AddEdge(L_NodeA, L_E_Root_NodeA);
            L_Root
                .AddEdgeWithWeight(L_NodeC, L_E_Root_NodeC_Internal, EGameEdgeWeight.InternalMove);
            L_Root
                .AddEdgeWithWeight(L_NodeC, R_E_Root_NodeA_EmptyArrowWeight, EGameEdgeWeight.EmptyArrowWeight);
            L_NodeA
                .AddEdgeWithWeight(L_NodeB, L_E_NodeA_NodeB_Internal, EGameEdgeWeight.InternalMove);
            L_NodeB
                .AddEdge(L_Root, L_E_NodeB_Root);
            L_NodeB
                .AddEdgeWithWeight(L_NodeInternalToSink, L_E_NodeB_NodeInternalToSink_Internal, EGameEdgeWeight.InternalMove);
            L_NodeC
                .AddEdge(L_NodeA, L_E_NodeC_NodeA);
            L_NodeInternalToSink
                .AddEdge(L_NodeSink, L_E_NodeInternalToSink_NodeSink);

            R_Root
                .AddEdge(R_NodeA, R_E_Root_NodeA);
            R_NodeA
                .AddEdgeWithWeight(R_NodeB, R_E_NodeA_NodeB_Internal, EGameEdgeWeight.InternalMove);
            R_NodeB
                .AddEdgeWithWeight(R_Root, R_E_NodeB_Root_Internal, EGameEdgeWeight.InternalMove);
        }
        private void CreateGraphWithAdditionalDefenderEdges()
        {
            L_Root
                .AddEdge(L_NodeA, L_E_Root_NodeA);
            L_Root
                .AddEdgeWithWeight(L_NodeC, L_E_Root_NodeC_Internal, EGameEdgeWeight.InternalMove);
            L_NodeA
                .AddEdgeWithWeight(L_NodeB, L_E_NodeA_NodeB_Internal, EGameEdgeWeight.InternalMove);
            L_NodeB
                .AddEdge(L_Root, L_E_NodeB_Root);
            L_NodeB
                .AddEdgeWithWeight(L_NodeInternalToSink, L_E_NodeB_NodeInternalToSink_Internal, EGameEdgeWeight.InternalMove);
            L_NodeC
                .AddEdge(L_NodeA, L_E_NodeC_NodeA);
            L_NodeInternalToSink
                .AddEdge(L_NodeSink, L_E_NodeInternalToSink_NodeSink);

            R_Root
                .AddEdge(R_NodeA, R_E_Root_NodeA);
            R_Root
                 .AddEdgeWithWeight(R_NodeA, R_E_Root_NodeA_Internal, EGameEdgeWeight.InternalMove);
            R_NodeA
                .AddEdgeWithWeight(R_NodeB, R_E_NodeA_NodeB_Internal, EGameEdgeWeight.InternalMove);
            R_NodeB
                .AddEdgeWithWeight(R_Root, R_E_NodeB_Root_Internal, EGameEdgeWeight.InternalMove);
        }
    }
}

