
using UnityEngine;

using CoupledSimilarity.Game.Graph;

namespace Tests
{
    public interface ITestGraph
    {
        IGraphNode<Transform, EGameEdgeWeight> AttackerStartNode { get; }
        IGraphNode<Transform, EGameEdgeWeight> DefenderStartNode { get; }
    }
}

