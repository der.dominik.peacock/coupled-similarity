
using UnityEngine;

using CoupledSimilarity.Game.Graph;

namespace Tests
{
    public static class SampleGraphUtility
    {
        public static void AddEdge(this GameObject fromNode, GameObject toNode, GameObject withEdgeObject)
        {
            AddEdgeWithWeight(fromNode, toNode, withEdgeObject, EGameEdgeWeight.StraightArrowWeight);
        }
        public static void AddEdgeWithWeight(this GameObject fromNode, GameObject toNode, GameObject withEdgeObject, EGameEdgeWeight weight)
        {
            withEdgeObject.AsEdge().EdgeWeight = weight;
            withEdgeObject.AsEdge().SetUpForTest(
                fromNode.AsNode(),
                toNode.AsNode()
                );
        }

        public static MonoNode AsNode(this GameObject gameObject)
        {
            return gameObject.GetComponent<MonoNode>();
        }
        public static MonoEdge AsEdge(this GameObject gameObject)
        {
            return gameObject.GetComponent<MonoEdge>();
        }
    }
}

