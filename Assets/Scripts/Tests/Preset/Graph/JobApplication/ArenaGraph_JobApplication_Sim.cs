
using UnityEngine;

using CoupledSimilarity.Game.Player.AI;
using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Utility;

namespace Tests
{
    public class ArenaGraph_JobApplication_Sim : ITestGraph
    {
        public SampleGraph_JobApplication GameGraph { get; private set; }

        public IGraphNode<Transform, EGameEdgeWeight> AttackerStartNode => GameGraph.AttackerStartNode;
        public IGraphNode<Transform, EGameEdgeWeight> DefenderStartNode => GameGraph.DefenderStartNode;

        // Naming convention [AttackerNode]_[DefenderNode]
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LRoot_RRoot;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeA_RRoot_DefTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeA_RNodeA_AttTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeA_RSubRoot_AttTurn;

        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeB_RRoot_DefTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeB_RNodeA_AttTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeB_RSubRoot_AttTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LChildOfNodeB_RNodeA_DefTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LChildOfNodeB_RSubRoot_DefTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LChildOfNodeB_RNodeB_AttTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LChildOfNodeB_RNodeC_AttTurn;

        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeC_RRoot_DefTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeC_RNodeA_AttTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeC_RSubRoot_DefTurn;

        public ArenaGraph_JobApplication_Sim()
        {
            GameGraph = new SampleGraph_JobApplication();
            CreateEdgesWithSameWeights();
        }

        public ArenaGraph_JobApplication_Sim(SampleGraph_JobApplication gameGraph)
        {
            GameGraph = gameGraph;
            CreateEdgesWithSameWeights();
        }

        public ArenaGraph_JobApplication_Sim(bool bCreateAllEdgesWithSameWeights)
        {
            if (bCreateAllEdgesWithSameWeights)
            {
                GameGraph = new SampleGraph_JobApplication();
                CreateEdgesWithSameWeights();
            }
            else
            {
                GameGraph = new SampleGraph_JobApplication(false);
                CreateEdgesWithDifferentWeights();
            }
        }

        private void CreateEdgesWithSameWeights()
        {
            LRoot_RRoot = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                new FArenaNodeValue(
                    EPlayerType.Attacker,
                    GameGraph.AttackerStartNode,
                    GameGraph.DefenderStartNode
                    )
                );

            // Go to L_NodeA
            {
                LNodeA_RRoot_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                new FArenaNodeValue(
                    EPlayerType.Defender,
                    GameGraph.L_NodeA.AsNode(),
                    GameGraph.DefenderStartNode
                    )
                );
                LNodeA_RNodeA_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                     new FArenaNodeValue(
                         EPlayerType.Attacker,
                         GameGraph.L_NodeA.AsNode(),
                         GameGraph.R_NodeA.AsNode()
                         )
                     );
                LNodeA_RSubRoot_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                     new FArenaNodeValue(
                         EPlayerType.Attacker,
                         GameGraph.L_NodeA.AsNode(),
                         GameGraph.R_SubRoot.AsNode()
                         )
                     );

                LRoot_RRoot.AddNeighbour(
                    LNodeA_RRoot_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                LNodeA_RRoot_DefTurn.AddNeighbour(
                     LNodeA_RNodeA_AttTurn,
                     new FArenaEdgeValue(
                         Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                         )
                     );
                LNodeA_RRoot_DefTurn.AddNeighbour(
                    LNodeA_RSubRoot_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
            }

            // Go to L_NodeB
            {
                LNodeB_RRoot_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                new FArenaNodeValue(
                    EPlayerType.Defender,
                    GameGraph.L_NodeB.AsNode(),
                    GameGraph.DefenderStartNode
                    )
                );
                LNodeB_RNodeA_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                     new FArenaNodeValue(
                         EPlayerType.Attacker,
                         GameGraph.L_NodeB.AsNode(),
                         GameGraph.R_NodeA.AsNode()
                         )
                     );
                LNodeB_RSubRoot_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                      new FArenaNodeValue(
                          EPlayerType.Attacker,
                          GameGraph.L_NodeB.AsNode(),
                          GameGraph.R_SubRoot.AsNode()
                          )
                      );

                LChildOfNodeB_RNodeA_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                     new FArenaNodeValue(
                         EPlayerType.Defender,
                         GameGraph.L_NodeChildOfB.AsNode(),
                         GameGraph.R_NodeA.AsNode()
                         )
                     );

                LChildOfNodeB_RSubRoot_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                     new FArenaNodeValue(
                         EPlayerType.Defender,
                         GameGraph.L_NodeChildOfB.AsNode(),
                         GameGraph.R_SubRoot.AsNode()
                         )
                     );
                LChildOfNodeB_RNodeB_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                     new FArenaNodeValue(
                         EPlayerType.Attacker,
                         GameGraph.L_NodeChildOfB.AsNode(),
                         GameGraph.R_NodeB.AsNode()
                         )
                     );
                LChildOfNodeB_RNodeC_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                     new FArenaNodeValue(
                         EPlayerType.Attacker,
                         GameGraph.L_NodeChildOfB.AsNode(),
                         GameGraph.R_NodeC.AsNode()
                         )
                     );

                LRoot_RRoot.AddNeighbour(
                    LNodeB_RRoot_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                LNodeB_RRoot_DefTurn.AddNeighbour(
                     LNodeB_RNodeA_AttTurn,
                     new FArenaEdgeValue(
                         Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                         )
                     );
                LNodeB_RRoot_DefTurn.AddNeighbour(
                    LNodeB_RSubRoot_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );

                LNodeB_RNodeA_AttTurn.AddNeighbour(
                    LChildOfNodeB_RNodeA_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );

                LNodeB_RSubRoot_AttTurn.AddNeighbour(
                    LChildOfNodeB_RSubRoot_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                LChildOfNodeB_RSubRoot_DefTurn.AddNeighbour(
                    LChildOfNodeB_RNodeB_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                LChildOfNodeB_RSubRoot_DefTurn.AddNeighbour(
                    LChildOfNodeB_RNodeC_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
            }

            // Go to L_NodeC
            {
                LNodeC_RRoot_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Defender,
                        GameGraph.L_NodeC.AsNode(),
                        GameGraph.DefenderStartNode
                        )
                    );
                LNodeC_RNodeA_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                     new FArenaNodeValue(
                         EPlayerType.Attacker,
                         GameGraph.L_NodeC.AsNode(),
                         GameGraph.R_NodeA.AsNode()
                         )
                     );
                LNodeC_RSubRoot_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                     new FArenaNodeValue(
                         EPlayerType.Attacker,
                         GameGraph.L_NodeC.AsNode(),
                         GameGraph.R_SubRoot.AsNode()
                         )
                     );

                LRoot_RRoot.AddNeighbour(
                    LNodeC_RRoot_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                LNodeC_RRoot_DefTurn.AddNeighbour(
                     LNodeC_RNodeA_AttTurn,
                     new FArenaEdgeValue(
                         Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                         )
                     );
                LNodeC_RRoot_DefTurn.AddNeighbour(
                    LNodeC_RSubRoot_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
            }
        }

        private void CreateEdgesWithDifferentWeights()
        {
            LRoot_RRoot = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                new FArenaNodeValue(
                    EPlayerType.Attacker,
                    GameGraph.AttackerStartNode,
                    GameGraph.DefenderStartNode
                    )
                );

            // Go to L_NodeA
            {
                LNodeA_RRoot_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                new FArenaNodeValue(
                    EPlayerType.Defender,
                    GameGraph.L_NodeA.AsNode(),
                    GameGraph.DefenderStartNode
                    )
                );
                LNodeA_RNodeA_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                     new FArenaNodeValue(
                         EPlayerType.Attacker,
                         GameGraph.L_NodeA.AsNode(),
                         GameGraph.R_NodeA.AsNode()
                         )
                     );

                LRoot_RRoot.AddNeighbour(
                    LNodeA_RRoot_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                LNodeA_RRoot_DefTurn.AddNeighbour(
                     LNodeA_RNodeA_AttTurn,
                     new FArenaEdgeValue(
                         Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                         )
                     );
            }

            // Go to L_NodeB
            {
                LNodeB_RRoot_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                new FArenaNodeValue(
                    EPlayerType.Defender,
                    GameGraph.L_NodeB.AsNode(),
                    GameGraph.DefenderStartNode
                    )
                );
                LNodeB_RSubRoot_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                      new FArenaNodeValue(
                          EPlayerType.Attacker,
                          GameGraph.L_NodeB.AsNode(),
                          GameGraph.R_SubRoot.AsNode()
                          )
                      );

                LChildOfNodeB_RSubRoot_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                     new FArenaNodeValue(
                         EPlayerType.Defender,
                         GameGraph.L_NodeChildOfB.AsNode(),
                         GameGraph.R_SubRoot.AsNode()
                         )
                     );
                LChildOfNodeB_RNodeB_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                     new FArenaNodeValue(
                         EPlayerType.Attacker,
                         GameGraph.L_NodeChildOfB.AsNode(),
                         GameGraph.R_NodeB.AsNode()
                         )
                     );

                LRoot_RRoot.AddNeighbour(
                    LNodeB_RRoot_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                LNodeB_RRoot_DefTurn.AddNeighbour(
                    LNodeB_RSubRoot_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );

                LNodeB_RSubRoot_AttTurn.AddNeighbour(
                    LChildOfNodeB_RSubRoot_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                LChildOfNodeB_RSubRoot_DefTurn.AddNeighbour(
                    LChildOfNodeB_RNodeB_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
            }

            // Go to L_NodeC
            {
                LNodeC_RRoot_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Defender,
                        GameGraph.L_NodeC.AsNode(),
                        GameGraph.DefenderStartNode
                        )
                    );
                LNodeC_RNodeA_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                     new FArenaNodeValue(
                         EPlayerType.Attacker,
                         GameGraph.L_NodeC.AsNode(),
                         GameGraph.R_NodeA.AsNode()
                         )
                     );

                LRoot_RRoot.AddNeighbour(
                    LNodeC_RRoot_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                LNodeC_RRoot_DefTurn.AddNeighbour(
                     LNodeC_RNodeA_AttTurn,
                     new FArenaEdgeValue(
                         Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                         )
                     );
            }
        }
    }
}

