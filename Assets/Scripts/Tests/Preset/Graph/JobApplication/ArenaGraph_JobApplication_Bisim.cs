
using CoupledSimilarity.Game.Player.AI;
using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Utility;

namespace Tests
{
    public class ArenaGraph_JobApplication_Bisim : ArenaGraph_JobApplication_Sim
    {
        // Naming convention B_[AttackerNode]_[DefenderNode]_[Player]Turn
        // B_ is for differentiating fields from the class' parent
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> B_RRoot_LRoot_AttTurn;

        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> B_RNodeA_LRoot_DefTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> B_RNodeA_LNodeA_AttTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> B_RNodeA_LNodeB_AttTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> B_RNodeA_LNodeC_AttTurn;

        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> B_LSubRoot_RRoot_DefTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> B_RSubRoot_LNodeA_AttTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> B_RSubRoot_LNodeB_AttTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> B_RSubRoot_LNodeC_AttTurn;

        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> B_RNodeB_LNodeA_DefTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> B_RNodeB_LNodeC_DefTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> B_RNodeB_LNodeB_DefTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> B_RNodeB_LNodeChildOfB_AttTurn;

        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> B_RNodeC_LNodeA_DefTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> B_RNodeC_LNodeC_DefTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> B_RNodeC_LNodeB_DefTurn;
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> B_RNodeC_LNodeChildOfB_AttTurn;

        public ArenaGraph_JobApplication_Bisim()
        {
            BuildGraph();
        }

        public ArenaGraph_JobApplication_Bisim(SampleGraph_JobApplication gameGraph)
            : base(gameGraph)
        {
            BuildGraph();
        }

        private void BuildGraph()
        {
            // Symmetry swap on root
            {
                B_RRoot_LRoot_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                new FArenaNodeValue(
                    EPlayerType.Attacker,
                    GameGraph.DefenderStartNode,
                    GameGraph.AttackerStartNode
                    )
                );

                LRoot_RRoot.AddNeighbour(
                    B_RRoot_LRoot_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
                B_RRoot_LRoot_AttTurn.AddNeighbour(
                    LRoot_RRoot,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
            }

            // Attacker moves Root to NodeA
            {

                B_RNodeA_LRoot_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Defender,
                        GameGraph.R_NodeA.AsNode(),
                        GameGraph.AttackerStartNode
                        )
                    );

                B_RRoot_LRoot_AttTurn.AddNeighbour(
                    B_RNodeA_LRoot_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );

            }
            // Defender moves after Attacker moved to NodeA
            {

                B_RNodeA_LNodeA_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Attacker,
                        GameGraph.R_NodeA.AsNode(),
                        GameGraph.L_NodeA.AsNode()
                        )
                    );
                B_RNodeA_LNodeB_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Attacker,
                        GameGraph.R_NodeA.AsNode(),
                        GameGraph.L_NodeB.AsNode()
                        )
                    );
                B_RNodeA_LNodeC_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Attacker,
                        GameGraph.R_NodeA.AsNode(),
                        GameGraph.L_NodeC.AsNode()
                        )
                    );

                B_RNodeA_LRoot_DefTurn.AddNeighbour(
                    B_RNodeA_LNodeA_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                B_RNodeA_LRoot_DefTurn.AddNeighbour(
                   B_RNodeA_LNodeB_AttTurn,
                   new FArenaEdgeValue(
                       Optional.FromNull<IAiArenaMove>(),
                       EMoveType.MoveOverEdge
                       )
                   );
                B_RNodeA_LRoot_DefTurn.AddNeighbour(
                   B_RNodeA_LNodeC_AttTurn,
                   new FArenaEdgeValue(
                       Optional.FromNull<IAiArenaMove>(),
                       EMoveType.MoveOverEdge
                       )
                   );
            }
            // Attacker moves Root to Subroot
            {
                B_LSubRoot_RRoot_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                new FArenaNodeValue(
                    EPlayerType.Defender,
                    GameGraph.R_SubRoot.AsNode(),
                    GameGraph.AttackerStartNode
                    )
                );

                B_RRoot_LRoot_AttTurn.AddNeighbour(
                    B_LSubRoot_RRoot_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );

            }
            // Defender moves after Attacker moved to SubRoot
            {
                B_RSubRoot_LNodeA_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                new FArenaNodeValue(
                    EPlayerType.Attacker,
                    GameGraph.R_SubRoot.AsNode(),
                    GameGraph.L_NodeA.AsNode()
                    )
                );
                B_RSubRoot_LNodeB_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                new FArenaNodeValue(
                    EPlayerType.Attacker,
                    GameGraph.R_SubRoot.AsNode(),
                    GameGraph.L_NodeB.AsNode()
                    )
                );
                B_RSubRoot_LNodeC_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                new FArenaNodeValue(
                    EPlayerType.Attacker,
                    GameGraph.R_SubRoot.AsNode(),
                    GameGraph.L_NodeC.AsNode()
                    )
                );

                B_LSubRoot_RRoot_DefTurn.AddNeighbour(
                    B_RSubRoot_LNodeA_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                B_LSubRoot_RRoot_DefTurn.AddNeighbour(
                    B_RSubRoot_LNodeB_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                B_LSubRoot_RRoot_DefTurn.AddNeighbour(
                   B_RSubRoot_LNodeC_AttTurn,
                   new FArenaEdgeValue(
                       Optional.FromNull<IAiArenaMove>(),
                       EMoveType.MoveOverEdge
                       )
                   );
            }
            // Attacker moves Subroot to NodeB
            {
                B_RNodeB_LNodeB_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                new FArenaNodeValue(
                    EPlayerType.Defender,
                    GameGraph.R_NodeB.AsNode(),
                    GameGraph.L_NodeB.AsNode()
                    )
                );
                B_RNodeB_LNodeChildOfB_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                new FArenaNodeValue(
                    EPlayerType.Attacker,
                    GameGraph.R_NodeB.AsNode(),
                    GameGraph.L_NodeChildOfB.AsNode()
                    )
                );
                B_RNodeB_LNodeA_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                new FArenaNodeValue(
                    EPlayerType.Defender,
                    GameGraph.R_NodeB.AsNode(),
                    GameGraph.L_NodeA.AsNode()
                    )
                );
                B_RNodeB_LNodeC_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                new FArenaNodeValue(
                    EPlayerType.Defender,
                    GameGraph.R_NodeB.AsNode(),
                    GameGraph.L_NodeC.AsNode()
                    )
                );

                B_RSubRoot_LNodeA_AttTurn.AddNeighbour(
                    B_RNodeB_LNodeA_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                B_RSubRoot_LNodeC_AttTurn.AddNeighbour(
                    B_RNodeB_LNodeC_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                B_RSubRoot_LNodeB_AttTurn.AddNeighbour(
                    B_RNodeB_LNodeB_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                B_RNodeB_LNodeB_DefTurn.AddNeighbour(
                    B_RNodeB_LNodeChildOfB_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
            }
            // Attacker moves Subroot to NodeC
            {
                B_RNodeC_LNodeB_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                new FArenaNodeValue(
                    EPlayerType.Defender,
                    GameGraph.R_NodeC.AsNode(),
                    GameGraph.L_NodeB.AsNode()
                    )
                );
                B_RNodeC_LNodeChildOfB_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                new FArenaNodeValue(
                    EPlayerType.Attacker,
                    GameGraph.R_NodeC.AsNode(),
                    GameGraph.L_NodeChildOfB.AsNode()
                    )
                );
                B_RNodeC_LNodeA_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                new FArenaNodeValue(
                    EPlayerType.Defender,
                    GameGraph.R_NodeC.AsNode(),
                    GameGraph.L_NodeA.AsNode()
                    )
                );
                B_RNodeC_LNodeC_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                new FArenaNodeValue(
                    EPlayerType.Defender,
                    GameGraph.R_NodeC.AsNode(),
                    GameGraph.L_NodeC.AsNode()
                    )
                );

                B_RSubRoot_LNodeA_AttTurn.AddNeighbour(
                    B_RNodeC_LNodeA_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                B_RSubRoot_LNodeC_AttTurn.AddNeighbour(
                    B_RNodeC_LNodeC_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                B_RSubRoot_LNodeB_AttTurn.AddNeighbour(
                    B_RNodeC_LNodeB_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                B_RNodeC_LNodeB_DefTurn.AddNeighbour(
                    B_RNodeC_LNodeChildOfB_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
            }


            // Symmetry swap on L_NodeA
            {
                LNodeA_RNodeA_AttTurn.AddNeighbour(
                    B_RNodeA_LNodeA_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
                B_RNodeA_LNodeA_AttTurn.AddNeighbour(
                    LNodeA_RNodeA_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
            }
            // Symmetry swap on L_NodeB
            {
                // Already handled by R_Subroot; see below.
            }
            // Symmetry swap on L_NodeC
            {
                LNodeC_RNodeA_AttTurn.AddNeighbour(
                    B_RNodeA_LNodeC_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
                B_RNodeA_LNodeC_AttTurn.AddNeighbour(
                    LNodeC_RNodeA_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
                LNodeC_RSubRoot_DefTurn.AddNeighbour(
                    B_RSubRoot_LNodeC_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
                B_RSubRoot_LNodeC_AttTurn.AddNeighbour(
                    LNodeC_RSubRoot_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
            }
            // Symmetry swap on L_ChildOfB
            {
                // Already handled by R_NodeB and R_NodeC; see below.
            }
            // Symmetry swap on R_NodeA
            {
                B_RNodeA_LNodeB_AttTurn.AddNeighbour(
                    LNodeB_RNodeA_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
                LNodeB_RNodeA_AttTurn.AddNeighbour(
                    B_RNodeA_LNodeB_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
            }
            // Symmetry swap on R_Subroot
            {
                B_RSubRoot_LNodeA_AttTurn.AddNeighbour(
                    LNodeA_RSubRoot_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
                LNodeA_RSubRoot_AttTurn.AddNeighbour(
                    B_RSubRoot_LNodeA_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
                B_RSubRoot_LNodeB_AttTurn.AddNeighbour(
                    LNodeB_RSubRoot_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
                LNodeB_RSubRoot_AttTurn.AddNeighbour(
                    B_RSubRoot_LNodeB_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
            }
            // Symmetry swap on R_NodeB
            {
                B_RNodeB_LNodeChildOfB_AttTurn.AddNeighbour(
                    LChildOfNodeB_RNodeB_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
                LChildOfNodeB_RNodeB_AttTurn.AddNeighbour(
                    B_RNodeB_LNodeChildOfB_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
            }
            // Symmetry swap on R_NodeC
            {
                B_RNodeC_LNodeChildOfB_AttTurn.AddNeighbour(
                    LChildOfNodeB_RNodeC_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
                LChildOfNodeB_RNodeC_AttTurn.AddNeighbour(
                    B_RNodeC_LNodeChildOfB_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
            }
        }
    }
}

