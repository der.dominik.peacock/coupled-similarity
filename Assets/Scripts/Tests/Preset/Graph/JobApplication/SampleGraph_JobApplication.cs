
using UnityEngine;

using CoupledSimilarity.Game.Graph;

namespace Tests
{
    public class SampleGraph_JobApplication : ITestGraph
    {
        public IGraphNode<Transform, EGameEdgeWeight> AttackerStartNode => L_Root.AsNode();
        public IGraphNode<Transform, EGameEdgeWeight> DefenderStartNode => R_Root.AsNode();

        // Left nodes
        public GameObject L_Root                = new GameObject("L_Root", typeof(CircleCollider2D), typeof(MonoNode));
        public GameObject L_NodeA               = new GameObject("L_NodeA", typeof(CircleCollider2D), typeof(MonoNode));
        public GameObject L_NodeB               = new GameObject("L_NodeB", typeof(CircleCollider2D), typeof(MonoNode));
        public GameObject L_NodeChildOfB        = new GameObject("L_NodeChildOfB", typeof(CircleCollider2D), typeof(MonoNode));
        public GameObject L_NodeC               = new GameObject("L_NodeC", typeof(CircleCollider2D), typeof(MonoNode));
        // Left edges
        public GameObject L_E_Root_NodeA        = new GameObject("L_E_Root_NodeA", typeof(MonoEdge));
        public GameObject L_E_Root_NodeB        = new GameObject("L_E_Root_NodeB", typeof(MonoEdge));
        public GameObject L_E_NodeB_ChildOfB    = new GameObject("L_E_NodeB_ChildOfB", typeof(MonoEdge));
        public GameObject L_E_Root_NodeC        = new GameObject("L_E_Root_NodeC", typeof(MonoEdge));

        // Right edges
        public GameObject R_Root                = new GameObject("R_Root", typeof(CircleCollider2D), typeof(MonoNode));
        public GameObject R_SubRoot             = new GameObject("R_SubRoot", typeof(CircleCollider2D), typeof(MonoNode));
        public GameObject R_NodeA               = new GameObject("R_NodeA", typeof(CircleCollider2D), typeof(MonoNode));
        public GameObject R_NodeB               = new GameObject("R_NodeB", typeof(CircleCollider2D), typeof(MonoNode));
        public GameObject R_NodeC               = new GameObject("R_NodeC", typeof(CircleCollider2D), typeof(MonoNode));
        // Right edges
        public GameObject R_E_Root_NodeA        = new GameObject("R_E_Root_NodeA", typeof(MonoEdge));
        public GameObject R_E_Root_SubRoot      = new GameObject("R_E_Root_SubRoot", typeof(MonoEdge));
        public GameObject R_E_SubRoot_NodeB     = new GameObject("R_E_SubRoot_NodeB", typeof(MonoEdge));
        public GameObject R_E_SubRoot_NodeC     = new GameObject("R_E_SubRoot_NodeC", typeof(MonoEdge));

        public SampleGraph_JobApplication()
        {
            CreateEdgesWithSameWeights();
        }
        public SampleGraph_JobApplication(bool bCreateAllEdgesWithSameWeights)
        {
            if(bCreateAllEdgesWithSameWeights)
            {
                CreateEdgesWithSameWeights();
            }
            else
            {
                CreateEdgesWithDifferentWeights();
            }
        }

        private void CreateEdgesWithSameWeights()
        {
            L_Root.AddEdge(L_NodeA, L_E_Root_NodeA);
            L_Root.AddEdge(L_NodeB, L_E_Root_NodeB);
            L_Root.AddEdge(L_NodeC, L_E_Root_NodeC);
            L_NodeB.AddEdge(L_NodeChildOfB, L_E_NodeB_ChildOfB);

            R_Root.AddEdge(R_NodeA, R_E_Root_NodeA);
            R_Root.AddEdge(R_SubRoot, R_E_Root_SubRoot);
            R_SubRoot.AddEdge(R_NodeB, R_E_SubRoot_NodeB);
            R_SubRoot.AddEdge(R_NodeC, R_E_SubRoot_NodeC);
        }

        private void CreateEdgesWithDifferentWeights()
        {
            L_Root.AddEdge(L_NodeA, L_E_Root_NodeA);
            L_Root.AddEdgeWithWeight(L_NodeB, L_E_Root_NodeB, EGameEdgeWeight.EmptyArrowWeight);
            L_Root.AddEdge(L_NodeC, L_E_Root_NodeC);
            L_NodeB.AddEdge(L_NodeChildOfB, L_E_NodeB_ChildOfB);

            R_Root.AddEdge(R_NodeA, R_E_Root_NodeA);
            R_Root.AddEdgeWithWeight(R_SubRoot, R_E_Root_SubRoot, EGameEdgeWeight.EmptyArrowWeight);
            R_SubRoot.AddEdge(R_NodeB, R_E_SubRoot_NodeB);
            R_SubRoot.AddEdgeWithWeight(R_NodeC, R_E_SubRoot_NodeC, EGameEdgeWeight.EmptyArrowWeight);
        }
    }
}

