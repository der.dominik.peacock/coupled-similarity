
using CoupledSimilarity.Game.Player.AI;
using CoupledSimilarity.Game.Graph;

namespace Tests
{
    public class WinningRegionGraph_JobApplication_Bisim
    {
        public ArenaGraph_JobApplication_Bisim BaseGraph { get; } = new ArenaGraph_JobApplication_Bisim();
        public FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> Root => BaseGraph.LRoot_RRoot;

        public WinningRegionGraph_JobApplication_Bisim(SampleGraph_JobApplication gameGraph)
        {
            BaseGraph = new ArenaGraph_JobApplication_Bisim(gameGraph);
            // Simulation
            BaseGraph.LRoot_RRoot.Value.WinningRegion
                = EWinningRegion.Defender;

            BaseGraph.LNodeA_RRoot_DefTurn.Value.WinningRegion
                = EWinningRegion.Defender;
            BaseGraph.LNodeA_RNodeA_AttTurn.Value.WinningRegion
                = EWinningRegion.Defender;
            BaseGraph.LNodeA_RSubRoot_AttTurn.Value.WinningRegion
                = EWinningRegion.Attacker;

            BaseGraph.LNodeB_RRoot_DefTurn.Value.WinningRegion
                = EWinningRegion.Defender;
            BaseGraph.LNodeB_RNodeA_AttTurn.Value.WinningRegion
                = EWinningRegion.Attacker;
            BaseGraph.LNodeB_RSubRoot_AttTurn.Value.WinningRegion
                = EWinningRegion.Defender;
            BaseGraph.LChildOfNodeB_RNodeA_DefTurn.Value.WinningRegion
                = EWinningRegion.Attacker;
            BaseGraph.LChildOfNodeB_RSubRoot_DefTurn.Value.WinningRegion
                = EWinningRegion.Defender;
            BaseGraph.LChildOfNodeB_RNodeB_AttTurn.Value.WinningRegion
                = EWinningRegion.Defender;
            BaseGraph.LChildOfNodeB_RNodeC_AttTurn.Value.WinningRegion
                = EWinningRegion.Defender;

            BaseGraph.LNodeC_RRoot_DefTurn.Value.WinningRegion
                = EWinningRegion.Defender;
            BaseGraph.LNodeC_RNodeA_AttTurn.Value.WinningRegion
                = EWinningRegion.Defender;
            BaseGraph.LNodeC_RSubRoot_DefTurn.Value.WinningRegion
                = EWinningRegion.Attacker;

            // Bisimulation
            BaseGraph.B_RRoot_LRoot_AttTurn.Value.WinningRegion
                = EWinningRegion.Defender;

            BaseGraph.B_RNodeA_LRoot_DefTurn.Value.WinningRegion
                = EWinningRegion.Defender;
            BaseGraph.B_RNodeA_LNodeA_AttTurn.Value.WinningRegion
                = EWinningRegion.Defender;
            BaseGraph.B_RNodeA_LNodeB_AttTurn.Value.WinningRegion
                = EWinningRegion.Attacker;
            BaseGraph.B_RNodeA_LNodeC_AttTurn.Value.WinningRegion
                = EWinningRegion.Defender;

            BaseGraph.B_LSubRoot_RRoot_DefTurn.Value.WinningRegion
                = EWinningRegion.Defender;
            BaseGraph.B_RSubRoot_LNodeA_AttTurn.Value.WinningRegion
                = EWinningRegion.Attacker;
            BaseGraph.B_RSubRoot_LNodeB_AttTurn.Value.WinningRegion
                = EWinningRegion.Defender;
            BaseGraph.B_RSubRoot_LNodeC_AttTurn.Value.WinningRegion
                = EWinningRegion.Attacker;

            BaseGraph.B_RNodeB_LNodeA_DefTurn.Value.WinningRegion
                = EWinningRegion.Attacker;
            BaseGraph.B_RNodeB_LNodeC_DefTurn.Value.WinningRegion
                = EWinningRegion.Attacker;
            BaseGraph.B_RNodeB_LNodeB_DefTurn.Value.WinningRegion
                = EWinningRegion.Defender;
            BaseGraph.B_RNodeB_LNodeChildOfB_AttTurn.Value.WinningRegion
                = EWinningRegion.Defender;

            BaseGraph.B_RNodeC_LNodeA_DefTurn.Value.WinningRegion
                = EWinningRegion.Attacker;
            BaseGraph.B_RNodeC_LNodeC_DefTurn.Value.WinningRegion
                = EWinningRegion.Attacker;
            BaseGraph.B_RNodeC_LNodeB_DefTurn.Value.WinningRegion
                = EWinningRegion.Defender;
            BaseGraph.B_RNodeC_LNodeChildOfB_AttTurn.Value.WinningRegion
                = EWinningRegion.Defender;
        }
    }
}

