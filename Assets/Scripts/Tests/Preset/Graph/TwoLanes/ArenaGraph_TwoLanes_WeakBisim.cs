
using UnityEngine;

using CoupledSimilarity.Game.Player.AI;
using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Utility;

namespace Tests
{
    public class ArenaGraph_TwoLanes_WeakBisim : ITestGraph
    {
        public readonly SampleGraph_TwoLanes GameGraph = new SampleGraph_TwoLanes();

        public IGraphNode<Transform, EGameEdgeWeight> AttackerStartNode => GameGraph.L_Root.AsNode();
        public IGraphNode<Transform, EGameEdgeWeight> DefenderStartNode => GameGraph.R_Root.AsNode();

        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LRoot_RRoot; 
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeA_RRoot; 
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> RRoot_LRoot; 
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> RNodeA_LRoot; 

        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeA_RNodeA_DefTurn; 
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeA_RNodeB;
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeA_RNodeB_AttTurn;
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeA_RNodeC;
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeA_RNodeC_Finish;
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> RNodeA_LNodeA;

        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeA_RNodeA_AttTurn;
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> RNodeB_LNodeA;
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> RNodeC_LNodeA;
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> RNodeC_LNodeA_DefTurn;

        public ArenaGraph_TwoLanes_WeakBisim()
        {
            // 1st move: Attacker
            {
                LRoot_RRoot = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Attacker,
                        GameGraph.L_Root.AsNode(),
                        GameGraph.R_Root.AsNode()
                    )
                );
                LNodeA_RRoot = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Defender,
                        GameGraph.L_NodeA.AsNode(),
                        GameGraph.R_Root.AsNode()
                    )
                );
                RRoot_LRoot = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Attacker,
                        GameGraph.R_Root.AsNode(),
                        GameGraph.L_Root.AsNode()
                    )
                );
                RNodeA_LRoot = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Defender,
                        GameGraph.R_NodeA.AsNode(),
                        GameGraph.L_Root.AsNode()
                    )
                );

                LRoot_RRoot.AddNeighbour(
                    LNodeA_RRoot,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                LRoot_RRoot.AddNeighbour(
                    RRoot_LRoot,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
                RRoot_LRoot.AddNeighbour(
                    LRoot_RRoot,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
                RRoot_LRoot.AddNeighbour(
                    RNodeA_LRoot,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
            }
            // 2nd move: Defender
            {
                LNodeA_RNodeA_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Defender,
                        GameGraph.L_NodeA.AsNode(),
                        GameGraph.R_NodeA.AsNode()
                    )
                );
                LNodeA_RNodeB = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Defender,
                        GameGraph.L_NodeA.AsNode(),
                        GameGraph.R_NodeB.AsNode()
                    )
                );
                LNodeA_RNodeC = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Defender,
                        GameGraph.L_NodeA.AsNode(),
                        GameGraph.R_NodeC.AsNode()
                    )
                );
                LNodeA_RNodeC_Finish = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Attacker,
                        GameGraph.L_NodeA.AsNode(),
                        GameGraph.R_NodeC.AsNode()
                    )
                );
                RNodeA_LNodeA = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Attacker,
                        GameGraph.R_NodeA.AsNode(),
                        GameGraph.L_NodeA.AsNode()
                    )
                );

                LNodeA_RRoot.AddNeighbour(
                    LNodeA_RNodeA_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                LNodeA_RNodeA_DefTurn.AddNeighbour(
                    LNodeA_RNodeB,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                
                LNodeA_RNodeB.AddNeighbour(
                    LNodeA_RNodeC,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                LNodeA_RNodeC.AddNeighbour(
                    LNodeA_RNodeC_Finish,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.FinishInternalMovesAsDefender
                        )
                    );
            }
            // 3rd move: Attacker
            {
                RNodeB_LNodeA = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Attacker,
                        GameGraph.R_NodeB.AsNode(),
                        GameGraph.L_NodeA.AsNode()
                    )
                );
                LNodeA_RNodeA_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Attacker,
                        GameGraph.L_NodeA.AsNode(),
                        GameGraph.R_NodeA.AsNode()
                    )
                );
                LNodeA_RNodeB_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Attacker,
                        GameGraph.L_NodeA.AsNode(),
                        GameGraph.R_NodeB.AsNode()
                    )
                );
                RNodeC_LNodeA = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Attacker,
                        GameGraph.R_NodeC.AsNode(),
                        GameGraph.L_NodeA.AsNode()
                    )
                );
                RNodeC_LNodeA_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Defender,
                        GameGraph.R_NodeC.AsNode(),
                        GameGraph.L_NodeA.AsNode()
                    )
                );

                RNodeB_LNodeA.AddNeighbour(
                    LNodeA_RNodeB_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
                RNodeB_LNodeA.AddNeighbour(
                    RNodeC_LNodeA_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                LNodeA_RNodeB_AttTurn.AddNeighbour(
                    RNodeB_LNodeA,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
                RNodeA_LNodeA.AddNeighbour(
                    RNodeB_LNodeA,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );

                // From previous defender's turn
                LNodeA_RNodeB.AddNeighbour(
                    LNodeA_RNodeB_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.FinishInternalMovesAsDefender
                        )
                    );
                LNodeA_RNodeC_Finish.AddNeighbour(
                    RNodeC_LNodeA,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
                RNodeC_LNodeA.AddNeighbour(
                    LNodeA_RNodeC_Finish,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
            }
        }

       
    }
}

