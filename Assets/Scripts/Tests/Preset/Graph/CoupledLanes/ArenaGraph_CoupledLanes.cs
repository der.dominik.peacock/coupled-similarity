
using UnityEngine;

using CoupledSimilarity.Game.Player.AI;
using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Utility;

namespace Tests
{
    public class ArenaGraph_CoupledLanes : ITestGraph
    {
        public readonly SampleGraph_CoupledLanes GameGraph = new SampleGraph_CoupledLanes();

        public IGraphNode<Transform, EGameEdgeWeight> AttackerStartNode => GameGraph.L_Root.AsNode();
        public IGraphNode<Transform, EGameEdgeWeight> DefenderStartNode => GameGraph.R_Root.AsNode();

        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LRoot_RRoot_AttTurn;
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeA_RRoot_DefTurn;
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LRoot_RRoot_Sym_DefTurn;

        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> RRoot_LRoot_AttTurn;
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeA_RNodeA_DefTurn;
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeA_RNodeB_DefTurn;
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeA_RNodeB_AttTurn;
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LRoot_RNodeA_DefTurn;
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> RNodeA_LRoot_AttTurn; //

        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> RNodeA_LRoot_DefTurn; // //
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> RNodeB_LRoot_DefTurn;
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> RRoot_LRoot_Sym_DefTurn;
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LNodeA_RNodeB_Sym_DefTurn;
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> RNodeA_LRoot_Sym_DefTurn; //

        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> LRoot_RNodeA_AttTurn;
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> RNodeB_LNodeA_DefTurn;
        public readonly FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue> RNodeB_LNodeA_AttTurn;


        public ArenaGraph_CoupledLanes()
        {
            // 1st move: Attacker
            {
                LRoot_RRoot_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Attacker,
                        GameGraph.L_Root.AsNode(),
                        GameGraph.R_Root.AsNode()
                    )
                );
                LNodeA_RRoot_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Defender,
                        GameGraph.L_NodeA.AsNode(),
                        GameGraph.R_Root.AsNode()
                    )
                );
                LRoot_RRoot_Sym_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Defender,
                        GameGraph.L_Root.AsNode(),
                        GameGraph.R_Root.AsNode()
                    )
                );

                LRoot_RRoot_AttTurn.AddNeighbour(
                    LNodeA_RRoot_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                LRoot_RRoot_AttTurn.AddNeighbour(
                    LRoot_RRoot_Sym_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
            }
            // 2nd Move: Defender
            {
                RRoot_LRoot_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Attacker,
                        GameGraph.R_Root.AsNode(),
                        GameGraph.L_Root.AsNode()
                    )
                );
                LNodeA_RNodeA_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Defender,
                        GameGraph.L_NodeA.AsNode(),
                        GameGraph.R_NodeA.AsNode()
                    )
                );
                LNodeA_RNodeB_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Defender,
                        GameGraph.L_NodeA.AsNode(),
                        GameGraph.R_NodeB.AsNode()
                    )
                );
                LNodeA_RNodeB_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Attacker,
                        GameGraph.L_NodeA.AsNode(),
                        GameGraph.R_NodeB.AsNode()
                    )
                );
                LRoot_RNodeA_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Defender,
                        GameGraph.L_Root.AsNode(),
                        GameGraph.R_NodeA.AsNode()
                    )
                );
                RNodeA_LRoot_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Attacker,
                        GameGraph.R_NodeA.AsNode(),
                        GameGraph.L_Root.AsNode()
                    )
                );

                LNodeA_RRoot_DefTurn.AddNeighbour(
                    LNodeA_RNodeA_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                LNodeA_RRoot_DefTurn.AddNeighbour(
                    LNodeA_RNodeB_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                LNodeA_RNodeB_DefTurn.AddNeighbour(
                    LNodeA_RNodeB_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.FinishInternalMovesAsDefender
                        )
                    );
                LRoot_RRoot_Sym_DefTurn.AddNeighbour(
                    LRoot_RNodeA_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                LRoot_RRoot_Sym_DefTurn.AddNeighbour(
                    RRoot_LRoot_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.FinishInternalMovesAsDefender
                        )
                    );
                LRoot_RNodeA_DefTurn.AddNeighbour(
                    RNodeA_LRoot_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.FinishInternalMovesAsDefender
                        )
                    );
            }
            // 3rd move: Attacker
            {
                RNodeA_LRoot_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Defender,
                        GameGraph.R_NodeA.AsNode(),
                        GameGraph.L_Root.AsNode()
                    )
                );
                RNodeB_LRoot_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Defender,
                        GameGraph.R_NodeB.AsNode(),
                        GameGraph.L_Root.AsNode()
                    )
                );
                RRoot_LRoot_Sym_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Defender,
                        GameGraph.R_Root.AsNode(),
                        GameGraph.L_Root.AsNode()
                    )
                );
                LNodeA_RNodeB_Sym_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Defender,
                        GameGraph.L_NodeA.AsNode(),
                        GameGraph.R_NodeB.AsNode()
                    )
                );
                RNodeA_LRoot_Sym_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Defender,
                        GameGraph.R_NodeA.AsNode(),
                        GameGraph.L_Root.AsNode()
                    )
                );

                RRoot_LRoot_AttTurn.AddNeighbour(
                    RNodeA_LRoot_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                RRoot_LRoot_AttTurn.AddNeighbour(
                    RNodeB_LRoot_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                RRoot_LRoot_AttTurn.AddNeighbour(
                    RRoot_LRoot_Sym_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
                LNodeA_RNodeB_AttTurn.AddNeighbour(
                    LNodeA_RNodeB_Sym_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
                RNodeA_LRoot_AttTurn.AddNeighbour(
                    RNodeA_LRoot_Sym_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
            }
            // 4th move: Defender
            {
                LRoot_RNodeA_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Attacker,
                        GameGraph.L_Root.AsNode(),
                        GameGraph.R_NodeA.AsNode()
                    )
                );
                RNodeB_LNodeA_DefTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Defender,
                        GameGraph.R_NodeB.AsNode(),
                        GameGraph.L_NodeA.AsNode()
                    )
                );
                RNodeB_LNodeA_AttTurn = new FNativeGraphNode<FArenaNodeValue, FArenaEdgeValue>(
                    new FArenaNodeValue(
                        EPlayerType.Attacker,
                        GameGraph.R_NodeB.AsNode(),
                        GameGraph.L_NodeA.AsNode()
                    )
                );

                RRoot_LRoot_Sym_DefTurn.AddNeighbour(
                    LRoot_RRoot_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.FinishInternalMovesAsDefender
                        )
                    );
                LNodeA_RNodeB_Sym_DefTurn.AddNeighbour(
                    LNodeA_RNodeB_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.FinishInternalMovesAsDefender
                        )
                    );
                RNodeA_LRoot_Sym_DefTurn.AddNeighbour(
                    LRoot_RNodeA_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.FinishInternalMovesAsDefender
                        )
                    );
                RNodeB_LRoot_DefTurn.AddNeighbour(
                    RNodeB_LNodeA_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                RNodeB_LNodeA_DefTurn.AddNeighbour(
                    RNodeB_LNodeA_AttTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.FinishInternalMovesAsDefender
                        )
                    );
            }
            // 5th move: Attacker
            {
                LRoot_RNodeA_AttTurn.AddNeighbour(
                    LNodeA_RNodeA_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.MoveOverEdge
                        )
                    );
                LRoot_RNodeA_AttTurn.AddNeighbour(
                    LRoot_RNodeA_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
                RNodeB_LNodeA_AttTurn.AddNeighbour(
                    RNodeB_LNodeA_DefTurn,
                    new FArenaEdgeValue(
                        Optional.FromNull<IAiArenaMove>(),
                        EMoveType.StartSymmetrySwapAsAttacker
                        )
                    );
            }
        }
    }
}

