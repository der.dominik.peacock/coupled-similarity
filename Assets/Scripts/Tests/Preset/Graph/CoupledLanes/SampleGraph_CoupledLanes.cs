
using UnityEngine;

using CoupledSimilarity.Game.Graph;

namespace Tests
{
    public class SampleGraph_CoupledLanes : ITestGraph
    {
        public IGraphNode<Transform, EGameEdgeWeight> AttackerStartNode => L_Root.AsNode();
        public IGraphNode<Transform, EGameEdgeWeight> DefenderStartNode => R_Root.AsNode();

        // Left nodes
        public GameObject L_Root                            = new GameObject("L_Root", typeof(CircleCollider2D), typeof(MonoNode));
        public GameObject L_NodeA                           = new GameObject("L_NodeA", typeof(CircleCollider2D), typeof(MonoNode));
        // Left edges
        public GameObject L_E_Root_NodeA                    = new GameObject("L_E_Root_NodeA", typeof(MonoEdge));

        // Right edges
        public GameObject R_Root                            = new GameObject("R_Root", typeof(CircleCollider2D), typeof(MonoNode));
        public GameObject R_NodeA                           = new GameObject("R_NodeA", typeof(CircleCollider2D), typeof(MonoNode));
        public GameObject R_NodeB                           = new GameObject("R_NodeB", typeof(CircleCollider2D), typeof(MonoNode));
        // Right edges
        public GameObject R_E_Root_NodeA                    = new GameObject("R_E_Root_NodeA", typeof(MonoEdge));
        public GameObject R_E_Root_NodeB                    = new GameObject("R_E_Root_NodeB", typeof(MonoEdge));

        public SampleGraph_CoupledLanes()
        {
            L_Root.AddEdge(L_NodeA, L_E_Root_NodeA);

            R_Root.AddEdgeWithWeight(R_NodeA, R_E_Root_NodeA, EGameEdgeWeight.InternalMove);
            R_Root.AddEdge(R_NodeB, R_E_Root_NodeB);
        }
    }
}

