﻿
using NUnit.Framework;
using UniRx;
using UnityEngine;

using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Graph;

namespace Tests
{
    [TestFixture]
    public class SimulationNoMovesLeftTests : SamePlayerGraphTest<PlayerLogicMock, SampleGraph_WeakBisim>
    {
        bool bDidAnyPlayerWin;
        EPlayerType PlayerThatWon;

        [SetUp]
        public void SetUp()
        {
            SetUp(
                () => new PlayerLogicMock()
                );
            bDidAnyPlayerWin = false;
            GameState.OnEndGame.Subscribe(
                stats =>
                {
                    bDidAnyPlayerWin = true;
                    PlayerThatWon = stats.Winner;
                }
            );

            GameMode.PrepareNewMatch();
        }

        [Test]
        [TestCase(EGameType.Simulation)]
        [TestCase(EGameType.Bisimulation)]
        [TestCase(EGameType.WeakBisimulation)]
        [TestCase(EGameType.CoupledSimulation)]
        public void DefenderHasNoMovesLeft_DefenderLoses(EGameType gameType)
        {
            GameMode.SelectSimulationGameType(gameType);

            Attacker.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(
                Graph.L_E_Root_NodeC_Internal.AsEdge()
                );

            Assert.IsTrue(bDidAnyPlayerWin, "Defender has no moves left.");
            Assert.AreEqual(EPlayerType.Attacker, PlayerThatWon, "Attacker should win.");
        }

        // This used to be a bug
        [Test]
        [TestCase(EGameType.WeakBisimulation)]
        [TestCase(EGameType.CoupledSimulation)]
        public void DefenderFinishesMovesAndHasMovesLeft_DefenderDoesNotLose(EGameType gameType)
        {
            GameMode.SelectSimulationGameType(gameType);

            // First turn
            Attacker.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(
                Graph.L_E_Root_NodeA.AsEdge()
                );
            Defender.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(
                Graph.R_E_Root_NodeA.AsEdge()
                );
            Defender.ConcretePlayerLogic.PlayerMoveRules.FinishInternalMovesAsDefender();
            // Second turn
            Attacker.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(
                Graph.L_E_NodeA_NodeB_Internal.AsEdge()
                );
            Defender.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(
                 Graph.R_E_NodeA_NodeB_Internal.AsEdge()
                 );
            Defender.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(
                Graph.R_E_NodeB_Root_Internal.AsEdge()
                );
            Defender.ConcretePlayerLogic.PlayerMoveRules.FinishInternalMovesAsDefender();

            Assert.IsFalse(bDidAnyPlayerWin, "Did any player win?");
        }

        // This used to be a bug
        [Test]

        [TestCase(EGameType.Bisimulation)]
        [TestCase(EGameType.WeakBisimulation)]
        public void DefenderMovesAfterSymmetrySwap_HasMovesLeft_DefenderDoesNotLose(EGameType gameType)
        {
            GameMode.SelectSimulationGameType(gameType);

            // First turn
            Attacker.ConcretePlayerLogic.PlayerMoveRules.StartSymmetrySwapAsAttacker();
            Attacker.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(
                Graph.R_E_Root_NodeA.AsEdge()
                );
            Defender.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(
               Graph.L_E_Root_NodeA.AsEdge()
               );

            Assert.IsFalse(bDidAnyPlayerWin, "Did any player win?");
        }

    }
}
