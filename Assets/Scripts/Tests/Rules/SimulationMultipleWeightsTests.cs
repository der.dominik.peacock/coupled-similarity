﻿
using NUnit.Framework;
using UniRx;
using UnityEngine;

using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Graph;

namespace Tests
{
    [TestFixture]
    public class SimulationMultipleWeightsRulesTests : SamePlayerGraphTest<PlayerLogicMock, SampleGraph_WeakBisim>
    {
        bool bIsDefenderTurn;

        bool bReceivedInvalidMove;
        EMoveInvalidityReason LastInvalidityReason;
        EMoveType LastMoveType;

        [SetUp]
        public void SetUp()
        {
            SetUp(
                () => new PlayerLogicMock()
                );

            bIsDefenderTurn = false;
            bReceivedInvalidMove = false;

            Defender.ConcretePlayerLogic.StartTurnEvent += 
                () => bIsDefenderTurn = true;
            Defender.ConcretePlayerLogic.EndTurnEvent += 
                () => bIsDefenderTurn = false;

            GameMode.PrepareNewMatch();
        }

        protected override IRuleFactory CreateRuleFactory()
        {
            return new InvalidMoveRuleFactory(
                (reason, move) =>
                {
                    bReceivedInvalidMove = true;
                    LastInvalidityReason = reason;
                    LastMoveType = move;
                }
                );
        }
        protected override SampleGraph_WeakBisim CreateGraph()
        {
            return new SampleGraph_WeakBisim(true);
        }

        [Test]
        [TestCase(EGameType.Simulation)]
        [TestCase(EGameType.Bisimulation)]
        [TestCase(EGameType.WeakBisimulation)]
        [TestCase(EGameType.CoupledSimulation)]
        public void AttackerCanOnlyMakeOneMove(EGameType gameType)
        {
            GameMode.SelectSimulationGameType(gameType);

            Attacker.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(
                Graph.L_E_Root_NodeC_Internal.AsEdge()
                );

            Assert.IsTrue(bIsDefenderTurn, "Attacker made move. It should now be the defender's turn.");
        }

        [Test]
        [TestCase(EGameType.Simulation)]
        [TestCase(EGameType.Bisimulation)]
        [TestCase(EGameType.WeakBisimulation)]
        [TestCase(EGameType.CoupledSimulation)]
        public void AttackerMovesOverEdge_DefenderMustUseSameEdgeWeight(EGameType gameType)
        {
            GameMode.SelectSimulationGameType(gameType);

            Attacker.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(
                Graph.L_E_Root_NodeA.AsEdge()
                );

            Defender.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(
                Graph.R_E_Root_NodeA_EmptyArrowWeight.AsEdge()
                );

            Assert.IsTrue(bReceivedInvalidMove, "Defender tried invalid move but rules did not report an error.");
            Assert.AreEqual(EMoveType.MoveOverEdge, LastMoveType, "Error move was not " + EMoveType.MoveOverEdge);
            bReceivedInvalidMove = false;

            Defender.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(
                Graph.R_E_Root_NodeA.AsEdge()
                );
            Assert.IsFalse(bReceivedInvalidMove, "Defender made valid move but rules reported an error.");
        }

    }
}
