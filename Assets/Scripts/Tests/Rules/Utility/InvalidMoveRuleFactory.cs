﻿
using System;

using CoupledSimilarity.Game.Rules;

namespace Tests
{
    public class InvalidMoveRuleFactory : RuleFactory
    {
        private class CallbackInvalidMoveHandler : IInvalidMoveHandler
        {
            private Action<EMoveInvalidityReason, EMoveType> InvalidMoveCallback;

            public CallbackInvalidMoveHandler(Action<EMoveInvalidityReason, EMoveType> invalidMoveCallback)
            {
                InvalidMoveCallback = invalidMoveCallback;
            }

            public void HandleInvalidMove(EMoveInvalidityReason invalidityReason, EMoveType triedMoveType)
            {
                InvalidMoveCallback(invalidityReason, triedMoveType);
            }
        }

        public InvalidMoveRuleFactory(Action<EMoveInvalidityReason, EMoveType> invalidMoveCallback)
            : base(new CallbackInvalidMoveHandler(invalidMoveCallback))
        {}
    }
}
