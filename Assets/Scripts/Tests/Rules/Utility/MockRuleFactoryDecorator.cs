﻿
using System;
using NUnit.Framework;

using CoupledSimilarity.Game.Rules;

namespace Tests
{
    public class MockRuleFactoryDecorator : IRuleFactory
    {
        readonly IRuleFactory RealImplementation;
        readonly Action<EGameType, IGameMode> ValidateCreateRulesInput;
        readonly Action<IRules, IPlayerEntity, IGameState> ValidateHumanVerificationRulesInput;
        readonly Action<IRules, IPlayerEntity, IGameState> ValidateBotVerificationRulesInput;

        public MockRuleFactoryDecorator(
            IRuleFactory realImplementation,
            Action<EGameType, IGameMode> validateCreateRulesInput,
            Action<IRules, IPlayerEntity, IGameState> validateHumanVerificationRulesInput,
            Action<IRules, IPlayerEntity, IGameState> validateBotVerificationRulesInput
            )
        {
            RealImplementation = realImplementation;
            ValidateCreateRulesInput = validateCreateRulesInput;
            ValidateHumanVerificationRulesInput = validateHumanVerificationRulesInput;
            ValidateBotVerificationRulesInput = validateBotVerificationRulesInput;
        }

        public IRules CreateGameRules(EGameType gameType, IGameMode gameMode, FGameState gameState)
        {
            ValidateCreateRulesInput(gameType, gameMode);
            return RealImplementation.CreateGameRules(gameType, gameMode, gameState);
        }

        public IRules DecorateHumanVerificationRules(IRules gameRules, IPlayerEntity humanPlayer, FGameState gameState)
        {
            ValidateHumanVerificationRulesInput(gameRules, humanPlayer, gameState);
            return RealImplementation.DecorateHumanVerificationRules(gameRules, humanPlayer, gameState);
        }

        public IRules DecorateBotVerificationRules(IRules gameRules, IPlayerEntity humanPlayer, FGameState gameState)
        {
            ValidateBotVerificationRulesInput(gameRules, humanPlayer, gameState);
            return RealImplementation.DecorateBotVerificationRules(gameRules, humanPlayer, gameState);
        }
    }
}
