﻿
using NUnit.Framework;
using UniRx;

using CoupledSimilarity.Game.Rules;

namespace Tests
{
    [TestFixture]
    public class CoupledSimulationRulesTests : SamePlayerGraphTest<PlayerLogicMock, SampleGraph_WeakBisim>
    {
        bool bIsAttackerTurn;
        bool bIsDefenderTurn;

        bool bMadeInvalidMove;
        EMoveInvalidityReason InvalidityReason;

        bool bReceivedMove;
        EMoveType ReceivedMoveType;

        [SetUp]
        public void SetUp()
        {
            SetUp(
                () => new PlayerLogicMock()
                );
            bIsAttackerTurn = false;
            bIsDefenderTurn = false;
            bMadeInvalidMove = false;
            bReceivedMove = false;

            Attacker.ConcretePlayerLogic.StartTurnEvent +=
                () => bIsAttackerTurn = true;
            Attacker.ConcretePlayerLogic.EndTurnEvent +=
                () => bIsAttackerTurn = false;
            Defender.ConcretePlayerLogic.StartTurnEvent +=
                () => bIsDefenderTurn = true;
            Defender.ConcretePlayerLogic.EndTurnEvent +=
                () => bIsDefenderTurn = false;

            GameState.OnGameMove.Subscribe(
                move =>
                {
                    bReceivedMove = true;
                    ReceivedMoveType = move.PerformedMove;
                }
                );

            GameMode.PrepareNewMatch();
            GameMode.SelectSimulationGameType(EGameType.CoupledSimulation);
        }

        protected override IRuleFactory CreateRuleFactory()
        {
            return new InvalidMoveRuleFactory(
                (reason, type) => 
                {
                    bMadeInvalidMove = true;
                    InvalidityReason = reason;
                }
                );
        }

        [Test]
        public void FinishInternalMoveBroadcastsMoveEvent()
        {
            Attacker.ConcretePlayerLogic.PlayerMoveRules.StartSymmetrySwapAsAttacker();
            Defender.ConcretePlayerLogic.PlayerMoveRules.FinishInternalMovesAsDefender();

            Assert.IsTrue(bReceivedMove, "Rules did not broadcast OnGameMove in game state.");
            Assert.AreEqual(EMoveType.FinishInternalMovesAsDefender, ReceivedMoveType, "Rules broadcast wrong move type.");
        }

        [Test]
        public void SymmetrySwap_IsDefenderTurn()
        {
            Attacker.ConcretePlayerLogic.PlayerMoveRules.StartSymmetrySwapAsAttacker();

            Assert.IsTrue(bIsDefenderTurn, "After symmetry swap, the defender is allowed to move");
        }

        [Test]
        public void SymmetrySwap_DefenderMakesMainMove_IsInvalid()
        {
            SymmetrySwap_IsDefenderTurn();
            Defender.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(Graph.R_E_Root_NodeA.AsEdge());

            Assert.IsTrue(bMadeInvalidMove, "The defender illegally responded to the symmetry swap by making a main move.");
            Assert.AreEqual(EMoveInvalidityReason.DefenderMakesMainMoveAfterSymmetrySwap, InvalidityReason, "Received wrong reason for moving being invalid.");
        }

        [Test]
        public void SymmetrySwap_DefenderRespondsWithInternalMoves_IsAttackerTurn()
        {
            // 1st move
            Attacker.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(Graph.L_E_Root_NodeA.AsEdge());
            // 2nd move
            Defender.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(Graph.R_E_Root_NodeA.AsEdge());
            Defender.ConcretePlayerLogic.PlayerMoveRules.FinishInternalMovesAsDefender();
            // 3rd move
            Attacker.ConcretePlayerLogic.PlayerMoveRules.StartSymmetrySwapAsAttacker();
            // 4th move
            Defender.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(Graph.R_E_NodeA_NodeB_Internal.AsEdge());
            Defender.ConcretePlayerLogic.PlayerMoveRules.FinishInternalMovesAsDefender();

            Assert.IsTrue(bIsAttackerTurn, "The defender responded to the symmetry swap. It should be the attacker's turn.");
        }

        [Test]
        public void SymmetrySwap_DefenderMakesNoInternalMoves_IsAttackerTurn()
        {
            // 1st move
            Attacker.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(Graph.L_E_Root_NodeA.AsEdge());
            // 2nd move
            Defender.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(Graph.R_E_Root_NodeA.AsEdge());
            Defender.ConcretePlayerLogic.PlayerMoveRules.FinishInternalMovesAsDefender();
            // 3rd move
            Attacker.ConcretePlayerLogic.PlayerMoveRules.StartSymmetrySwapAsAttacker();
            // 4th move
            Defender.ConcretePlayerLogic.PlayerMoveRules.FinishInternalMovesAsDefender();

            Assert.IsTrue(bIsAttackerTurn, "The defender responded to the symmetry swap. It should be the attacker's turn.");
        }

    }
}
