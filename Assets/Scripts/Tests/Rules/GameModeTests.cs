﻿
using System;
using NUnit.Framework;

using CoupledSimilarity.Game.Rules;

namespace Tests
{
    [TestFixture]
    public class GameModeTests : SamePlayerGraphTest<PlayerLogicMock, SampleGraph_JobApplication>
    {
        bool bIsAttackerTurn;
        bool bIsDefenderTurn;

        [SetUp]
        public void SetUp()
        {
            SetUp(
                () => new PlayerLogicMock()
                );

            bIsAttackerTurn = false;
            bIsDefenderTurn = false;

            Attacker.ConcretePlayerLogic.StartTurnEvent += 
                () => bIsAttackerTurn = true;
            Attacker.ConcretePlayerLogic.EndTurnEvent += 
                () => bIsAttackerTurn = false;
            Defender.ConcretePlayerLogic.StartTurnEvent += 
                () => bIsDefenderTurn = true;
            Defender.ConcretePlayerLogic.EndTurnEvent += 
                () => bIsDefenderTurn = false;
        }

        protected override IRuleFactory CreateRuleFactory()
        {
            return new MockRuleFactoryDecorator(
                base.CreateRuleFactory(),
                (gameType, gameMode) => { },
                (rules, playerEntity, gameMode) => Assert.AreEqual(playerEntity, Attacker, "Human verification rules must be used on attacker."),
                (rules, playerEntity, gameMode) => Assert.AreEqual(playerEntity, Defender, "Bot verification rules must be used on defender.")
                );
        }

        [Test]
        public void StartGame_AllPlayersReceiveOnStartNewGameEvent()
        {
            bool bAttackerReceivedStart = false;
            Attacker.ConcretePlayerLogic.OnStartNewGameEvent +=
                (state, rules) => bAttackerReceivedStart = true;
            bool bDefenderReceivedStart = false;
            Defender.ConcretePlayerLogic.OnStartNewGameEvent +=
                (state, rules) => bDefenderReceivedStart = true;

            GameMode.PrepareNewMatch();
            GameMode.SelectSimulationGameType(EGameType.Simulation);

            Assert.IsTrue(bAttackerReceivedStart, "Attacker did not receive game start event.");
            Assert.IsTrue(bDefenderReceivedStart, "Defender did not receive game start event.");
        }

        [Test]
        public void StartGame_FirstTurnIsForAttacker()
        {
            StartGame_AllPlayersReceiveOnStartNewGameEvent();

            Assert.IsTrue(bIsAttackerTurn, "It should be the attacker's turn.");
            Assert.IsFalse(bIsDefenderTurn, "It shouldn't be the defender's turn.");
        }

        [Test]
        public void AttackerFinishTurn_IsDefenderTurn()
        {
            StartGame_FirstTurnIsForAttacker();

            Attacker.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(
                Graph.L_E_Root_NodeB.AsEdge()
                );

            Assert.IsFalse(bIsAttackerTurn, "It shouldn't be the attacker's turn.");
            Assert.IsTrue(bIsDefenderTurn, "It should be the defender's turn.");
        }

        [Test]
        public void DefenderFinishTurn_IsAttackerTurn()
        {
            AttackerFinishTurn_IsDefenderTurn();

            Defender.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(
                Graph.R_E_Root_SubRoot.AsEdge()
                );

            Assert.IsTrue(bIsAttackerTurn, "It should be the attacker's turn.");
            Assert.IsFalse(bIsDefenderTurn, "It shouldn't be the defender's turn.");
        }

    }
}
