﻿
using NUnit.Framework;
using UniRx;
using UnityEngine;

using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Graph;

namespace Tests
{
    [TestFixture]
    public class WeakBisimulationRulesTests : SamePlayerGraphTest<PlayerLogicMock, SampleGraph_WeakBisim>
    {
        bool bIsAttackerTurn;
        bool bIsDefenderTurn;
        bool bDidAnyPlayerWin;
        EPlayerType PlayerThatWon;

        bool bReceivedInvalidMove;
        EMoveInvalidityReason LastInvalidityReason;
        EMoveType LastMoveType;

        [SetUp]
        public void SetUp()
        {
            SetUp(
                () => new PlayerLogicMock()
                );
            bIsAttackerTurn = false;
            bIsDefenderTurn = false;
            bDidAnyPlayerWin = false;
            bReceivedInvalidMove = false;

            Attacker.ConcretePlayerLogic.StartTurnEvent +=
                () => bIsAttackerTurn = true;
            Attacker.ConcretePlayerLogic.EndTurnEvent +=
                () => bIsAttackerTurn = false;
            Defender.ConcretePlayerLogic.StartTurnEvent +=
                () => bIsDefenderTurn = true;
            Defender.ConcretePlayerLogic.EndTurnEvent +=
                () => bIsDefenderTurn = false;

            GameState.OnEndGame.Subscribe(
                stats =>
                {
                    bDidAnyPlayerWin = true;
                    PlayerThatWon = stats.Winner;
                }
            );

            GameMode.PrepareNewMatch();
        }

        protected override IRuleFactory CreateRuleFactory()
        {
            return new InvalidMoveRuleFactory(
                (reason, move) =>
                {
                    bReceivedInvalidMove = true;
                    LastInvalidityReason = reason;
                    LastMoveType = move;
                }
                );
        }

        protected override SampleGraph_WeakBisim CreateGraph()
        {
            return new SampleGraph_WeakBisim(true);
        }

        [Test]
        [TestCase(EGameType.WeakBisimulation)]
        [TestCase(EGameType.CoupledSimulation)]
        public void AttackerMakesInternalMove_DefenderMakesSeveralInternalMoves_IsAttackerTurn(EGameType gameTypeToPlay)
        {
            GameMode.SelectSimulationGameType(gameTypeToPlay);


            // Attacker' turn
            Attacker.ConcretePlayerLogic.PlayerMoveRules
                .MoveOverEdge(Graph.L_E_Root_NodeA.AsEdge());
            Assert.IsFalse(bReceivedInvalidMove);
            Assert.IsTrue(bIsDefenderTurn, "Attacker finished with main move. It should be the defender's turn.");
            // Defender's turn
            Defender.ConcretePlayerLogic.PlayerMoveRules
                .MoveOverEdge(Graph.R_E_Root_NodeA.AsEdge());
            Assert.IsFalse(bReceivedInvalidMove);
            Defender.ConcretePlayerLogic.PlayerMoveRules
                .FinishInternalMovesAsDefender();
            Assert.IsFalse(bReceivedInvalidMove);
            Assert.IsTrue(bIsAttackerTurn, "Defender finished move. It should be the defender's turn.");
            // Attacker' turn
            Attacker.ConcretePlayerLogic.PlayerMoveRules
                .MoveOverEdge(Graph.L_E_NodeA_NodeB_Internal.AsEdge());
            Assert.IsFalse(bReceivedInvalidMove);
            Assert.IsTrue(bIsDefenderTurn, "Attacker finished with main move. It should be the defender's turn.");
            // Defender's turn
            Defender.ConcretePlayerLogic.PlayerMoveRules
                .MoveOverEdge(Graph.R_E_NodeA_NodeB_Internal.AsEdge());
            Assert.IsFalse(bReceivedInvalidMove);
            Defender.ConcretePlayerLogic.PlayerMoveRules
                .MoveOverEdge(Graph.R_E_NodeB_Root_Internal.AsEdge());
            Assert.IsFalse(bReceivedInvalidMove);
            Defender.ConcretePlayerLogic.PlayerMoveRules
                .MoveOverEdge(Graph.R_E_Root_NodeA.AsEdge());
            Assert.IsTrue(bReceivedInvalidMove, "Defender tried to move over non-internal edge although attacker moved over internal edge. This should be invalid.");
            bReceivedInvalidMove = false;
            Defender.ConcretePlayerLogic.PlayerMoveRules
                .MoveOverEdge(Graph.R_E_Root_NodeA_Internal.AsEdge());
            Assert.IsFalse(bReceivedInvalidMove);
            Defender.ConcretePlayerLogic.PlayerMoveRules.FinishInternalMovesAsDefender();
            Assert.IsFalse(bReceivedInvalidMove);

            Assert.IsFalse(bDidAnyPlayerWin, "Both players still have moves left but {0} was declared winner", PlayerThatWon);
            Assert.IsTrue(bIsAttackerTurn, "Defender finished move. It should be the defender's turn.");
        }

    }
}
