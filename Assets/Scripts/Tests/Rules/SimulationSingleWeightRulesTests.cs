﻿
using NUnit.Framework;
using UniRx;
using UnityEngine;

using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Graph;

namespace Tests
{
    [TestFixture]
    public class SimulationSingleWeightRulesTests : SamePlayerGraphTest<PlayerLogicMock, SampleGraph_JobApplication>
    {
        bool bIsAttackerTurn;
        bool bIsDefenderTurn;

        bool bReceivedInvalidMove;
        EMoveInvalidityReason LastInvalidityReason;
        EMoveType LastMoveType;

        bool bDidAnyPlayerWin;
        EPlayerType PlayerThatWon;

        bool bReceivedMove;
        EMoveType ReceivedMoveType;

        [SetUp]
        public void SetUp()
        {
            SetUp(
                () => new PlayerLogicMock()
                );

            bIsAttackerTurn = false;
            bIsDefenderTurn = false;
            bReceivedInvalidMove = false;
            bDidAnyPlayerWin = false;
            bReceivedMove = false;

            Attacker.ConcretePlayerLogic.StartTurnEvent += 
                () => bIsAttackerTurn = true;
            Attacker.ConcretePlayerLogic.EndTurnEvent += 
                () => bIsAttackerTurn = false;
            Defender.ConcretePlayerLogic.StartTurnEvent += 
                () => bIsDefenderTurn = true;
            Defender.ConcretePlayerLogic.EndTurnEvent += 
                () => bIsDefenderTurn = false;

            GameState.OnGameMove.Subscribe(
                move =>
                {
                    bReceivedMove = true;
                    ReceivedMoveType = move.PerformedMove;
                }
                );
            GameState.OnEndGame.Subscribe(
                stats =>
                {
                    bDidAnyPlayerWin = true;
                    PlayerThatWon = stats.Winner;
                }
            );

            GameMode.PrepareNewMatch();
        }

        protected override IRuleFactory CreateRuleFactory()
        {
            return new InvalidMoveRuleFactory(
                (reason, move) =>
                {
                    bReceivedInvalidMove = true;
                    LastInvalidityReason = reason;
                    LastMoveType = move;
                }
                );
        }

        [Test]
        [TestCase(EGameType.Simulation)]
        [TestCase(EGameType.Bisimulation)]
        [TestCase(EGameType.WeakBisimulation)]
        [TestCase(EGameType.CoupledSimulation)]
        public void AttackerMakesValidFirstMove_IsAllowed(EGameType gameType)
        {
            GameMode.SelectSimulationGameType(gameType);
            Assert.IsTrue(bIsAttackerTurn, "The game didn't start with the attacker's turn.");

            Attacker.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(
                Graph.L_E_Root_NodeB.AsEdge()
                );

            Assert.IsFalse(bReceivedInvalidMove, "Attacker made valid move but rules reported an error.");
        }

        [Test]
        [TestCase(EGameType.Simulation)]
        [TestCase(EGameType.Bisimulation)]
        [TestCase(EGameType.WeakBisimulation)]
        [TestCase(EGameType.CoupledSimulation)]
        public void MoveOverEdgeBroadcastsMoveEvent(EGameType gameType)
        {
            AttackerMakesValidFirstMove_IsAllowed(gameType);

            Assert.IsTrue(bReceivedMove, "Rules did not broadcast OnGameMove in game state.");
            Assert.AreEqual(EMoveType.MoveOverEdge, ReceivedMoveType, "Rules broadcast wrong move type.");
        }

        [Test]
        [TestCase(EGameType.Simulation)]
        [TestCase(EGameType.Bisimulation)]
        [TestCase(EGameType.WeakBisimulation)]
        [TestCase(EGameType.CoupledSimulation)]
        public void AttackerMakeMove_IsDefenderTurn(EGameType gameType)
        {
            AttackerMakesValidFirstMove_IsAllowed(gameType);
            Assert.IsTrue(bIsDefenderTurn, "The game didn't continue with the defender's turn.");
        }

        [Test]
        [TestCase(EGameType.Simulation)]
        [TestCase(EGameType.Bisimulation)]
        [TestCase(EGameType.WeakBisimulation)]
        [TestCase(EGameType.CoupledSimulation)]
        public void CanOnlyMoveToNeighbours(EGameType gameType)
        {
            AttackerMakeMove_IsDefenderTurn(gameType);

            MakeInvalidMove(Graph.R_E_SubRoot_NodeB.AsEdge());
            MakeInvalidMove(Graph.R_E_SubRoot_NodeC.AsEdge());
            Defender.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(Graph.R_E_Root_SubRoot.AsEdge());

            Assert.IsFalse(bReceivedInvalidMove, "Defender made valid move but rules reported an error.");
        }
        private void MakeInvalidMove(IGraphEdge<Transform, EGameEdgeWeight> edgeToMoveOver)
        {
            Defender.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(
                edgeToMoveOver
                );

            Assert.IsTrue(bReceivedInvalidMove, "Defender tried invalid move but rules did not report an error.");
            Assert.AreEqual(EMoveType.MoveOverEdge, LastMoveType, "Error move was not " + EMoveType.MoveOverEdge);

            bReceivedInvalidMove = false;
        }

        [Test]
        [TestCase(EGameType.Simulation)]
        [TestCase(EGameType.Bisimulation)]
        public void MakeInvalidMove_InvalidityReasonIsCorrect(EGameType gameType)
        {
            AttackerMakesValidFirstMove_IsAllowed(gameType);
            MakeInvalidMove(Graph.R_E_SubRoot_NodeB.AsEdge());

            Assert.AreEqual(EMoveInvalidityReason.MoveToUnneighbouredNode, LastInvalidityReason, "Error message was not " + EMoveInvalidityReason.MoveToUnneighbouredNode);
        }

        [Test]
        [TestCase(EGameType.Simulation)]
        [TestCase(EGameType.Bisimulation)]
        public void DefenderMakeMove_IsAttackerTurn(EGameType gameType)
        {
            AttackerMakeMove_IsDefenderTurn(gameType);
            Defender.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(Graph.R_E_Root_SubRoot.AsEdge());

            Assert.IsTrue(bIsAttackerTurn, "The game didn't continue with the attacker's turn.");
        }

        [Test]
        public void AfterMoveAttackerHasNoMovesButDefenderDoes_AttackerLoses()
        {
            GameMode.SelectSimulationGameType(EGameType.Simulation);

            Attacker.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(
                Graph.L_E_Root_NodeA.AsEdge()
                );

            Assert.IsTrue(bDidAnyPlayerWin, "Attacker has no moves left. Rules did not report any winner.");
            Assert.AreEqual(EPlayerType.Defender, PlayerThatWon, "Attacker has no moves left. Defender should be declared winner.");
        }

    }
}
