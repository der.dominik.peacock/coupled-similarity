﻿
using NUnit.Framework;
using UniRx;
using UnityEngine;

using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Graph;

namespace Tests
{
    public class BisimulationRulesTests : SamePlayerGraphTest<PlayerLogicMock, SampleGraph_JobApplication>
    {
        bool bDidAnyPlayerWin;
        EPlayerType PlayerThatWon;

        bool bReceivedMove;
        EMoveType ReceivedMoveType;

        [SetUp]
        public void SetUp()
        {
            SetUp(
                () => new PlayerLogicMock() 
                );
            bDidAnyPlayerWin = false;
            bReceivedMove = false;

            GameState.OnGameMove.Subscribe(
                move =>
                {
                    bReceivedMove = true;
                    ReceivedMoveType = move.PerformedMove;
                }
                );
            GameState.OnEndGame.Subscribe(
                stats =>
                {
                    bDidAnyPlayerWin = true;
                    PlayerThatWon = stats.Winner;
                }
            );

            GameMode.PrepareNewMatch();
        }

        protected override IRuleFactory CreateRuleFactory()
        {
            return new InvalidMoveRuleFactory(
                (reason, move) =>
                {
                    Assert.Fail(string.Format("Move {0} was reported invalid because {1} but it was actually valid.", move, reason));
                }
                );
        }

        [Test]
        [TestCase(EGameType.Bisimulation)]
        [TestCase(EGameType.WeakBisimulation)]
        [TestCase(EGameType.CoupledSimulation)]
        public void StartSymmetrySwapBroadcastsMoveEvent(EGameType gameType)
        {
            GameMode.SelectSimulationGameType(gameType);
            Attacker.ConcretePlayerLogic.PlayerMoveRules.StartSymmetrySwapAsAttacker();

            Assert.IsTrue(bReceivedMove, "Rules did not broadcast OnGameMove in game state.");
            Assert.AreEqual(EMoveType.StartSymmetrySwapAsAttacker, ReceivedMoveType, "Rules broadcast wrong move type.");
        }

        [Test]
        [TestCase(EGameType.Bisimulation)]
        [TestCase(EGameType.WeakBisimulation)]
        public void AttackerCanSymmetrySwapAfterMove_AttackerDoesntLose(EGameType gameType)
        {
            GameMode.SelectSimulationGameType(gameType);

            Attacker.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(
                Graph.L_E_Root_NodeA.AsEdge()
                );

            Assert.IsFalse(bDidAnyPlayerWin, "Attacker has no moves left. Rules did not report any winner.");
        }

        [Test]
        [TestCase(EGameType.Bisimulation)]
        public void AttackerCannotMoveAfterDefenderMove_DefenderWins(EGameType gameType)
        {
            GameMode.SelectSimulationGameType(gameType);

            Attacker.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(Graph.L_E_Root_NodeB.AsEdge());
            Defender.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(Graph.R_E_Root_SubRoot.AsEdge());
            Attacker.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(Graph.L_E_NodeB_ChildOfB.AsEdge());
            Defender.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(Graph.R_E_SubRoot_NodeB.AsEdge());

            Assert.IsTrue(bDidAnyPlayerWin && PlayerThatWon == EPlayerType.Defender, "Defender made final move and attacker cannot move: Defender should win.");
        }

    }
}
