
using System;
using UnityEngine;

using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;

namespace Tests
{
    public class MovementLogicMock : IMovementLogic
    {
        public IObservable<IGraphNode<Transform, EGameEdgeWeight>> OnReachTargetNode => throw new NotImplementedException();
        public IGraphNode<Transform, EGameEdgeWeight> CurrentNode { get; private set; }
        public Transform EntityTransform => throw new NotImplementedException("Test mock does not implement EntityTransform.");

        public void MoveTo(IGraphNode<Transform, EGameEdgeWeight> nodeToMoveTo)
        {
            CurrentNode = nodeToMoveTo;
        }

        public void TeleportTo(IGraphNode<Transform, EGameEdgeWeight> nodeToTeleportTo)
        {
            CurrentNode = nodeToTeleportTo;
        }
    }
}

