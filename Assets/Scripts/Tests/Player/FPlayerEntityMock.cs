
using CoupledSimilarity.Game.Rules;

namespace Tests
{
    public class FPlayerEntityMock<TPlayerLogic, TMovementLogic> : IPlayerEntity
        where TPlayerLogic : IPlayerLogic
        where TMovementLogic : IMovementLogic
    {
        public TPlayerLogic ConcretePlayerLogic { get; }
        public IPlayerLogic PlayerLogic => ConcretePlayerLogic;

        public TMovementLogic ConcreteMovementLogic { get; }
        public IMovementLogic MovementLogic => ConcreteMovementLogic;

        public FPlayerEntityMock(TPlayerLogic playerLogic, TMovementLogic movementLogic)
        {
            ConcretePlayerLogic = playerLogic;
            ConcreteMovementLogic = movementLogic;
        }
    }
}

