﻿
using NUnit.Framework;

using CoupledSimilarity.Game.Player.AI;
using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;

namespace Tests
{
    [TestFixture]
    public class WinningRegionTests
    {
        private static void AreGraphsEqual(
            IGraphNode<FArenaNodeValue, FArenaEdgeValue> expectedRoot,
            IGraphNode<FArenaNodeValue, FArenaEdgeValue> actualRoot,
            EGameType gameTypeToTest)
        {
            GraphUtility.AssertAreEqualRecursive(
                expectedRoot,
                actualRoot,
                AreNodesEqualIncludingWinningRegion
            );
        }
        private static bool AreNodesEqualIncludingWinningRegion(
            IGraphNode<FArenaNodeValue, FArenaEdgeValue> expectedNode,
            IGraphNode<FArenaNodeValue, FArenaEdgeValue> actualNode)
        {
            bool bAreAttackerPositionsEqual = expectedNode.Value.AttackerPosition == actualNode.Value.AttackerPosition;
            bool bAreDefenderPositionsEqual = expectedNode.Value.DefenderPosition == actualNode.Value.DefenderPosition;
            bool bArePlayerTurnsEqual = expectedNode.Value.PlayerWhoseTurnItIs == actualNode.Value.PlayerWhoseTurnItIs;
            bool bAreWinningRegionsEqual = expectedNode.Value.WinningRegion == actualNode.Value.WinningRegion;
            return bAreAttackerPositionsEqual
                   && bAreDefenderPositionsEqual
                   && bArePlayerTurnsEqual
                   && bAreWinningRegionsEqual;
        }

        [Test]
        public void WinningRegion_Bisim_JobApplication_IsCorrect()
        {
            ArenaGraph_JobApplication_Bisim actualGraphNetwork = new ArenaGraph_JobApplication_Bisim();
            WinningRegionGraph_JobApplication_Bisim expectedGraph = new WinningRegionGraph_JobApplication_Bisim(actualGraphNetwork.GameGraph);

            WinningRegion.ComputeWinningRegions(actualGraphNetwork.LRoot_RRoot);

            AreGraphsEqual(expectedGraph.Root, actualGraphNetwork.LRoot_RRoot, EGameType.Bisimulation);
        }

    }
}
