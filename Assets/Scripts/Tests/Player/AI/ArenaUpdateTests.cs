﻿
using NUnit.Framework;

using CoupledSimilarity.Game.Player.AI;
using CoupledSimilarity.Game.Rules;

namespace Tests
{
    [TestFixture]
    public class ArenaUpdateTests : SamePlayerGraphTest<PlayerLogicMock, ArenaGraph_JobApplication_Sim>
    {
        FArenaGraph ArenaNetwork;

        [SetUp]
        public void SetUp()
        {
            SetUp(
                () => new PlayerLogicMock()
                );

            ArenaNetwork = new FArenaGraph(Graph.LRoot_RRoot, GameState);

            GameMode.PrepareNewMatch();
            GameMode.SelectSimulationGameType(EGameType.Simulation);
        }

        [Test]
        public void WhenAttackerMoves_ArenaTransitionsToCorrespondingNode()
        {
            Attacker.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(
                Graph.GameGraph.L_E_Root_NodeB.AsEdge()
                );

            Assert.AreEqual(Graph.LNodeB_RRoot_DefTurn, ArenaNetwork.CurrentGamePositionInArena, "The attacker's move did not update the position on the arena graph");
        }

        [Test]
        public void WhenDefenderMoves_ArenaTransitionsToCorrespondingNode()
        {
            WhenAttackerMoves_ArenaTransitionsToCorrespondingNode();

            Defender.ConcretePlayerLogic.PlayerMoveRules.MoveOverEdge(
                Graph.GameGraph.R_E_Root_SubRoot.AsEdge()
                );

            Assert.AreEqual(Graph.LNodeB_RSubRoot_AttTurn, ArenaNetwork.CurrentGamePositionInArena, "The defender's move did not update the position on the arena graph");
        }

    }
}
