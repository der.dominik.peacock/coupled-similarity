﻿
using NUnit.Framework;

using CoupledSimilarity.Game.Player.AI;
using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;

namespace Tests
{
    [TestFixture]
    public class ArenaGenerationTests
    {
        private static void AreGraphsEqual(
            IGraphNode<FArenaNodeValue, FArenaEdgeValue> correctGraphRoot,
            ITestGraph testGraph,
            EGameType gameTypeToTest)
        {
            GraphUtility.AreGraphsEqual(
                correctGraphRoot,
                testGraph,
                gameTypeToTest,
                AreNodesEqualExceptForWinningRegion
                );
        }
        private static bool AreNodesEqualExceptForWinningRegion(
            IGraphNode<FArenaNodeValue, FArenaEdgeValue> expectedNode,
            IGraphNode<FArenaNodeValue, FArenaEdgeValue> actualNode)
        {
            return expectedNode.Value.AttackerPosition == actualNode.Value.AttackerPosition
                   && expectedNode.Value.DefenderPosition == actualNode.Value.DefenderPosition
                   && expectedNode.Value.PlayerWhoseTurnItIs == actualNode.Value.PlayerWhoseTurnItIs;
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void SimulationArena_CorrectArenaGenerated(bool bCreateAllEdgesWithSameWeights)
        {
            ArenaGraph_JobApplication_Sim arenaGraphToTest = new ArenaGraph_JobApplication_Sim(bCreateAllEdgesWithSameWeights);
            AreGraphsEqual(arenaGraphToTest.LRoot_RRoot, arenaGraphToTest.GameGraph, EGameType.Simulation);
        }

        [Test]
        public void BisimulationArena_CorrectArenaGenerated()
        {
            ArenaGraph_JobApplication_Bisim arenaGraphToTest = new ArenaGraph_JobApplication_Bisim();
            AreGraphsEqual(arenaGraphToTest.LRoot_RRoot, arenaGraphToTest.GameGraph, EGameType.Bisimulation);
        }

        [Test]
        public void WeakBisimulationArena_CorrectArenaGenerated()
        {
            ArenaGraph_TwoLanes_WeakBisim arenaGraphToTest = new ArenaGraph_TwoLanes_WeakBisim();
            AreGraphsEqual(arenaGraphToTest.LRoot_RRoot, arenaGraphToTest.GameGraph, EGameType.WeakBisimulation);
        }

        [Test]
        public void CoupledSimulationArena_CorrectArenaGenerated()
        {
            ArenaGraph_CoupledLanes arenaGraphToTest = new ArenaGraph_CoupledLanes();
            AreGraphsEqual(arenaGraphToTest.LRoot_RRoot_AttTurn, arenaGraphToTest.GameGraph, EGameType.CoupledSimulation);
        }

    }
}
