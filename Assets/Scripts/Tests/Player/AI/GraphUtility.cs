﻿
using System;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

using CoupledSimilarity.Game.Player.AI;
using CoupledSimilarity.Game.Graph;
using CoupledSimilarity.Game.Rules;
using CoupledSimilarity.Game.Utility;

namespace Tests
{
    public static class GraphUtility
    {
        private class FNodePair
        {
            public readonly IGraphNode<FArenaNodeValue, FArenaEdgeValue> ExpectedNode;
            public readonly IGraphNode<FArenaNodeValue, FArenaEdgeValue> ActualNode;

            public FNodePair(IGraphNode<FArenaNodeValue, FArenaEdgeValue> expectedNode, IGraphNode<FArenaNodeValue, FArenaEdgeValue> actualNode)
            {
                ExpectedNode = expectedNode;
                ActualNode = actualNode;
            }

            public override bool Equals(object obj)
            {
                FNodePair other = obj as FNodePair;
                return other != null
                    && ExpectedNode == other.ExpectedNode
                    && ActualNode == other.ActualNode;
            }
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        public delegate bool AreNodeContentsEqual(
            IGraphNode<FArenaNodeValue, FArenaEdgeValue> expectedNode,
            IGraphNode<FArenaNodeValue, FArenaEdgeValue> actualNode);

        public static void AreGraphsEqual(
            IGraphNode<FArenaNodeValue, FArenaEdgeValue> correctGraphRoot,
            ITestGraph testGraph, 
            EGameType gameTypeToTest,
            AreNodeContentsEqual areNodeContentsEqual)
        {
            IGraphNode<FArenaNodeValue, FArenaEdgeValue> actualRootNode = ArenaGraph.GenerateArenaFrom(
                testGraph.AttackerStartNode,
                testGraph.DefenderStartNode,
                gameTypeToTest,
                defenderGameRules: null // We only inspect the graph's hierarchy; the passed rule object will never have methods called on it.
            );

            AssertAreEqualRecursive(correctGraphRoot, actualRootNode, areNodeContentsEqual);
        }
        public static void AssertAreEqualRecursive(
            IGraphNode<FArenaNodeValue, FArenaEdgeValue> expectedNode,
            IGraphNode<FArenaNodeValue, FArenaEdgeValue> actualNode,
            AreNodeContentsEqual areNodeContentsEqual)
        {
            if (!areNodeContentsEqual(expectedNode, actualNode))
            {
                Assert.Fail($"The root nodes are not equal. Expected: '{expectedNode}' Actual: '{actualNode}'");
            }

            Queue<FNodePair> pairsToCheck = new Queue<FNodePair>();
            ExtensionList<IGraphNode<FArenaNodeValue, FArenaEdgeValue>, EPlayerType> visitedSet = new ExtensionList<IGraphNode<FArenaNodeValue, FArenaEdgeValue>, EPlayerType>();

            FNodePair rootPair = new FNodePair(expectedNode, actualNode);
            pairsToCheck.Enqueue(rootPair);
            visitedSet.MarkCombination(expectedNode, actualNode, EPlayerType.Attacker);

            while(pairsToCheck.Count > 0)
            {
                FNodePair dequeuedPair = pairsToCheck.Dequeue();
                expectedNode = dequeuedPair.ExpectedNode;
                actualNode = dequeuedPair.ActualNode;

                Assert.AreEqual(
                    expectedNode.Neighbours.Count,
                    actualNode.Neighbours.Count,
                    "Do the nodes have the same number of children? expectedNode:{0} actualNode:{1}",
                    expectedNode,
                    actualNode
                );
                expectedNode.Neighbours.ForEach(
                    expectedChild =>
                    {
                        List<IGraphNode<FArenaNodeValue, FArenaEdgeValue>> equivalentChildrenInActualArena = actualNode.Neighbours
                            .Where(actualChild => areNodeContentsEqual(expectedChild.TargetNode, actualChild.TargetNode))
                            .Select(edge => edge.TargetNode)
                            .ToList();

                        Assert.AreEqual(
                            1,
                            equivalentChildrenInActualArena.Count,
                            "There should be exactly 1 mutually equal child for expectedNode = '{0}' and actualNode = '{1}' but the list is '{2}' and the child is {3}",
                            expectedNode,
                            actualNode,
                            equivalentChildrenInActualArena.toCompactString(),
                            expectedChild.TargetNode
                            );

                        EPlayerType playerWhoseTurnItIs = expectedChild.TargetNode.Value.PlayerWhoseTurnItIs;
                        if (!visitedSet.IsCombinationMarked(expectedChild.TargetNode, equivalentChildrenInActualArena[0], playerWhoseTurnItIs))
                        {
                            FNodePair pairToEnqueue = new FNodePair(expectedChild.TargetNode, equivalentChildrenInActualArena[0]);
                            pairsToCheck.Enqueue(pairToEnqueue);
                            visitedSet.MarkCombination(expectedChild.TargetNode, equivalentChildrenInActualArena[0], playerWhoseTurnItIs);
                        }
                    }
                    );
            }
        }
    }
}
