﻿
using System;

using CoupledSimilarity.Game.Rules;

namespace Tests
{
    public class PlayerLogicMock : IPlayerLogic
    {
        public IGameState GameState { get; private set; }
        public IRules PlayerMoveRules { get; private set; }

        public event Action StartTurnEvent;
        public event Action EndTurnEvent;
        public event Action<IGameState, IRules> OnStartNewGameEvent;

        public void StartTurn()
        {
            StartTurnEvent?.Invoke();
        }

        public void EndTurn()
        {
            EndTurnEvent?.Invoke();
        }

        public void OnStartNewGame(IGameState gameState, IRules playerMoveRules)
        {
            GameState = gameState;
            PlayerMoveRules = playerMoveRules;
            OnStartNewGameEvent?.Invoke(gameState, playerMoveRules);
        }
    }
}
